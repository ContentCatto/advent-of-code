#![cfg(test)]

use std::{
    fmt::{Debug, Display},
};

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
enum ConsoleLine {
    List,
    ChangeDir(String),
    Dir(String),
    File(String, u64),
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
enum Entry {
    Dir(Directory),
    File(String, u64),
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Entry {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let self_name = match &self {
            Entry::Dir(dir) => &dir.name,
            Entry::File(name, _) => name,
        };
        let other_name = match &other {
            Entry::Dir(dir) => &dir.name,
            Entry::File(name, _) => name,
        };
        self_name.cmp(other_name)
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Ord, Hash)]
struct Directory {
    name: String,
    contents: Vec<Entry>,
}

impl Display for Directory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.pretty_print(f, 0)
    }
}

impl Debug for Directory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.debug_print(f, 0)
    }
}

impl Directory {
    fn pretty_print(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        mut indent_level: usize,
    ) -> std::fmt::Result {
        const INDENT_BUFFER: &str = "                                          ";
        fn indent(indent_level: usize) -> &'static str {
            &INDENT_BUFFER[..indent_level * 2]
        }
        let name = if !self.name.is_empty() {
            &self.name
        } else {
            "/"
        };
        writeln!(f, "{}- {} (dir)", indent(indent_level), name)?;
        indent_level += 1;
        for entry in &self.contents {
            match entry {
                Entry::Dir(subdir) => subdir.pretty_print(f, indent_level),
                Entry::File(name, size) => writeln!(
                    f,
                    "{}- {} (file, size={})",
                    indent(indent_level),
                    name,
                    size
                ),
            }?
        }
        Ok(())
    }

    fn debug_print(
        &self,
        f: &mut std::fmt::Formatter<'_>,
        mut indent_level: usize,
    ) -> std::fmt::Result {
        const INDENT_BUFFER: &str = "                                          ";
        fn indent(indent_level: usize) -> &'static str {
            &INDENT_BUFFER[..indent_level * 2]
        }
        let name = if !self.name.is_empty() {
            &self.name
        } else {
            "/"
        };
        writeln!(
            f,
            "{}- {} (dir, size={})",
            indent(indent_level),
            name,
            self.total_size()
        )?;
        indent_level += 1;
        for entry in &self.contents {
            match entry {
                Entry::Dir(subdir) => subdir.debug_print(f, indent_level),
                Entry::File(name, size) => writeln!(
                    f,
                    "{}- {} (file, size={})",
                    indent(indent_level),
                    name,
                    size
                ),
            }?
        }
        Ok(())
    }

    fn total_size(&self) -> u64 {
        let mut size = 0;
        for entry in &self.contents {
            size += match entry {
                Entry::Dir(subdir) => subdir.total_size(),
                Entry::File(_, size) => *size,
            };
        }
        size
    }
}

fn build_filesystem(lines: Vec<ConsoleLine>) -> Directory {
    let root = Directory {
        name: String::from(""),
        contents: Vec::new(),
    };

    let mut parent_dirs: Vec<Directory> = Vec::new();
    let mut current_dir: Directory = root;

    /// Returns the new current directory
    fn up_one_dir(parent_dirs: &mut Vec<Directory>, current_dir: Directory) -> Directory {
        let mut parent = parent_dirs.pop().unwrap();
        parent.contents.push(Entry::Dir(current_dir));
        parent.contents.sort();
        parent
    }

    for line in lines {
        match line {
            ConsoleLine::List => {}
            ConsoleLine::ChangeDir(dir_name) => {
                let dir_name: &str = &dir_name;
                match dir_name {
                    "/" => {
                        while !parent_dirs.is_empty() {
                            current_dir = up_one_dir(&mut parent_dirs, current_dir);
                        }
                    }
                    ".." => {
                        current_dir = up_one_dir(&mut parent_dirs, current_dir);
                    }
                    dir_name => {
                        let mut new_wd: Option<Directory> = None;
                        let subdirs = current_dir.contents.into_iter();
                        let mut remaining = Vec::new();
                        for subdir in subdirs {
                            match subdir {
                                Entry::Dir(dir) if dir.name == dir_name => {
                                    assert!(new_wd.is_none());
                                    new_wd = Some(dir);
                                }
                                subdir => remaining.push(subdir),
                            }
                        }
                        current_dir.contents = remaining;
                        parent_dirs.push(current_dir);
                        current_dir = new_wd.unwrap();
                    }
                }
            }
            ConsoleLine::Dir(name) => current_dir.contents.push(Entry::Dir(Directory {
                name,
                contents: Vec::new(),
            })),
            ConsoleLine::File(name, size) => current_dir.contents.push(Entry::File(name, size)),
        }
    }

    while !parent_dirs.is_empty() {
        current_dir = up_one_dir(&mut parent_dirs, current_dir);
    }
    // This will be the root directory
    current_dir
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Vec<ConsoleLine> {
    fn get_rest<'a>(words: impl Iterator<Item = &'a str>) -> String {
        words.fold(String::new(), |acc, w| acc + w)
    }
    rows.map(|row: String| {
        let mut words = row.split_whitespace();
        match words.next().unwrap() {
            "$" => match words.next().unwrap() {
                "cd" => ConsoleLine::ChangeDir(get_rest(words)),
                "ls" => ConsoleLine::List,
                _ => panic!(),
            },
            "dir" => ConsoleLine::Dir(get_rest(words)),
            file_size if file_size.parse::<u64>().is_ok() => {
                let file_size: u64 = file_size.parse().unwrap();
                ConsoleLine::File(get_rest(words), file_size)
            }
            _ => panic!(),
        }
    })
    .collect()
}

fn input() -> Vec<ConsoleLine> {
    input_from_rows(common::input_rows("input/day07"))
}

fn example_input() -> Vec<ConsoleLine> {
    input_from_rows(common::input_rows("input/day07-example"))
}

#[test]
fn example_matches_format() {
    let fs = build_filesystem(example_input());
    let debug = format!("{}", fs);
    let expected = "- / (dir)
  - a (dir)
    - e (dir)
      - i (file, size=584)
    - f (file, size=29116)
    - g (file, size=2557)
    - h.lst (file, size=62596)
  - b.txt (file, size=14848514)
  - c.dat (file, size=8504156)
  - d (dir)
    - j (file, size=4060174)
    - d.log (file, size=8033020)
    - d.ext (file, size=5626152)
    - k (file, size=7214296)
";
    for (debug, expected) in debug.lines().zip(expected.lines()) {
        eprintln!("Real:     {}", debug);
        eprintln!("Expected: {}\n", expected);
    }
    assert_eq!(debug, expected);
}

#[test]
fn example_matches_sizes() {
    let fs = build_filesystem(example_input());
    assert_eq!(fs.total_size(), 48381165);
    assert_eq!(
        fs.contents
            .iter()
            .find_map(|entry| match entry {
                Entry::Dir(subdir) if subdir.name == "d" => Some(subdir),
                _ => None,
            })
            .unwrap()
            .total_size(),
        24933642
    );
}

fn find_matching_dirs<'a>(
    fs: &'a Directory,
    predicate: impl Fn(&Directory) -> bool + Clone,
) -> impl Iterator<Item = &'a Directory> {
    let mut selected_dirs: Vec<&'a Directory> = Vec::new();
    if predicate(fs) {
        selected_dirs.push(fs);
    }
    for entry in &fs.contents {
        if let Entry::Dir(subdir) = entry {
            find_matching_dirs(subdir, predicate.clone()).for_each(|dir| selected_dirs.push(dir))
        }
    }
    selected_dirs.into_iter()
}

#[test]
fn dirs_less_than_100k() {
    let fs = build_filesystem(input());
    let total: u64 = find_matching_dirs(&fs, |dir: &Directory| dir.total_size() <= 100000)
        .map(Directory::total_size)
        .sum();
    assert_eq!(total, 1086293);
}

#[test]
fn dir_to_delete() {
    let fs = build_filesystem(input());
    const FS_SIZE: u64 = 70000000;
    const UPDATE_SIZE: u64 = 30000000;
    let used_space = fs.total_size();
    let free_space = FS_SIZE - used_space;
    let required_freed_space = UPDATE_SIZE - free_space;

    let sizes = find_matching_dirs(&fs, |dir: &Directory| {
        dir.total_size() >= required_freed_space
    })
    .map(Directory::total_size);
    let smallest = sizes.reduce(std::cmp::min).unwrap();
    assert_eq!(smallest, 366028);
}
