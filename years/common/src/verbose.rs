#[macro_export]
macro_rules! verbose_println {
    () => {
        if $crate::verbose::DEBUG {
            println!();
        }
    };
    ($($arg:tt)*) => {{
        if $crate::verbose::DEBUG {
            println!($($arg)*);
        }
    }};
}

#[cfg(not(debug_assertions))]
pub const DEBUG: bool = false;

#[cfg(debug_assertions)]
pub const DEBUG: bool = true;
