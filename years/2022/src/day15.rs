use std::{
    cmp::Ordering,
    collections::{hash_map::Keys, HashMap, HashSet},
    convert::{TryFrom, TryInto},
    fmt::{Debug, Display},
    iter::FromIterator,
    num::NonZeroUsize,
    ops::Range,
};

use common::{
    annotations::{rasterize_with_coordinate_margins, DisplayClosure},
    pos::*,
    sparse_range::SparseRange,
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day15"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day15-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let rows: Vec<String> = rows.collect();
    let rows = rows.iter().map(String::as_str);
    let sensors: Vec<Sensor> = rows.map(Sensor::try_from).try_collect().unwrap();
    Map::from(sensors)
}

pub mod representations {
    use anstyle::{AnsiColor, Color, Style};
    use lazy_static::lazy_static;

    use common::annotations::GridCellRepresentation;

    pub const SENSOR_STYLE: Style = Style::new().fg_color(Some(Color::Ansi(AnsiColor::BrightBlue)));
    pub const BEACON_STYLE: Style = Style::new().fg_color(Some(Color::Ansi(AnsiColor::Yellow)));
    pub const EMPTY_GRID_CELL_STYLE: Style =
        Style::new().fg_color(Some(Color::Ansi(AnsiColor::BrightBlack)));
    pub const COVERED_GRID_CELL_STYLE: Style =
        Style::new().fg_color(Some(Color::Ansi(AnsiColor::BrightBlack)));

    lazy_static! {
        static ref SENSOR_REPR: String = format!("{SENSOR_STYLE}S{SENSOR_STYLE:#}");
        static ref BEACON_REPR: String = format!("{BEACON_STYLE}B{BEACON_STYLE:#}");
        static ref EMPTY_GRID_CELL_REPR: String =
            format!("{EMPTY_GRID_CELL_STYLE}.{EMPTY_GRID_CELL_STYLE:#}");
        static ref COVERED_GRID_CELL_REPR: String =
            format!("{COVERED_GRID_CELL_STYLE}#{COVERED_GRID_CELL_STYLE:#}");
        static ref TARGET_SENSOR_REPR: String = {
            let style = SENSOR_STYLE.bold();
            format!("{style}S{style:#}")
        };
        static ref TARGET_BEACON_REPR: String = {
            let style = BEACON_STYLE.bold();
            format!("{style}B{style:#}")
        };
    }

    pub fn sensor_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&SENSOR_REPR)
    }

    pub fn beacon_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&BEACON_REPR)
    }

    pub fn empty_grid_cell_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&EMPTY_GRID_CELL_REPR)
    }

    pub fn covered_grid_cell_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&COVERED_GRID_CELL_REPR)
    }

    pub fn target_sensor_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&TARGET_SENSOR_REPR)
    }

    pub fn target_beacon_repr() -> GridCellRepresentation<'static> {
        GridCellRepresentation::Str(&TARGET_BEACON_REPR)
    }
}

type Input = Map;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Sensor {
    pos: Pos,
    closest_beacon: Pos,
}

impl TryFrom<&str> for Sensor {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let value = value.replace(", ", ",");
        match value.split_whitespace().collect::<Vec<&str>>().as_slice() {
            // Example line:
            // "Sensor at x=2,y=18: closest beacon is at x=-2,y=15"
            &["Sensor", "at", sensor_pos_str, "closest", "beacon", "is", "at", beacon_pos_str] => {
                let sensor_pos_str = sensor_pos_str.strip_suffix(":").ok_or_else(|| {
                    format!("Sensor position is not followed by a colon: {sensor_pos_str:?}")
                })?;

                let sensor_pos =
                    Pos::try_from_string_with_explicit_xy_denominators(sensor_pos_str)?;
                let beacon_pos =
                    Pos::try_from_string_with_explicit_xy_denominators(beacon_pos_str)?;

                Ok(Sensor {
                    pos: sensor_pos,
                    closest_beacon: beacon_pos,
                })
            }
            _ => Err(format!("Invalid line: {value:?}")),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Map {
    pub closest_beacon_by_sensor: HashMap<Pos, Pos>,
    pub beacons: HashSet<Pos>,
    pub beacon_distance_by_sensor: HashMap<Pos, u64>,
}

impl Map {
    pub fn new(closest_beacon_by_sensor: HashMap<Pos, Pos>) -> Self {
        let beacons = closest_beacon_by_sensor.values().cloned().collect();
        let beacon_distance_by_sensor = HashMap::from_iter(
            closest_beacon_by_sensor
                .iter()
                .map(|(&sensor, &beacon)| (sensor, sensor.hamming_distance(beacon))),
        );
        Self {
            closest_beacon_by_sensor,
            beacons,
            beacon_distance_by_sensor,
        }
    }

    pub fn sensors<'a>(&'a self) -> Keys<'a, Pos, Pos> {
        self.closest_beacon_by_sensor.keys()
    }

    pub fn has_sensor(&self, sensor: &Pos) -> bool {
        self.closest_beacon_by_sensor.contains_key(sensor)
    }

    pub fn position_bounds(&self) -> (Pos, Pos) {
        let mentioned_positions: Vec<Pos> = self
            .beacons
            .iter()
            .chain(self.closest_beacon_by_sensor.keys())
            .copied()
            .collect();
        mentioned_positions.into_iter().into_bounds().unwrap()
    }

    pub fn sensor_coverage_bounds_for_sensor(&self, target_sensor: Pos) -> Option<(Pos, Pos)> {
        let Some(&closest_beacon) = self.closest_beacon_by_sensor.get(&target_sensor) else {
            return None;
        };

        let dist = target_sensor.hamming_distance(closest_beacon) as isize;

        Some(
            [
                target_sensor + Pos(-dist, -dist),
                target_sensor + Pos(dist, dist),
            ]
            .iter()
            .cloned()
            .into_bounds()
            .unwrap(),
        )
    }

    pub fn position_bounds_for_map_and_sensors<It>(&self, target_sensors: It) -> Option<(Pos, Pos)>
    where
        It: IntoIterator<Item = Pos>,
        <It as IntoIterator>::IntoIter: Clone,
    {
        let (map_min_bound, map_max_bound) = self.position_bounds();
        let map_bounds = IntoIterator::into_iter([map_min_bound, map_max_bound]);

        let sensor_coverage_bounds = target_sensors.into_iter().flat_map(|sensor_pos| {
            let (coverage_min_bounds, coverage_max_bounds) =
                self.sensor_coverage_bounds_for_sensor(sensor_pos).unwrap();
            IntoIterator::into_iter([coverage_min_bounds, coverage_max_bounds])
        });

        Some(
            map_bounds
                .chain(sensor_coverage_bounds)
                .into_bounds()
                .unwrap(),
        )
    }

    pub fn total_position_bounds(&self) -> (Pos, Pos) {
        self.position_bounds_for_map_and_sensors(self.sensors().copied())
            .unwrap()
    }

    pub fn display_total_coverage<'map>(&'map self) -> impl Display + 'map {
        self.display_coverage_for_sensors(self.sensors().copied())
            .unwrap()
    }

    pub fn display_coverage_for_sensor<'map>(
        &'map self,
        target_sensor: Pos,
    ) -> Option<impl Display + 'map> {
        self.display_coverage_for_sensors([target_sensor])
    }

    pub fn make_total_coverage_probe(&self) -> CoverageProbe {
        self.make_coverage_probe(self.sensors().copied()).unwrap()
    }

    pub fn make_coverage_probe(
        &self,
        target_sensors: impl Iterator<Item = Pos> + Clone,
    ) -> Option<CoverageProbe> {
        if !target_sensors
            .clone()
            .all(|sensor_pos| self.closest_beacon_by_sensor.contains_key(&sensor_pos))
        {
            return None;
        }

        let target_beacons = target_sensors
            .clone()
            .map(|sensor_pos| *self.closest_beacon_by_sensor.get(&sensor_pos).unwrap());

        Some(CoverageProbe::new(target_sensors.zip(target_beacons)))
    }

    pub fn display_coverage_for_sensors<'map>(
        &'map self,
        target_sensors: impl IntoIterator<Item = Pos>,
    ) -> Option<impl Display + 'map> {
        let target_sensors: HashSet<Pos> = HashSet::from_iter(target_sensors.into_iter());
        let coverage = self.make_coverage_probe(target_sensors.iter().copied())?;

        let target_beacons: HashSet<Pos> = HashSet::from_iter(
            target_sensors
                .iter()
                .copied()
                .map(|sensor_pos| *self.closest_beacon_by_sensor.get(&sensor_pos).unwrap()),
        );

        let (Pos(min_x, min_y), Pos(max_x, max_y)) = self
            .position_bounds_for_map_and_sensors(target_sensors.iter().copied())
            .unwrap();

        Some(DisplayClosure::new(
            move |formatter: &mut std::fmt::Formatter<'_>| -> std::fmt::Result {
                rasterize_with_coordinate_margins(
                    formatter,
                    min_x..=max_x,
                    min_y..=max_y,
                    NonZeroUsize::new(5).unwrap(),
                    NonZeroUsize::new(1).unwrap(),
                    1,
                    0,
                    |pos: Pos| {
                        if target_sensors.contains(&pos) {
                            return representations::target_sensor_repr();
                        }

                        if self.has_sensor(&pos) {
                            return representations::sensor_repr();
                        }

                        if target_beacons.contains(&pos) {
                            return representations::target_beacon_repr();
                        }

                        if self.beacons.contains(&pos) {
                            return representations::beacon_repr();
                        }

                        if coverage.is_covered(pos) {
                            return representations::covered_grid_cell_repr();
                        }

                        return representations::empty_grid_cell_repr();
                    },
                )
            },
        ))
    }

    pub fn num_positions_covered_in_row(&self, row: isize) -> Option<usize> {
        let coverage = self.make_total_coverage_probe();
        let (Pos(min_x, min_y), Pos(max_x, max_y)) = self.total_position_bounds();
        if !(min_y..=max_y).contains(&row) {
            return None;
        }

        Some(
            (min_x..=max_x)
                .filter(|&x| coverage.is_covered(Pos(x, row)))
                .filter(|&x| !self.beacons.contains(&Pos(x, row)))
                .count(),
        )
    }

    pub fn make_sparse_ranges(&self, map_max_size: usize) -> Vec<SparseRange<u64>> {
        let mut sparse_ranges: Vec<SparseRange<u64>> =
            (0..map_max_size + 1).map(|_| SparseRange::new()).collect();

        for (&Pos(sensor_x, sensor_y), &sensor_range) in self.beacon_distance_by_sensor.iter() {
            let sensor_x: u64 = sensor_x.try_into().unwrap();
            let sensor_y: u64 = sensor_y.try_into().unwrap();

            let affected_y_coords = sensor_y.saturating_sub(sensor_range)
                ..=(sensor_y + sensor_range).min(map_max_size as u64);
            for sparse_range_y in affected_y_coords {
                let effective_sensor_range = sensor_range - sparse_range_y.abs_diff(sensor_y);
                let covered_x_coords = sensor_x.saturating_sub(effective_sensor_range)
                    ..(sensor_x + effective_sensor_range + 1).min(map_max_size as u64 + 1);

                sparse_ranges[sparse_range_y as usize].add(covered_x_coords);
            }
        }

        // #[cfg(debug_assertions)]
        // #[cfg(test)]
        // for y in 0..=map_max_size as isize {
        //     for x in 0..=map_max_size as isize {
        //         let pos = Pos(x, y);
        //         dbg!(pos);
        //         let range = &sparse_ranges[y as usize];
        //         let is_covered = range.contains(&(x as u64));
        //         let should_be_covered = self.is_covered(pos);
        //         assert_eq!(is_covered, should_be_covered);
        //     }
        // }

        sparse_ranges
    }

    pub fn find_distress_signal(&self, map_max_size: usize) -> Pos {
        let sparse_ranges = self.make_sparse_ranges(map_max_size);
        for y in 0..=map_max_size {
            let range = &sparse_ranges[y];
            if range.sub_ranges().len() != 1 {
                let mut uncovered = SparseRange::from(0..map_max_size as u64 + 1);
                uncovered -= range;
                let sub_range: Range<u64> = match uncovered.sub_ranges() {
                    &[ref sub_range] => sub_range.clone(),
                    sub_ranges => panic!("Expected exactly 1 sub-range, not: {sub_ranges:?}"),
                };
                let x_values = Vec::from_iter(sub_range);
                match &x_values[..] {
                    &[x] => return Pos(x as isize, y as isize),
                    x_values => panic!("Expected exactly 1 x-value, not: {x_values:?}"),
                }
            }
        }
        panic!("Whole map is covered?");
    }

    pub fn find_distress_signal_old_impl(&self, map_max_size: usize) -> Pos {
        let mut random_source = random::default(123123123123123);

        let mut already_checked = HashSet::new();

        loop {
            let mut current_pos: Pos = random::Value::read(&mut random_source);
            current_pos.0 = (current_pos.0 % map_max_size as isize).abs();
            current_pos.1 = (current_pos.1 % map_max_size as isize).abs();

            if let Some(uncovered_pos) =
                self.search_for_uncovered_pos(current_pos, &mut already_checked, map_max_size)
            {
                return uncovered_pos;
            }
        }
    }

    #[inline]
    pub fn is_covered(&self, pos: Pos) -> bool {
        self.covering_sensors(pos).next().is_some()
    }

    #[inline]
    pub fn covering_sensors(&self, pos: Pos) -> impl Iterator<Item = (Pos, u64)> + '_ {
        self.beacon_distance_by_sensor
            .iter()
            .map(|(&sensor_pos, &max_dist)| (sensor_pos, max_dist))
            .filter(move |&(sensor_pos, max_dist)| sensor_pos.hamming_distance(pos) <= max_dist)
    }

    pub fn search_for_uncovered_pos(
        &self,
        start: Pos,
        already_checked: &mut HashSet<Pos>,
        map_max_size: usize,
    ) -> Option<Pos> {
        let map_bounds = 0..map_max_size as isize;
        let mut current_pos = start;
        loop {
            if already_checked.contains(&current_pos) {
                return None;
            }
            #[cfg(debug_assertions)]
            eprintln!("Checking total sensor coverage for pos {current_pos}");
            let covering_sensors = self.covering_sensors(current_pos);
            let mut is_covered = false;
            let mut last_change = Ordering::Equal;
            for (covering_sensor, sensor_range) in covering_sensors {
                is_covered = true;
                if already_checked.len() % 1000000 == 0 {
                    eprintln!("{} positions checked", already_checked.len());
                }
                already_checked.insert(current_pos);
                let new_pos =
                    self.move_pos_away_from_sensor(current_pos, covering_sensor, sensor_range);
                let new_change = new_pos.0.cmp(&current_pos.0);
                if last_change != Ordering::Equal && last_change != new_change {
                    #[cfg(debug_assertions)]
                    eprintln!(
                        "Not moving to pos {current_pos} to get away from sensor at pos {covering_sensor}, \
                        because that puts us closer to another sensor"
                    );
                }
                last_change = new_change;
                current_pos = new_pos;

                #[cfg(debug_assertions)]
                eprintln!(
                    "Moving to pos {current_pos} to get away from sensor at pos {covering_sensor}"
                );
                if !map_bounds.contains(&current_pos.0) || !map_bounds.contains(&current_pos.1) {
                    return None;
                }
                if current_pos.hamming_distance(covering_sensor) > sensor_range {
                    break;
                }
            }
            if !is_covered {
                return Some(current_pos);
            }
        }
    }

    pub fn move_pos_away_from_sensor(
        &self,
        pos: Pos,
        away_from_sensor: Pos,
        sensor_max_range: u64,
    ) -> Pos {
        // TODO: move in more directions than just X.
        let dist = pos.hamming_distance(away_from_sensor);
        if dist > sensor_max_range {
            return pos;
        }
        let diff = (sensor_max_range - dist) as isize;
        if pos.0 >= away_from_sensor.0 {
            Pos(pos.0 + diff, pos.1)
        } else {
            Pos(pos.0 - diff, pos.1)
        }
    }
}

impl<T: IntoIterator<Item = Sensor>> From<T> for Map {
    fn from(value: T) -> Self {
        let closest_beacon_by_sensor = HashMap::from_iter(
            value
                .into_iter()
                .map(|sensor| (sensor.pos, sensor.closest_beacon)),
        );
        Self::new(closest_beacon_by_sensor)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CoverageProbe {
    pub sensor_ranges: Vec<(Pos, u64)>,
}

impl CoverageProbe {
    pub fn new(sensors_and_beacons: impl Iterator<Item = (Pos, Pos)>) -> Self {
        let sensor_ranges = sensors_and_beacons
            .map(|(sensor_pos, closest_beacon_pos)| {
                let sensor_range = sensor_pos.hamming_distance(closest_beacon_pos);
                (sensor_pos, sensor_range)
            })
            .collect();
        Self { sensor_ranges }
    }

    pub fn is_covered(&self, pos: Pos) -> bool {
        for (sensor_pos, sensor_range) in self.sensor_ranges.iter() {
            if sensor_pos.hamming_distance(pos) <= *sensor_range {
                return true;
            }
        }
        false
    }
}

fn tuning_signal(pos: Pos) -> u64 {
    dbg!(pos);
    pos.0 as u64 * 4000000 + pos.1 as u64
}

#[test]
fn test_parse_sensor_and_beacon() {
    let sensor: Sensor = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15"
        .try_into()
        .unwrap();
    let expected = Sensor {
        pos: Pos(2, 18),
        closest_beacon: Pos(-2, 15),
    };
    assert_eq!(sensor, expected);
}

#[test]
fn test_parse_example_input() {
    let sensors_and_beacons = example_input();
    let expected = Map::new(HashMap::from([
        (Pos(2, 18), Pos(-2, 15)),
        (Pos(9, 16), Pos(10, 16)),
        (Pos(13, 2), Pos(15, 3)),
        (Pos(12, 14), Pos(10, 16)),
        (Pos(10, 20), Pos(10, 16)),
        (Pos(14, 17), Pos(10, 16)),
        (Pos(8, 7), Pos(2, 10)),
        (Pos(2, 0), Pos(2, 10)),
        (Pos(0, 11), Pos(2, 10)),
        (Pos(20, 14), Pos(25, 17)),
        (Pos(17, 20), Pos(21, 22)),
        (Pos(16, 7), Pos(15, 3)),
        (Pos(14, 3), Pos(15, 3)),
        (Pos(20, 1), Pos(15, 3)),
    ]));
    assert_eq!(sensors_and_beacons, expected);
}

#[test]
fn test_print_single_sensor_coverage() {
    let map = example_input();

    println!("{}", map.display_coverage_for_sensor(Pos(8, 7)).unwrap());
}

#[test]
fn test_print_total_sensor_coverage() {
    let map = example_input();

    println!("{}", map.display_total_coverage());
}

#[test]
fn make_sparse_ranges_example() {
    let map = example_input();
    let sparse_ranges = map.make_sparse_ranges(20);

    let expected = vec![
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..14, 15..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
        SparseRange::from_iter([0..21]),
    ];

    assert_eq!(sparse_ranges.len(), expected.len());
    for (mut sparse_range, mut expected) in sparse_ranges.into_iter().zip(expected) {
        sparse_range.sort();
        expected.sort();
        dbg!(&sparse_range, &expected);
        assert!(sparse_range.equals(&mut expected));
    }
}

#[test]
fn part1_example() {
    let map = example_input();

    assert_eq!(map.num_positions_covered_in_row(10), Some(26));
}

#[test]
fn part1_real() {
    let map = input();

    assert_eq!(map.num_positions_covered_in_row(2_000_000), Some(4717631));
}

#[test]
fn part2_example() {
    let map = example_input();
    let distress_beacon_pos = map.find_distress_signal(20);

    assert_eq!(tuning_signal(distress_beacon_pos), 56000011);
}

#[test]
fn part2_real() {
    let map = input();
    let distress_beacon_pos = map.find_distress_signal(4000000);

    assert_eq!(tuning_signal(distress_beacon_pos), 13197439355220);
}
