use std::{
    cell::Cell,
    collections::HashMap,
    fmt::{Display, Write},
    num::NonZeroUsize,
    ops::RangeInclusive,
};

use crate::pos::Pos;

pub trait Annotatable {
    fn write_fmt_annotated(
        &self,
        writer: impl Write,
        annotations: &HashMap<Pos, String>,
    ) -> std::fmt::Result;

    fn annotate(&self, annotations: impl Into<HashMap<Pos, String>>) -> Annotated<Self> {
        Annotated {
            inner: self,
            annotations: annotations.into(),
        }
    }
}

#[macro_export]
macro_rules! impl_display_for_annotatable {
    ($typ:ty) => {
        impl Display for $typ {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                use $crate::annotations::Annotatable;
                self.write_fmt_annotated(f, &HashMap::new())
            }
        }
    };
}

#[derive(Clone)]
pub struct Annotated<'a, T>
where
    T: Annotatable + ?Sized,
{
    pub inner: &'a T,
    pub annotations: HashMap<Pos, String>,
}

impl<T: Annotatable> Display for Annotated<'_, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.inner.write_fmt_annotated(f, &self.annotations)
    }
}

pub fn select_coords_to_print(
    range: RangeInclusive<isize>,
    repeat_every: NonZeroUsize,
    disallow_adjacent_to_edge: usize,
) -> (HashMap<isize, String>, usize) {
    let repeat_every = repeat_every.get() as isize;

    let start = range.clone().next().unwrap();
    let end = range.clone().last().unwrap();
    let start_and_end = IntoIterator::into_iter([start, end]);
    let repeats = range
        .clone()
        .into_iter()
        .filter(|x| *x % repeat_every == 0)
        .filter(|x| x.abs_diff(*range.start()) > disallow_adjacent_to_edge)
        .filter(|x| x.abs_diff(*range.end()) > disallow_adjacent_to_edge);

    let coords_to_print: HashMap<isize, String> =
        HashMap::from_iter(start_and_end.chain(repeats).map(|x| (x, x.to_string())));

    let max_coord_num_chars = coords_to_print
        .values()
        .map(|s| s.len())
        .max()
        .expect("always has bounds");

    (coords_to_print, max_coord_num_chars)
}

pub struct DisplayClosure<T>
where
    T: FnOnce(&mut std::fmt::Formatter<'_>) -> std::fmt::Result,
{
    closure: Cell<Option<T>>,
}

impl<T> DisplayClosure<T>
where
    T: FnOnce(&mut std::fmt::Formatter<'_>) -> std::fmt::Result,
{
    pub fn new(closure: T) -> Self {
        Self {
            closure: Cell::new(Some(closure)),
        }
    }
}

impl<T> Display for DisplayClosure<T>
where
    T: FnOnce(&mut std::fmt::Formatter<'_>) -> std::fmt::Result,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(closure) = self.closure.take() {
            closure(f)
        } else {
            f.write_str("<already-used>")
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum GridCellRepresentation<'s> {
    Char(char),
    Str(&'s str),
}

impl Display for GridCellRepresentation<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            Self::Char(ch) => write!(f, "{}", ch),
            Self::Str(s) => write!(f, "{}", s),
        }
    }
}

pub fn rasterize_with_coordinate_margins<'s>(
    w: &mut impl std::fmt::Write,
    range_x: RangeInclusive<isize>,
    range_y: RangeInclusive<isize>,
    repeat_x_coords_every: NonZeroUsize,
    repeat_y_coords_every: NonZeroUsize,
    disallow_adjacent_to_edge_x: usize,
    disallow_adjacent_to_edge_y: usize,
    get_grid_cell_representation: impl Fn(Pos) -> GridCellRepresentation<'s> + 's,
) -> std::fmt::Result {
    let (x_coords_to_print, max_x_coord_num_chars) = select_coords_to_print(
        range_x.clone(),
        repeat_x_coords_every,
        disallow_adjacent_to_edge_x,
    );
    let (y_coords_to_print, max_y_coord_num_chars) = select_coords_to_print(
        range_y.clone(),
        repeat_y_coords_every,
        disallow_adjacent_to_edge_y,
    );

    let y_coord_pad = " ".repeat(max_y_coord_num_chars);

    for coord_char_index in 0..max_x_coord_num_chars {
        write!(w, "{y_coord_pad} ")?;
        for x in range_x.clone() {
            let Some(string) = x_coords_to_print.get(&x) else {
                write!(w, " ")?;
                continue;
            };
            let blank_pad_count = max_x_coord_num_chars - string.len();
            let Some(digit_index) = coord_char_index.checked_sub(blank_pad_count) else {
                write!(w, " ")?;
                continue;
            };
            let digit_char = string.chars().nth(digit_index).unwrap();
            write!(w, "{}", digit_char)?;
        }
        write!(w, "\n")?;
    }

    for y in range_y {
        if let Some(string) = y_coords_to_print.get(&y) {
            write!(w, "{string:>max_y_coord_num_chars$} ")?;
        } else {
            write!(w, "{y_coord_pad} ")?;
        }

        for x in range_x.clone() {
            let pos = Pos(x, y);
            let grid_cell = get_grid_cell_representation(pos);
            write!(w, "{}", grid_cell)?;
        }
        write!(w, "\n")?;
    }
    Ok(())
}
