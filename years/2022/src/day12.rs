#![cfg(test)]

use std::{
    collections::HashMap,
    fmt::{self, Display, Write},
    rc::Rc,
};

use common::{verbose_println, DEBUG};

fn input() -> Map {
    input_from_rows(common::input_rows("input/day12"))
}

fn example_input() -> Map {
    input_from_rows(common::input_rows("input/day12-example"))
}

fn iter_positions(size: (usize, usize)) -> impl Iterator<Item = Pos> {
    (0..size.0).flat_map(move |x: usize| (0..size.1).map(move |y: usize| (x as i32, y as i32)))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Map {
    let rows: Vec<_> = rows.collect();
    let row_lengths: Vec<_> = rows.iter().map(String::len).collect();
    let row_length = row_lengths[0];
    assert!(row_lengths.iter().all(|len| *len == row_length));
    let column_length = row_lengths.len();

    let mut grid = Vec::with_capacity(row_length * column_length);
    let size = (row_length, column_length);
    // Map grid will be read left-to-right then down-to-up
    rows.into_iter()
        .rev()
        .for_each(|row: String| row.chars().for_each(|ch: char| grid.push(ch as u32)));

    fn find_char(grid: &[u32], size: (usize, usize), ch: char) -> Pos {
        iter_positions(size)
            .find(|(x, y)| {
                let idx = *x as usize + *y as usize * size.0;
                *grid.get(idx).unwrap() == ch as u32
            })
            .unwrap()
    }
    let start = find_char(&grid, size, 'S');
    let end = find_char(&grid, size, 'E');

    let mut map = Map {
        grid,
        size,
        start,
        end,
    };

    *map.index_mut(map.start).unwrap() = 'a' as u32;
    *map.index_mut(map.end).unwrap() = 'z' as u32;

    map
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    const VARIANTS: [Dir; 4] = [Dir::Up, Dir::Down, Dir::Left, Dir::Right];

    fn iter() -> impl Iterator<Item = Dir> {
        Self::VARIANTS.iter().copied()
    }

    fn iter_offsets() -> impl Iterator<Item = Pos> {
        Self::iter().map(Self::as_offset)
    }

    fn as_offset(self) -> Pos {
        match self {
            Dir::Up => (0, 1),
            Dir::Down => (0, -1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
        }
    }

    fn as_ascii_arrow(self) -> char {
        match self {
            Dir::Up => '^',
            Dir::Down => 'v',
            Dir::Left => '<',
            Dir::Right => '>',
        }
    }

    fn from_offset(offset: Pos) -> Option<Self> {
        match offset {
            (0, 1) => Some(Dir::Up),
            (0, -1) => Some(Dir::Down),
            (-1, 0) => Some(Dir::Left),
            (1, 0) => Some(Dir::Right),
            _ => None,
        }
    }
}

type Cell = u32;
type Pos = (i32, i32);

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
struct Map {
    grid: Vec<Cell>,
    size: (usize, usize),
    start: Pos,
    end: Pos,
}

fn traversal_to_positions(((start, end), path): ((Pos, Pos), Path)) -> Vec<Pos> {
    let mut traveled_positions = Vec::with_capacity(path.len() + 1);

    let mut current_pos = start;
    traveled_positions.push(current_pos);
    for &dir in path.iter() {
        current_pos = add_dir(current_pos, dir);
        traveled_positions.push(current_pos);
    }

    assert_eq!(current_pos, end);
    traveled_positions
}

impl Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.format(f, [])
    }
}

impl Map {
    fn format(
        &self,
        f: &mut impl Write,
        annotations: impl IntoIterator<Item = (Pos, char)>,
    ) -> fmt::Result {
        let annotations =
            IntoIterator::into_iter([(self.start, 'S'), (self.end, 'E')]).chain(annotations);

        self.format_from_scratch(f, annotations)
    }

    fn format_from_scratch(
        &self,
        f: &mut impl Write,
        annotations: impl IntoIterator<Item = (Pos, char)>,
    ) -> fmt::Result {
        let annotations_by_pos: HashMap<Pos, char> = annotations.into_iter().collect();

        for y in (0..self.size.1).rev() {
            for x in 0..self.size.0 {
                let ch =
                    if let Some(&annotated_char) = annotations_by_pos.get(&(x as i32, y as i32)) {
                        annotated_char
                    } else {
                        let cell = self.index((x as i32, y as i32)).unwrap();
                        char::from_u32(cell).unwrap()
                    };

                write!(f, "{}", ch)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }

    fn format_traversal(
        &self,
        f: &mut impl Write,
        ((start, end), path): ((Pos, Pos), &[Dir]),
    ) -> fmt::Result {
        let mut annotations = HashMap::with_capacity(self.size.0 * self.size.1);

        for (pos, _) in self.iter() {
            annotations.insert(pos, '.');
        }

        let mut current_pos = start;
        for &dir in path.iter() {
            annotations.insert(current_pos, dir.as_ascii_arrow());
            current_pos = add_dir(current_pos, dir);
        }
        assert_eq!(current_pos, end);
        annotations.insert(end, 'E');

        self.format_from_scratch(f, annotations)
    }

    fn iter<'a>(&'a self) -> impl Iterator<Item = (Pos, Cell)> + 'a {
        iter_positions(self.size).map(move |(x, y)| {
            let ch = self.index((x, y)).unwrap();
            ((x, y), ch)
        })
    }

    fn index(&self, (x, y): Pos) -> Option<Cell> {
        if x < 0 || y < 0 {
            return None;
        }
        let grid_index = x + y * self.size.0 as i32;
        self.grid.get(grid_index as usize).copied()
    }

    fn index_mut(&mut self, (x, y): Pos) -> Option<&mut Cell> {
        if x < 0 || y < 0 {
            return None;
        }
        let grid_index = x + y * self.size.0 as i32;
        self.grid.get_mut(grid_index as usize)
    }

    fn neighbors<'a>(&'a self, (x, y): Pos) -> impl Iterator<Item = Dir> + 'a {
        Dir::iter().filter(move |dir: &Dir| {
            let (x_off, y_off) = dir.as_offset();
            let pos = (x + x_off, y + y_off);
            self.index(pos).is_some()
        })
    }

    fn valid_neighbors<'a>(&'a self, (x, y): Pos) -> impl Iterator<Item = Dir> + 'a {
        let prev_height = self.index((x, y)).unwrap();
        Dir::iter().filter(move |dir: &Dir| {
            let (x_off, y_off) = dir.as_offset();
            let pos = (x + x_off, y + y_off);
            if let Some(height) = self.index(pos) {
                height <= prev_height + 1
            } else {
                false
            }
        })
    }
}

fn add((ax, ay): Pos, (bx, by): Pos) -> Pos {
    (ax + bx, ay + by)
}

fn add_dir((x, y): Pos, dir: Dir) -> Pos {
    add((x, y), dir.as_offset())
}

type Path = Rc<[Dir]>;

fn find_path_from_start_to_end(map: &Map) -> Path {
    let goal = (map.start, map.end);
    let (start, end) = goal;
    let shortest_paths: HashMap<(Pos, Pos), Path> =
        find_shortest_paths(map, |known_paths, _iteration| {
            known_paths.contains_key(&goal)
        });

    let final_path = shortest_paths.get(&goal).unwrap().clone();

    println!("Found path from start position {start:?} to end position {end:?}:");
    let mut buf = String::new();
    map.format_traversal(&mut buf, ((start, end), &final_path[..]))
        .unwrap();
    print!("{}", buf);

    final_path
}

fn find_path_from_a_to_end(map: &Map) -> Path {
    let valid_starts = map.iter().filter_map(
        |(pos, cell)| {
            if cell == 'a' as u32 {
                Some(pos)
            } else {
                None
            }
        },
    );
    let end = map.end;
    let valid_goals: Vec<(Pos, Pos)> = valid_starts.map(|start| (start, end)).collect();

    let shortest_paths: HashMap<(Pos, Pos), Path> =
        find_shortest_paths(map, |known_paths, iteration| {
            iteration % 10 == 0
                && valid_goals
                    .iter()
                    .any(|start_end| known_paths.contains_key(start_end))
        });

    let (start, final_path) = valid_goals
        .iter()
        .filter_map(|&(start, end)| -> Option<(Pos, Path)> {
            shortest_paths
                .get(&(start, end))
                .and_then(|path| Some((start, path.clone())))
        })
        .min_by_key(|(_, path)| path.len())
        .expect("No cells at elevation 'a' have a path to the end");

    println!("Found path from start position {start:?} to end position {end:?}:");
    let mut buf = String::new();
    map.format_traversal(&mut buf, ((start, end), &final_path[..]))
        .unwrap();
    print!("{}", buf);

    final_path
}

fn find_shortest_paths<F>(map: &Map, early_exit_callback: F) -> HashMap<(Pos, Pos), Path>
where
    F: Fn(&HashMap<(Pos, Pos), Path>, usize) -> bool,
{
    let mut buf = String::new();
    map.format(&mut buf, []).unwrap();
    println!("Finding paths for map:");
    print!("{buf}");
    println!();

    let mut known_paths: HashMap<(Pos, Pos), Path> = HashMap::new();

    // Start by filling the hashmap with the paths between adjacent positions.
    for ((x, y), _cell) in map.iter() {
        for dir in map.valid_neighbors((x, y)) {
            let start = (x, y);
            let end = add_dir(start, dir);
            known_paths.insert((start, end), vec![dir].into());
        }
    }

    let mut last_paths_added: HashMap<(Pos, Pos), Path> = known_paths.clone();
    let mut new_paths_to_add: Vec<((Pos, Pos), Path)> = Vec::new();

    for iteration in 0.. {
        println!(
            "Beginning iteration {iteration} with {} known paths",
            known_paths.len(),
        );

        // Increase path max length by extending existing paths one more step.
        for ((start, end), path) in last_paths_added.drain() {
            for dir in map.valid_neighbors(end) {
                let new_end = add_dir(end, dir);
                let mut new_path = Vec::from(&path[..]);
                new_path.push(dir);

                new_paths_to_add.push(((start, new_end), new_path.into()));
            }
        }

        for ((start, end), new_path) in new_paths_to_add.drain(..) {
            if let Some(prev_path) = known_paths.get_mut(&(start, end)) {
                let new_steps_needed = new_path.len();
                let prev_steps_needed = prev_path.len();
                if new_steps_needed < prev_steps_needed {
                    verbose_println!(
                        "Found shorter path between {start:?} and {end:?} with length {new_steps_needed}"
                    );
                    last_paths_added.insert((start, end), new_path.clone());
                    *prev_path = new_path;
                }
            } else {
                if DEBUG {
                    let new_steps_needed = new_path.len();
                    verbose_println!(
                        "Found new path between {start:?} and {end:?} with length {new_steps_needed}"
                    );
                }
                last_paths_added.insert((start, end), new_path.clone());
                known_paths.insert((start, end), new_path);
            }
        }

        if last_paths_added.len() == 0 {
            break;
        }

        if early_exit_callback(&known_paths, iteration) {
            break;
        }
    }

    println!();
    println!("Done!");
    println!();

    known_paths
}

#[test]
fn non_empty() {
    for input in [example_input(), input()] {
        assert_ne!(input.grid.len(), 0);
    }
}

#[test]
fn correct_start_and_end() {
    for input in [example_input(), input()] {
        let start = input.start;
        let end = input.end;

        let start_cell = input.index(start).unwrap();
        let end_cell = input.index(end).unwrap();

        assert_eq!(start_cell, 'a' as u32);
        assert_eq!(end_cell, 'z' as u32);
    }
}

#[test]
fn displayed_correctly() {
    for rows in [
        common::input_rows("input/day12-example"),
        common::input_rows("input/day12"),
    ] {
        let rows: Vec<_> = rows.collect();

        let map = input_from_rows(rows.iter().cloned());
        eprintln!("Expected:");
        for expected in rows.iter() {
            eprintln!("{}", expected);
        }

        let display = format!("{}", map);
        let rows_displayed: Vec<_> = display.lines().collect();
        eprintln!("Actual:");
        for actual in rows_displayed.iter() {
            eprintln!("{}", actual);
        }

        for (expected, actual) in rows.iter().zip(rows_displayed.iter()) {
            eprintln!("Expected: {}", expected);
            eprintln!("Actual:   {}", actual);
            eprintln!();
            assert_eq!(actual, expected);
        }
    }
}

#[test]
fn correct_neighbors() {
    let map = example_input();

    assert_eq!(
        map.neighbors((2, 2)).collect::<Vec<_>>(),
        vec![Dir::Up, Dir::Down, Dir::Left, Dir::Right]
    );

    assert_eq!(
        map.neighbors((0, 0)).collect::<Vec<_>>(),
        vec![Dir::Up, Dir::Right]
    );
}

#[test]
fn part1_example() {
    let map = example_input();

    assert_eq!(find_path_from_start_to_end(&map).len(), 31);
}

#[test]
fn part1_actual() {
    let map = input();

    assert_eq!(find_path_from_start_to_end(&map).len(), 330);
}

#[test]
fn part2_example() {
    let map = example_input();

    assert_eq!(find_path_from_a_to_end(&map).len(), 29);
}

#[test]
fn part2_actual() {
    let map = input();

    assert_eq!(find_path_from_a_to_end(&map).len(), 321);
}
