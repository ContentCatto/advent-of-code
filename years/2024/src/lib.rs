#![feature(array_chunks)]
#![feature(array_windows)]
#![feature(iter_array_chunks)]
#![feature(iterator_try_collect)]
#![feature(round_char_boundary)]
#![feature(pattern)]
#![feature(strict_overflow_ops)]
#![feature(fn_traits)]
#![feature(unboxed_closures)]
#![feature(test)]
extern crate test;

extern crate anstyle; // ????
extern crate arrayvec; // ????
extern crate common; // ????
extern crate derive_display_from_debug; // ????
extern crate lazy_static;
extern crate log; // ????
extern crate num; // ????
extern crate once_cell; // ????
extern crate random; // ???? // ????

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
