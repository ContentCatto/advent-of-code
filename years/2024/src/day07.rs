use num::Integer;

fn input() -> Input {
    input_from_rows(common::input_rows("input/day07"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day07-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.map(|row| {
        let (result, operands) = row
            .split_once(": ")
            .expect("Row is missing colon separating the result from the operators");
        let result = result.parse().expect("Invalid result");
        let operands = operands
            .split(' ')
            .map(|operand| operand.parse().expect("Invalid operand"))
            .collect();
        (result, operands)
    })
    .collect()
}

type Operands = Vec<u64>;
type UnfilledEquation = (u64, Operands);

type Input = Vec<UnfilledEquation>;

type OpList = Vec<Op>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Op {
    Add,
    Multiply,
    Concat,
}

impl Op {
    pub fn perform(self, lhs: u64, rhs: u64) -> u64 {
        match self {
            Op::Add => u64::strict_add(lhs, rhs),
            Op::Multiply => u64::strict_mul(lhs, rhs),
            Op::Concat => lhs * 10u64.pow(rhs.ilog10() + 1) + rhs,
        }
    }
}

fn possible_ops<'p>(
    num_operands: usize,
    possible_ops: &'p [Op],
) -> impl Iterator<Item = OpList> + 'p {
    let num_operations = num_operands - 1;
    let num_possible_options = possible_ops.len().pow(
        num_operations
            .try_into()
            .expect("The number of operands should be reasonable"),
    );
    (0..num_possible_options).map(move |mut discriminant| {
        let mut ops = Vec::with_capacity(num_operations);
        while ops.len() < num_operations {
            let new_op_id;
            (discriminant, new_op_id) = discriminant.div_mod_floor(&possible_ops.len());
            let new_op = possible_ops[new_op_id];
            ops.push(new_op);
        }
        ops
    })
}

fn is_equation_true(expected_result: u64, operands: &Operands, operators: OpList) -> bool {
    let mut current_value = operands[0];
    for (&operand, operator) in operands[1..].iter().zip(operators) {
        current_value = operator.perform(current_value, operand);
    }
    current_value == expected_result
}

fn sum_possibly_true_test_equations_from_valid_ops(
    unfilled_equations: Input,
    valid_ops: &[Op],
) -> u64 {
    unfilled_equations
        .into_iter()
        .filter(|(expected_result, operands)| {
            possible_ops(operands.len(), valid_ops)
                .any(|ops| is_equation_true(*expected_result, operands, ops))
        })
        .map(|(expected_result, _operands)| expected_result)
        .sum()
}

fn part1_sum_possibly_true_test_equations(unfilled_equations: Input) -> u64 {
    sum_possibly_true_test_equations_from_valid_ops(unfilled_equations, &[Op::Add, Op::Multiply])
}

fn part2_sum_possibly_true_test_equations(unfilled_equations: Input) -> u64 {
    sum_possibly_true_test_equations_from_valid_ops(
        unfilled_equations,
        &[Op::Add, Op::Multiply, Op::Concat],
    )
}

#[test]
fn correct_possible_ops_with_2_options() {
    let ops: Vec<OpList> = possible_ops(4, &[Op::Add, Op::Multiply]).collect();
    let expected = vec![
        vec![Op::Add, Op::Add, Op::Add],
        vec![Op::Multiply, Op::Add, Op::Add],
        vec![Op::Add, Op::Multiply, Op::Add],
        vec![Op::Multiply, Op::Multiply, Op::Add],
        vec![Op::Add, Op::Add, Op::Multiply],
        vec![Op::Multiply, Op::Add, Op::Multiply],
        vec![Op::Add, Op::Multiply, Op::Multiply],
        vec![Op::Multiply, Op::Multiply, Op::Multiply],
    ];
    assert_eq!(ops, expected);
}

#[test]
fn correct_possible_ops_with_3_options() {
    let ops: Vec<OpList> = possible_ops(3, &[Op::Add, Op::Multiply, Op::Concat]).collect();
    let expected = vec![
        vec![Op::Add, Op::Add],
        vec![Op::Multiply, Op::Add],
        vec![Op::Concat, Op::Add],
        vec![Op::Add, Op::Multiply],
        vec![Op::Multiply, Op::Multiply],
        vec![Op::Concat, Op::Multiply],
        vec![Op::Add, Op::Concat],
        vec![Op::Multiply, Op::Concat],
        vec![Op::Concat, Op::Concat],
    ];
    assert_eq!(ops, expected);
}

#[test]
fn part1_example() {
    let input = example_input();
    let result = part1_sum_possibly_true_test_equations(input);
    assert_eq!(result, 3749);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_sum_possibly_true_test_equations(input);
    assert_eq!(result, 10741443549536);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_sum_possibly_true_test_equations(input);
    assert_eq!(result, 11387);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_sum_possibly_true_test_equations(input);
    assert_eq!(result, 500335179214836);
}
