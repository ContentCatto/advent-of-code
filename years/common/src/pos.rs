use std::{
    fmt::Display,
    num::TryFromIntError,
    ops::{Add, Sub},
};

use random::{Source, Value};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Pos(pub isize, pub isize);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct UPos(pub usize, pub usize);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Size2D(pub usize, pub usize);

impl Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{},{}", self.0, self.1))
    }
}

impl Display for UPos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{},{}", self.0, self.1))
    }
}

impl Value for Pos {
    fn read<S>(source: &mut S) -> Self
    where
        S: Source,
    {
        Pos(Value::read(source), Value::read(source))
    }
}

impl Value for UPos {
    fn read<S>(source: &mut S) -> Self
    where
        S: Source,
    {
        UPos(Value::read(source), Value::read(source))
    }
}

impl Pos {
    pub fn into_tuple(self) -> (isize, isize) {
        self.into()
    }

    pub fn from_tuple(tup: (isize, isize)) -> Self {
        tup.into()
    }

    /// Parses `"2,-18"` as `Pos(2, -18)`.
    pub fn try_from_string_no_parens(string: &str) -> Result<Pos, String> {
        let Some((x_str, y_str)) = string.split_once(',') else {
            return Err(format!("No comma in input: {:?}", string));
        };

        if y_str.contains(',') {
            return Err(format!("More than 1 comma in input: {:?}", string));
        }

        let x = x_str
            .parse()
            .map_err(|err| format!("Error parsing x coordinate: {:?}", err))?;
        let y = y_str
            .parse()
            .map_err(|err| format!("Error parsing y coordinate: {:?}", err))?;

        Ok(Pos(x, y))
    }

    /// Parses `"x=2,y=-18"` as `Pos(2, -18)`.
    pub fn try_from_string_with_explicit_xy_denominators(string: &str) -> Result<Pos, String> {
        let Some((x_str, y_str)) = string.split_once(',') else {
            return Err(format!("No comma in input: {:?}", string));
        };

        if y_str.contains(',') {
            return Err(format!("More than 1 comma in input: {:?}", string));
        }

        let Some(x_str) = x_str.strip_prefix("x=") else {
            return Err(format!(
                "X coordinate is not denominated with \"x=\": {:?}",
                string
            ));
        };

        let Some(y_str) = y_str.strip_prefix("y=") else {
            return Err(format!(
                "Y coordinate is not denominated with \"y=\": {:?}",
                string
            ));
        };

        let x = x_str
            .parse()
            .map_err(|err| format!("Error parsing x coordinate: {:?}", err))?;
        let y = y_str
            .parse()
            .map_err(|err| format!("Error parsing y coordinate: {:?}", err))?;

        Ok(Pos(x, y))
    }

    pub fn to_x(&self) -> isize {
        self.0
    }

    pub fn to_y(&self) -> isize {
        self.1
    }

    pub fn to_x_owned(self) -> isize {
        self.0
    }

    pub fn to_y_owned(self) -> isize {
        self.1
    }

    pub fn hamming_distance(self, other: Pos) -> u64 {
        let Pos(x1, y1) = self;
        let Pos(x2, y2) = other;

        x1.abs_diff(x2) as u64 + y1.abs_diff(y2) as u64
    }
}

impl UPos {
    pub fn into_tuple(self) -> (usize, usize) {
        self.into()
    }

    pub fn from_tuple(tup: (usize, usize)) -> Self {
        tup.into()
    }
}

impl From<Pos> for (isize, isize) {
    fn from(Pos(x, y): Pos) -> Self {
        (x, y)
    }
}

impl From<(isize, isize)> for Pos {
    fn from(tup: (isize, isize)) -> Self {
        Self::from_tuple(tup)
    }
}

impl From<UPos> for (usize, usize) {
    fn from(UPos(x, y): UPos) -> Self {
        (x, y)
    }
}

impl From<(usize, usize)> for UPos {
    fn from(tup: (usize, usize)) -> Self {
        Self::from_tuple(tup)
    }
}

impl From<Pos> for UPos {
    fn from(Pos(x, y): Pos) -> Self {
        UPos(x.try_into().unwrap(), y.try_into().unwrap())
    }
}

impl TryFrom<UPos> for Pos {
    type Error = TryFromIntError;

    fn try_from(UPos(x, y): UPos) -> Result<Self, TryFromIntError> {
        Ok(Pos(x.try_into()?, y.try_into()?))
    }
}

impl Add<Pos> for Pos {
    type Output = Pos;

    fn add(self, Pos(x2, y2): Pos) -> Self::Output {
        let Pos(x1, y1) = self;
        Pos(x1 + x2, y1 + y2)
    }
}

impl Sub<Pos> for Pos {
    type Output = Pos;

    fn sub(self, Pos(x2, y2): Pos) -> Self::Output {
        let Pos(x1, y1) = self;
        Pos(x1 - x2, y1 - y2)
    }
}

impl Add<(usize, usize)> for UPos {
    type Output = UPos;

    fn add(self, (x2, y2): (usize, usize)) -> Self::Output {
        let UPos(x1, y1) = self;
        UPos(x1 + x2, y1 + y2)
    }
}

impl Sub<(usize, usize)> for UPos {
    type Output = UPos;

    fn sub(self, (x2, y2): (usize, usize)) -> Self::Output {
        let UPos(x1, y1) = self;
        UPos(x1 - x2, y1 - y2)
    }
}

impl Add<(isize, isize)> for Pos {
    type Output = Pos;

    fn add(self, (x2, y2): (isize, isize)) -> Self::Output {
        let Pos(x1, y1) = self;
        let x = x1 + x2;
        let y = y1 + y2;
        Pos(x, y)
    }
}

impl Sub<(isize, isize)> for Pos {
    type Output = Pos;

    fn sub(self, (x2, y2): (isize, isize)) -> Self::Output {
        let Pos(x1, y1) = self;
        let x = x1 - x2;
        let y = y1 - y2;
        Pos(x, y)
    }
}

pub trait IntoBounds<T> {
    fn into_bounds(&self) -> Option<(T, T)>;
}

impl<T> IntoBounds<Pos> for T
where
    T: Iterator<Item = Pos> + Clone,
{
    fn into_bounds(&self) -> Option<(Pos, Pos)> {
        let mentioned_x_coordinates: Vec<isize> = self.clone().map(Pos::to_x_owned).collect();
        let mentioned_y_coordinates: Vec<isize> = self.clone().map(Pos::to_y_owned).collect();

        if mentioned_x_coordinates.is_empty() {
            return None;
        }

        let min_x = mentioned_x_coordinates.iter().cloned().min().unwrap();
        let max_x = mentioned_x_coordinates.iter().cloned().max().unwrap();
        let min_y = mentioned_y_coordinates.iter().cloned().min().unwrap();
        let max_y = mentioned_y_coordinates.iter().cloned().max().unwrap();

        let min_pos = Pos(min_x, min_y);
        let max_pos = Pos(max_x, max_y);

        Some((min_pos, max_pos))
    }
}
