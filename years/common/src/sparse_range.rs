use std::cmp::Ordering;
use std::fmt::Debug;
use std::ops::{Add, Bound, Range, RangeBounds, Sub, SubAssign};

#[derive(Clone, Debug, Default)]
pub struct SparseRange<Idx> {
    // TODO: Add `overall_bounds: Range<Idx>` field for optimizations.
    sub_ranges: Vec<Range<Idx>>,
}

impl<Idx> SparseRange<Idx> {
    pub fn new() -> Self {
        Self {
            sub_ranges: Vec::new(),
        }
    }

    pub fn sub_ranges(&self) -> &[Range<Idx>] {
        &self.sub_ranges
    }

    pub fn contains<OthIdx>(&self, index: &OthIdx) -> bool
    where
        Idx: PartialOrd<OthIdx>,
        OthIdx: ?Sized + PartialOrd<Idx>,
    {
        self.sub_ranges
            .iter()
            .any(|sub_range| sub_range.contains(index))
    }

    pub fn add<R>(&mut self, new_range: R)
    where
        R: Into<Range<Idx>>,
        Range<Idx>: RangeIntersect<Idx, Range<Idx>> + Clone + Eq + Debug,
        // TODO: Change to only require Clone.
        Idx: Eq + Copy + Debug,
    {
        let new_range = new_range.into();
        #[cfg(test)]
        #[cfg(debug_assertions)]
        dbg!(&self.sub_ranges, &new_range);
        if new_range.start == new_range.end {
            return;
        }
        let mut coalesce: Option<(usize, Range<Idx>)> = None;

        for index in (0..self.sub_ranges.len()).rev() {
            let Some(contact) = self.sub_ranges[index].contact(&new_range) else {
                continue;
            };
            let current = &mut self.sub_ranges[index];
            match contact {
                Contact::Intersection(overlap) => {
                    if &overlap == current {
                        let _removed = self.sub_ranges.swap_remove(index);
                        if let Some((coalesce_index, coalesce_range)) = coalesce {
                            if coalesce_index == self.sub_ranges.len() {
                                coalesce = Some((index, coalesce_range));
                            } else {
                                coalesce = Some((coalesce_index, coalesce_range));
                            }
                        }
                        #[cfg(test)]
                        eprintln!(
                            "Range {_removed:?} is encompassed completely by new range {new_range:?} (case 1)"
                        );
                        continue;
                    }

                    if current.end == overlap.end {
                        if let Some((coalesce_index, mut coalesce_range)) = coalesce {
                            #[cfg(test)]
                            eprintln!("Coalescing range {coalesce_range:?} into range {current:?} (case 2)");
                            current.end = coalesce_range.end;
                            coalesce_range.clone_from(&current);
                            self.sub_ranges.swap_remove(coalesce_index);
                            coalesce = Some((index, coalesce_range));
                        } else {
                            #[cfg(test)]
                            eprintln!(
                                "Coalescing range {new_range:?} into range {current:?} (case 3)"
                            );
                            current.end = new_range.end;
                            coalesce = Some((index, current.clone()));
                        }
                    } else if current.start == overlap.start {
                        if let Some((coalesce_index, mut coalesce_range)) = coalesce {
                            #[cfg(test)]
                            eprintln!("Coalescing range {coalesce_range:?} into range {current:?} (case 4)");
                            current.start = coalesce_range.start;
                            coalesce_range.clone_from(&current);
                            self.sub_ranges.swap_remove(coalesce_index);
                            coalesce = Some((index, coalesce_range));
                        } else {
                            #[cfg(test)]
                            eprintln!(
                                "Coalescing range {new_range:?} into range {current:?} (case 5)"
                            );
                            current.start = new_range.start;
                            coalesce = Some((index, current.clone()));
                        }
                    } else {
                        #[cfg(test)]
                        eprintln!(
                            "Existing range {current:?} covers all of the new range {new_range:?} (case 6)"
                        );
                        return;
                    }
                }
                Contact::Touch(touch) => {
                    if current.end == touch {
                        if let Some((coalesce_index, mut coalesce_range)) = coalesce {
                            #[cfg(test)]
                            eprintln!("Coalescing touching range {coalesce_range:?} into range {current:?} (case 7)");
                            current.end = coalesce_range.end;
                            coalesce_range.clone_from(&current);
                            self.sub_ranges.swap_remove(coalesce_index);
                            coalesce = Some((index, coalesce_range));
                        } else {
                            #[cfg(test)]
                            eprintln!(
                                "Coalescing touching range {new_range:?} into range {current:?} (case 8)"
                            );
                            current.end = new_range.end;
                            coalesce = Some((index, current.clone()));
                        }
                    } else if current.start == touch {
                        if let Some((coalesce_index, mut coalesce_range)) = coalesce {
                            #[cfg(test)]
                            eprintln!("Coalescing touching range {coalesce_range:?} into range {current:?} (case 9)");
                            current.start = coalesce_range.start;
                            coalesce_range.clone_from(&current);
                            self.sub_ranges.swap_remove(coalesce_index);
                            coalesce = Some((index, coalesce_range));
                        } else {
                            #[cfg(test)]
                            eprintln!(
                                "Coalescing touching range {new_range:?} into range {current:?} (case 10)"
                            );
                            current.start = new_range.start;
                            coalesce = Some((index, current.clone()));
                        }
                    } else {
                        unreachable!()
                    }
                }
            }
        }

        if coalesce.is_none() {
            #[cfg(test)]
            eprintln!("No existing ranges touch the new range {new_range:?} (case 11)");
            self.sub_ranges.push(new_range);
        }
    }

    pub fn remove_range<'a, R>(&mut self, range_to_remove: R)
    where
        R: Into<&'a Range<Idx>>,
        Idx: 'a + Eq + Clone,
        Range<Idx>: RangeIntersect<Idx, Range<Idx>> + Eq + Clone,
    {
        let range_to_remove = range_to_remove.into();
        if range_to_remove.start == range_to_remove.end {
            return;
        }

        for index in (0..self.sub_ranges.len()).rev() {
            let Some(intersection) = self.sub_ranges[index].intersection(range_to_remove) else {
                continue;
            };
            let current = &mut self.sub_ranges[index];
            if current == &intersection {
                self.sub_ranges.swap_remove(index);
            } else if intersection.start == current.start {
                current.start = intersection.end;
            } else if intersection.end == current.end {
                current.end = intersection.start;
            } else {
                let mut new = range_to_remove.clone();
                new.start = range_to_remove.end.clone();
                new.end = current.end.clone();
                current.end = intersection.start;
                self.sub_ranges.push(new);
                return;
            }
        }
    }

    pub fn sort(&mut self)
    where
        Idx: Ord,
        bool: Into<Idx>,
        for<'a> &'a Idx: Add<Output = Idx>,
    {
        sort_ranges(&mut self.sub_ranges);
    }

    pub fn equals(&mut self, rhs: &mut Self) -> bool
    where
        Idx: Ord,
        bool: Into<Idx>,
        for<'a> &'a Idx: Add<Output = Idx>,
    {
        self.sort();
        rhs.sort();
        self.sub_ranges == rhs.sub_ranges
    }

    #[cfg(test)]
    fn sanity_check_invariants(&self)
    where
        SparseRange<Idx>: Clone,
        Idx: Ord,
        bool: Into<Idx>,
        for<'a> &'a Idx: Add<Output = Idx>,
    {
        let mut sparse_range = self.clone();
        sparse_range.sort();
        let mut sub_ranges = sparse_range.sub_ranges.into_iter();
        let Some(prev_range) = sub_ranges.next() else {
            return;
        };
        for range in sub_ranges {
            assert!(range.start > prev_range.end);
        }
    }
}

impl<Idx, R> From<R> for SparseRange<Idx>
where
    R: Into<Range<Idx>>,
{
    fn from(value: R) -> Self {
        Self {
            sub_ranges: vec![value.into()],
        }
    }
}

impl<Idx, R> FromIterator<R> for SparseRange<Idx>
where
    R: Into<Range<Idx>>,
    Range<Idx>: RangeIntersect<Idx, Range<Idx>> + Eq + Clone + Debug,
    // TODO: Change to only require Clone.
    Idx: Eq + Copy + Debug,
{
    fn from_iter<T: IntoIterator<Item = R>>(iter: T) -> Self {
        let mut sparse_range = SparseRange::new();
        for range in iter {
            sparse_range.add(range);
        }
        sparse_range
    }
}

impl<Idx> SubAssign<&Self> for SparseRange<Idx>
where
    Idx: Copy + Eq + Ord,
{
    fn sub_assign(&mut self, rhs: &Self) {
        for sub_range in &rhs.sub_ranges {
            self.remove_range(sub_range);
        }
    }
}

impl<'a, Idx> Sub for &'a SparseRange<Idx>
where
    SparseRange<Idx>: Clone,
    Idx: Copy + Eq + Ord,
{
    type Output = SparseRange<Idx>;

    fn sub(self: &'a SparseRange<Idx>, rhs: &SparseRange<Idx>) -> Self::Output {
        let mut new = SparseRange::clone(self);
        new.sub_assign(rhs);
        new
    }
}

pub enum Contact<R, Idx> {
    Intersection(R),
    Touch(Idx),
}

pub trait RangeIntersect<Idx, Rhs>
where
    Self: RangeBounds<Idx> + Sized,
    Rhs: RangeBounds<Idx>,
{
    fn intersection(&self, rhs: &Rhs) -> Option<Self>;
    fn contact(&self, rhs: &Rhs) -> Option<Contact<Self, Idx>>;
}

impl<Idx> RangeIntersect<Idx, Range<Idx>> for Range<Idx>
where
    Idx: Copy + Ord,
{
    fn intersection(&self, rhs: &Range<Idx>) -> Option<Self> {
        let (left, right) = if self.start <= rhs.start {
            (self, rhs)
        } else {
            (rhs, self)
        };

        let start = left.start.max(right.start);
        let end = left.end.min(right.end);

        if start < end {
            Some(start..end)
        } else {
            None
        }
    }

    fn contact(&self, rhs: &Range<Idx>) -> Option<Contact<Range<Idx>, Idx>> {
        let (left, right) = if self.start <= rhs.start {
            (self, rhs)
        } else {
            (rhs, self)
        };

        let start = left.start.max(right.start);
        let end = left.end.min(right.end);

        if start == end {
            Some(Contact::Touch(start))
        } else if start < end {
            Some(Contact::Intersection(start..end))
        } else {
            None
        }
    }
}

pub fn sort_ranges<Idx, R>(ranges: &mut [R])
where
    R: RangeBounds<Idx>,
    Idx: Ord,
    bool: Into<Idx>,
    for<'a> &'a Idx: Add<Output = Idx>,
{
    ranges.sort_by(|a, b| {
        match (
            range_bound_ordering(a.start_bound(), b.start_bound(), false),
            range_bound_ordering(a.end_bound(), b.end_bound(), true),
        ) {
            (Ordering::Less, _) => Ordering::Less,
            (Ordering::Equal, ordering) => ordering,
            (Ordering::Greater, _) => Ordering::Greater,
        }
    });
}

pub fn range_bound_ordering<Idx>(a: Bound<&Idx>, b: Bound<&Idx>, is_end_bound: bool) -> Ordering
where
    bool: Into<Idx>,
    Idx: Ord,
    for<'a> &'a Idx: Add<Output = Idx>,
{
    let one: Idx = true.into();
    match (a, b, is_end_bound) {
        (Bound::Unbounded, Bound::Unbounded, _) => Ordering::Equal,

        (_, Bound::Unbounded, false) => Ordering::Greater,
        (_, Bound::Unbounded, true) => Ordering::Less,

        (Bound::Unbounded, _, false) => Ordering::Less,
        (Bound::Unbounded, _, true) => Ordering::Greater,

        (Bound::Included(a), Bound::Included(b), _) => a.cmp(&b),
        (Bound::Excluded(a), Bound::Excluded(b), _) => a.cmp(&b),

        // [6 (5 -> [6 [6
        (Bound::Included(a), Bound::Excluded(b), false) => a.cmp(&(b + &one)),
        // 5] 6) -> 6] 6]
        (Bound::Included(a), Bound::Excluded(b), true) => (a + &one).cmp(&b),

        // (5 [6 -> [6 [6
        (Bound::Excluded(a), Bound::Included(b), false) => (a + &one).cmp(&b),
        // 6) 5] -> 6] 6]
        (Bound::Excluded(a), Bound::Included(b), true) => a.cmp(&(b + &one)),
    }
}

#[cfg(test)]
mod tests {
    use crate::combinatorics::permutations;

    use super::{RangeIntersect, SparseRange};

    #[test]
    fn range_overlaps() {
        assert_eq!((1..5).intersection(&(3..7)), Some(3..5));
        assert_eq!((3..7).intersection(&(1..5)), Some(3..5));
    }

    #[test]
    fn range_overlaps_by_one() {
        assert_eq!((1..5).intersection(&(4..7)), Some(4..5));
        assert_eq!((4..7).intersection(&(1..5)), Some(4..5));
    }

    #[test]
    fn range_touches() {
        assert_eq!((1..5).intersection(&(5..7)), None);
        assert_eq!((5..7).intersection(&(1..5)), None);
    }

    #[test]
    fn range_doesnt_even_touch() {
        assert_eq!((1..3).intersection(&(5..7)), None);
        assert_eq!((5..7).intersection(&(1..3)), None);
    }

    #[test]
    fn sparse_range_no_overlaps() {
        for ranges in permutations([0..3, 4..7, 10..11]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(sparse_range.contains(&2));
            assert!(!sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(!sparse_range.contains(&7));
            assert!(!sparse_range.contains(&8));
            assert!(!sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(!sparse_range.contains(&11));
            assert!(!sparse_range.contains(&12));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..3, 4..7, 10..11]);
        }
    }

    #[test]
    fn sparse_range_some_touching() {
        for ranges in permutations([0..4, 4..7, 10..11]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(sparse_range.contains(&2));
            assert!(sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(!sparse_range.contains(&7));
            assert!(!sparse_range.contains(&8));
            assert!(!sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(!sparse_range.contains(&11));
            assert!(!sparse_range.contains(&12));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..7, 10..11]);
        }
    }

    #[test]
    fn sparse_range_some_overlaps() {
        for ranges in permutations([0..3, 4..7, 5..12, 10..13]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(sparse_range.contains(&2));
            assert!(!sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(sparse_range.contains(&7));
            assert!(sparse_range.contains(&8));
            assert!(sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(sparse_range.contains(&11));
            assert!(sparse_range.contains(&12));
            assert!(!sparse_range.contains(&13));
            assert!(!sparse_range.contains(&14));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..3, 4..13]);
        }
    }

    #[test]
    fn sparse_range_lots_of_overlaps() {
        for ranges in permutations([0..5, 4..7, 5..12, 10..13]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range: SparseRange<i32> = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(sparse_range.contains(&2));
            assert!(sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(sparse_range.contains(&7));
            assert!(sparse_range.contains(&8));
            assert!(sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(sparse_range.contains(&11));
            assert!(sparse_range.contains(&12));
            assert!(!sparse_range.contains(&13));
            assert!(!sparse_range.contains(&14));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..13]);
        }
    }

    #[test]
    fn sparse_range_just_touching() {
        for ranges in permutations([0..4, 4..7, 7..10, 10..13]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range: SparseRange<i32> = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(sparse_range.contains(&2));
            assert!(sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(sparse_range.contains(&7));
            assert!(sparse_range.contains(&8));
            assert!(sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(sparse_range.contains(&11));
            assert!(sparse_range.contains(&12));
            assert!(!sparse_range.contains(&13));
            assert!(!sparse_range.contains(&14));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..13]);
        }
    }

    #[test]
    fn sparse_range_with_empty() {
        for ranges in permutations([0..2, 3..3, 4..7, 5..12, 10..13]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range = SparseRange::from_iter(ranges);
            assert!(!sparse_range.contains(&-1));
            assert!(sparse_range.contains(&0));
            assert!(sparse_range.contains(&1));
            assert!(!sparse_range.contains(&2));
            assert!(!sparse_range.contains(&3));
            assert!(sparse_range.contains(&4));
            assert!(sparse_range.contains(&5));
            assert!(sparse_range.contains(&6));
            assert!(sparse_range.contains(&7));
            assert!(sparse_range.contains(&8));
            assert!(sparse_range.contains(&9));
            assert!(sparse_range.contains(&10));
            assert!(sparse_range.contains(&11));
            assert!(sparse_range.contains(&12));
            assert!(!sparse_range.contains(&13));
            assert!(!sparse_range.contains(&14));

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..2, 4..13]);
        }
    }

    #[test]
    fn sparse_range_very_many_ranges() {
        for ranges in permutations([0..3, 4..7, 5..12, 10..13, 16..18, 18..19, 20..22, 5..18]) {
            eprintln!("Ranges are {ranges:?}");
            let sparse_range = SparseRange::from_iter(ranges);

            let mut sparse_range = sparse_range;
            sparse_range.sanity_check_invariants();
            sparse_range.sort();
            assert_eq!(sparse_range.sub_ranges, [0..3, 4..19, 20..22]);
        }
    }
}
