fn input() -> Input {
    input_from_rows(common::input_rows("input/day02"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day02-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.map(|row| {
        row.split_whitespace()
            .map(str::parse)
            .collect::<Result<_, _>>()
            .unwrap_or_else(|error| panic!("Invalid row {row:?}: {error}"))
    })
    .collect()
}

type Level = u64;
type Report = Vec<Level>;

type Input = Vec<Report>;

fn level_to_i64(level: Level) -> i64 {
    level
        .try_into()
        .expect("Casting a level to a i64 shouldn't wrap")
}

fn is_increasing(levels: impl AsRef<[Level]>, difference: impl Fn(i64, i64) -> i64) -> bool {
    let levels = levels.as_ref();
    levels.array_windows::<2>().all(|&[left, right]| {
        (1..=3).contains(&difference(level_to_i64(left), level_to_i64(right)))
    })
}

fn report_with_up_to_one_level_removed(levels: &[Level]) -> impl Iterator<Item = Vec<Level>> + '_ {
    (0..levels.len())
        .map(|index_to_remove| {
            let mut levels = levels.to_vec();
            levels.remove(index_to_remove);
            levels
        })
        .chain(std::iter::once(levels.to_vec()))
}

fn is_safe_without_removing_a_level(levels: impl AsRef<[Level]>) -> bool {
    let levels = levels.as_ref();
    is_increasing(levels, |left, right| left - right)
        || is_increasing(levels, |left, right| right - left)
}

fn is_safe_with_up_to_one_level_removed(levels: impl AsRef<[Level]>) -> bool {
    report_with_up_to_one_level_removed(levels.as_ref()).any(is_safe_without_removing_a_level)
}

fn part1_count_safe(reports: Input) -> u64 {
    reports
        .into_iter()
        .filter(|report| is_safe_without_removing_a_level(report))
        .count() as u64
}

fn part2_count_safe_with_up_to_one_level_removed(reports: Input) -> u64 {
    reports
        .into_iter()
        .filter(|report| is_safe_with_up_to_one_level_removed(report))
        .count() as u64
}

#[test]
fn parse_example() {
    let reports = example_input();
    #[rustfmt::skip]
    let expected = vec![
        vec![7, 6, 4, 2, 1],
        vec![1, 2, 7, 8, 9],
        vec![9, 7, 6, 2, 1],
        vec![1, 3, 2, 4, 5],
        vec![8, 6, 4, 4, 1],
        vec![1, 3, 6, 7, 9],
    ];
    assert_eq!(reports, expected);
}

#[test]
fn part1_example() {
    let reports = example_input();
    let num_safe = part1_count_safe(reports);
    assert_eq!(num_safe, 2);
}

#[test]
fn part1_real() {
    let reports = input();
    let num_safe = part1_count_safe(reports);
    assert_eq!(num_safe, 257);
}

#[test]
fn part2_example() {
    let reports = example_input();
    let num_safe = part2_count_safe_with_up_to_one_level_removed(reports);
    assert_eq!(num_safe, 4);
}

#[test]
fn part2_real() {
    let reports = input();
    let num_safe = part2_count_safe_with_up_to_one_level_removed(reports);
    assert_eq!(num_safe, 328);
}
