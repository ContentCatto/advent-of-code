use std::collections::HashMap;

use arrayvec::ArrayVec;

fn input() -> Input {
    input_from_rows(common::input_rows("input/day10"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day10-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.map(|row| {
        row.chars()
            .map(|ch| {
                (ch as u32 - '0' as u32)
                    .try_into()
                    .expect("Map positions should be digits")
            })
            .collect()
    })
    .collect()
}

type Map = Vec<Vec<u8>>;
type Input = Map;

type Pos = (usize, usize);
type Trail = ArrayVec<Pos, 10>;

fn neighbors(map: &Map, (x, y): Pos) -> impl Iterator<Item = Pos> {
    let map_size_x = map[0].len();
    let map_size_y = map.len();
    [
        (x as isize + 1, y as isize),
        (x as isize - 1, y as isize),
        (x as isize, y as isize + 1),
        (x as isize, y as isize - 1),
    ]
    .into_iter()
    .filter(move |&(x, y)| {
        (0..map_size_x as isize).contains(&x) && (0..map_size_y as isize).contains(&y)
    })
    .map(|(x, y)| (x as usize, y as usize))
}

fn hikeable_neighbors(map: &Map, (x, y): Pos) -> impl Iterator<Item = Pos> + '_ {
    let current_height = map[y][x];
    let target_height = current_height + 1;
    neighbors(map, (x, y)).filter(move |&(x, y)| map[y][x] == target_height)
}

fn continue_trail(map: &Map, current: Trail) -> impl Iterator<Item = Trail> + '_ {
    let last = *current.last().expect("Trail must start somewhere");
    hikeable_neighbors(map, last).map(move |next_step| {
        let mut after_next_step = current.clone();
        after_next_step.push(next_step);
        after_next_step
    })
}

fn find_trails_for_head(map: &Map, head: Pos) -> impl Iterator<Item = Trail> + '_ {
    let start: Trail = {
        let mut start = ArrayVec::new();
        start.push(head);
        start
    };

    continue_trail(map, start)
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .flat_map(|step| continue_trail(map, step))
        .inspect(|trail| {
            let (start_x, start_y) = trail[0];
            assert_eq!(map[start_y][start_x], 0);
            let (end_x, end_y) = trail[9];
            assert_eq!(map[end_y][end_x], 9);
            assert_eq!(trail.len(), 10);
        })
}

fn find_trails(map: &Map) -> impl Iterator<Item = Trail> + '_ {
    (0..map[0].len())
        .flat_map(|x| std::iter::repeat(x).zip(0..map.len()))
        .filter(|&(x, y)| map[y][x] == 0)
        .flat_map(|(x, y)| find_trails_for_head(map, (x, y)))
}

fn part1_score_trailheads(map: Map) -> u64 {
    let unique_trail_starts_and_ends: HashMap<(Pos, Pos), Trail> = find_trails(&map)
        .map(|trail| ((trail[0], trail[9]), trail))
        .collect();
    unique_trail_starts_and_ends.len() as u64
}

fn part2_count_trails(map: Map) -> u64 {
    find_trails(&map).count() as u64
}

#[test]
fn part1_example() {
    let input = example_input();
    let result = part1_score_trailheads(input);
    assert_eq!(result, 36);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_score_trailheads(input);
    assert_eq!(result, 822);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_count_trails(input);
    assert_eq!(result, 81);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_count_trails(input);
    assert_eq!(result, 1801);
}
