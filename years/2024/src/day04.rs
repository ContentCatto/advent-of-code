use regex::Regex;
use regex_automata::{nfa::thompson, util::syntax, MatchKind};
use test::Bencher;

fn input() -> Input {
    input_from_rows(common::input_rows("input/day04"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day04-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.map(|row| row.chars().collect()).collect()
}

type Input = Vec<Vec<char>>;

fn substrings_from_stride<'a>(
    rows: &'a Input,
    (stride_x, stride_y): (isize, isize),
    start_positions: impl Iterator<Item = (usize, usize)> + Clone + 'a,
) -> impl Iterator<Item = String> + Clone + 'a {
    let column_length = rows.len();
    let row_length = rows[0].len();

    start_positions.map(move |(start_x, start_y)| {
        let mut s = String::with_capacity(
            (column_length - start_y * stride_y.abs() as usize)
                .min(row_length - start_x * stride_x.abs() as usize),
        );

        let (mut pos_x, mut pos_y) = (start_x, start_y);
        while let Some(&ch) = rows.get(pos_y).and_then(|row| row.get(pos_x)) {
            s.push(ch);
            pos_x = (pos_x as isize + stride_x) as usize;
            pos_y = (pos_y as isize + stride_y) as usize;
        }
        s
    })
}

fn part1_count_xmas(rows: Input) -> u64 {
    let column_length = rows.len();
    let row_length = rows[0].len();
    let last_column = column_length - 1;
    let last_row = row_length - 1;

    let left_edge = std::iter::repeat(0).zip(0..column_length);
    let _right_edge = std::iter::repeat(last_row).zip(0..column_length);
    let top_edge = (0..row_length).zip(std::iter::repeat(0));
    let bottom_edge = (0..row_length).zip(std::iter::repeat(last_column));

    let left_to_right = substrings_from_stride(&rows, (1, 0), left_edge.clone());
    let up_to_down = substrings_from_stride(&rows, (0, 1), top_edge.clone());
    let up_left_to_down_right = substrings_from_stride(
        &rows,
        (1, 1),
        top_edge.clone().chain(left_edge.clone().skip(1)),
    );
    let down_left_to_up_right = substrings_from_stride(
        &rows,
        (1, -1),
        bottom_edge.clone().skip(1).chain(left_edge.clone()),
    );

    let half = left_to_right
        .chain(up_to_down)
        .chain(up_left_to_down_right)
        .chain(down_left_to_up_right);

    assert_eq!(
        half.clone().map(|s| s.chars().count()).sum::<usize>(),
        rows.iter().flatten().count() * 4
    );

    let half = half
        .reduce(|left, right| format!("{left} {right}"))
        .expect("Input is non-empty");

    // NOTE: Not correct way to reverse a unicode string.
    let all = format!(
        "{half} {reverse}",
        reverse = half.chars().rev().collect::<String>()
    );
    drop(half);

    let regex = Regex::new("XMAS").expect("Regex should be valid");

    regex.find_iter(&all).count() as u64
}

fn part2_prepare(rows: Input) -> (usize, String, regex_automata::nfa::thompson::NFA) {
    let row_length = rows[0].len();
    let stride =
        // Wrap around the row.
        row_length
        // The newline needs to be skipped.
        + 1
        // The width of the X is 3 characters.
        - 3;

    let as_string: String = rows
        .into_iter()
        .flat_map(|row| row.into_iter().chain(std::iter::once('\n')))
        .collect();

    let mas_or_sam = [&['M', 'A', 'S'], &['S', 'A', 'M']].into_iter();
    let all_combos = mas_or_sam
        .clone()
        .flat_map(|left| std::iter::repeat(left).zip(mas_or_sam.clone()));

    let regex_pattern = all_combos
        .map(|(&[a1, a2, a3], &[b1, b2, b3])| {
            assert_eq!(a2, b2);
            format!(
                "{row1}{newline}{row2}{newline}{row3}",
                row1 = format!("{a1}.{b1}"),
                row2 = format!(".{a2}."),
                row3 = format!("{b3}.{a3}"),
                newline = format!(".{{{stride}}}")
            )
        })
        .reduce(|left, right| format!("{left}|{right}"))
        .expect("the number of pattens is always the same");

    let regex_pattern = format!("(?s-u)(?:{regex_pattern})");
    dbg!(&regex_pattern);

    let syntax_config = syntax::Config::new()
        .dot_matches_new_line(true)
        .unicode(false)
        .utf8(false);
    let thompson_config = thompson::Config::new().shrink(true).utf8(false);

    let nfa = regex_automata::nfa::thompson::NFA::compiler()
        .syntax(syntax_config)
        .configure(thompson_config.clone())
        .build(&regex_pattern)
        .expect("Regex should be valid");
    println!("Finished building NFA");

    (row_length, as_string, nfa)
}

fn part2_execute_search(
    (row_length, as_string, nfa): (usize, impl AsRef<str>, regex_automata::nfa::thompson::NFA),
) -> u64 {
    // use regex_automata::dfa::dense as dfa;
    // use regex_automata::dfa::OverlappingState;
    use regex_automata::hybrid::dfa;
    use regex_automata::hybrid::dfa::OverlappingState;

    let dfa = dfa::DFA::builder()
        .configure(dfa::DFA::config().match_kind(MatchKind::All))
        .build_from_nfa(nfa)
        .expect("Regex should be valid");
    println!("Finished building DFA");

    let input = regex_automata::Input::new(as_string.as_ref());

    let mut cache = dfa.create_cache();
    let mut state = OverlappingState::start();
    let mut count = 0;
    loop {
        dfa.try_search_overlapping_fwd(&mut cache, &input, &mut state)
            .expect("Searching shouldn't fail");

        #[allow(unused)]
        let Some(mat) = state.get_match() else {
            break;
        };
        count += 1;

        if common::DEBUG {
            let off = mat.offset();
            let rlpo = row_length + 1;
            for rows_back in (0..3).rev() {
                let end = off - rlpo * rows_back;
                let start = end - 3;
                println!("{}", &as_string.as_ref()[start..end]);
            }
            println!("count: {count}");
        }
    }

    count
}

fn part2_count_x_mas(rows: Input) -> u64 {
    let preparations = part2_prepare(rows);
    part2_execute_search(preparations)
}

#[test]
fn part1_example() {
    let input = example_input();
    let num_matches = part1_count_xmas(input);
    assert_eq!(num_matches, 18);
}

#[test]
fn part1_real() {
    let input = input();
    let num_matches = part1_count_xmas(input);
    assert_eq!(num_matches, 2642);
}

#[test]
fn part2_example() {
    let input = example_input();
    let num_matches = part2_count_x_mas(input);
    assert_eq!(num_matches, 9);
}

#[test]
fn part2_real() {
    let input = input();
    let num_matches = part2_count_x_mas(input);
    assert_eq!(num_matches, 1974);
}

#[bench]
fn bench_part2_real(b: &mut Bencher) {
    let input = input();
    let (row_length, as_string, nfa) = part2_prepare(input);
    b.iter(|| part2_execute_search((row_length, &as_string, nfa.clone())));
}
