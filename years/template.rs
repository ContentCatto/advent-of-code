fn input() -> Input {
    input_from_rows(common::input_rows("input/day00"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day00-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    todo!()
}

type Input = ();

#[test]
fn part1_example() {
    let input = example_input();
    let result = todo!();
    assert_eq!(result, todo!());
}

#[test]
fn part1_real() {
    let input = input();
    let result = todo!();
    assert_eq!(result, todo!());
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = todo!();
    assert_eq!(result, todo!());
}

#[test]
fn part2_real() {
    let input = input();
    let result = todo!();
    assert_eq!(result, todo!());
}
