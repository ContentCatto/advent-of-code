use regex::Regex;
use regex_automata::{
    dfa::{dense, Automaton, OverlappingState},
    MatchKind,
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day03"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day03-example"))
}

fn example2_input() -> Input {
    input_from_rows(common::input_rows("input/day03-example2"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.reduce(|left, right| format!("{left}\n{right}"))
        .expect("Input must be non-empty")
}

type Input = String;

fn calculate_sum_from_patterns<
    's,
    I,
    It: IntoIterator<Item = I>,
    Match: Fn(&'s str) -> It + 's,
    Extract: Fn(I) -> (u64, u64),
    Op: Fn(u64, u64) -> u64,
>(
    memory: &'s str,
    patterns: &[(Match, Extract, Op)],
) -> u64 {
    patterns
        .iter()
        .flat_map(|(matcher, extract, op)| {
            matcher(memory)
                .into_iter()
                .map(extract)
                .map(|(lhs, rhs)| op(lhs, rhs))
        })
        .sum()
}

#[rustfmt::skip]
fn part1_find_muls(memory: impl AsRef<str>) -> u64 {
    calculate_sum_from_patterns(memory.as_ref(), &[
        (
            {
                let regex = Regex::new(r"mul\((?<l>\d+),(?<r>\d+)\)").expect("Should be valid");
                move |haystack| regex.captures_iter(haystack).map(
                    |captures| {
                        let left = captures
                            .name("l")
                            .expect("Regex should have a left operand capture group")
                            .as_str()
                            .parse()
                            .expect("Captured value should be a valid unsigned integer");
                        let right = captures
                            .name("r")
                            .expect("Regex should have a right operand capture group")
                            .as_str()
                            .parse()
                            .expect("Captured value should be a valid unsigned integer");
                        (left, right)
                    }
                ).collect::<Vec<_>>().into_iter()
            },
            |input| input,
            u64::strict_mul,
        ),
    ])
}

/// Essentially just `<'s>|haystack: &'s str| -> Vec<&'s str> { /* ... */ }` that uses `dfa` in
/// the closure body.
struct Matcher {
    dfa: dense::DFA<Vec<u32>>,
}

impl<'s> FnOnce<(&'s str,)> for Matcher {
    type Output = Vec<&'s str>;

    extern "rust-call" fn call_once(self, args: (&'s str,)) -> Self::Output {
        self.call(args)
    }
}

impl<'s> FnMut<(&'s str,)> for Matcher {
    extern "rust-call" fn call_mut(&mut self, args: (&'s str,)) -> Self::Output {
        self.call(args)
    }
}

impl<'s> Fn<(&'s str,)> for Matcher {
    extern "rust-call" fn call(&self, (haystack,): (&'s str,)) -> Self::Output {
        let input = regex_automata::Input::new(haystack);
        let mut state = OverlappingState::start();

        let mut matches = vec![];
        while let Some(hm) = {
            self.dfa
                .try_search_overlapping_fwd(&input, &mut state)
                .expect("TODO");
            state.get_match()
        } {
            matches.push(&haystack[..hm.offset()]);
        }
        matches
    }
}

fn inverted_literal_regex_match(literal_match: &str) -> String {
    if literal_match.is_empty() {
        return String::new();
    }

    let mut sorted_chars: Vec<char> = literal_match.chars().collect();
    sorted_chars.sort_unstable();
    assert!(sorted_chars
        .array_windows::<2>()
        .all(|&[left, right]| left != right));

    let repeating_part = literal_match
        .char_indices()
        .map(|(inverted_char_index, inverted_char)| {
            let inverted_range =
                inverted_char_index..inverted_char_index + inverted_char.len_utf8();
            let before_inverted = &literal_match[..inverted_range.start];
            let str_to_invert = &literal_match[inverted_range];
            format!(
                "{before}[^{inverted}]",
                before = regex::escape(before_inverted),
                inverted = regex::escape(str_to_invert),
            )
        })
        .reduce(|left, right| format!("{left}|{right}"))
        .expect("Input is non-empty");
    let repeating_part = format!("(?:{repeating_part})*?");

    let trailing_part = literal_match
        .char_indices()
        .skip(1)
        .map(|(inverted_char_index, _inverted_char)| {
            let before_inverted = &literal_match[..inverted_char_index];
            regex::escape(before_inverted)
        })
        .reduce(|left, right| format!("{left}|{right}"))
        .expect("Input is non-empty");
    let trailing_part = format!("(?:{trailing_part})?");

    format!("{repeating_part}{trailing_part}")
}

#[test]
fn test_generate_inverse_regex() {
    let inverse_regex = inverted_literal_regex_match("don't()");
    let expected = r"(?:[^d]|d[^o]|do[^n]|don[^']|don'[^t]|don't[^\(]|don't\([^\)])*?(?:d|do|don|don'|don't|don't\()?";
    assert_eq!(inverse_regex, expected);
}

fn part2_prepare() -> Matcher {
    let dfa = dense::DFA::builder()
        .configure(dense::DFA::config().match_kind(MatchKind::All))
        // Inverted search for "don't()"
        .build(&format!(
            "{do}{no_dont}{mul}",
            do = r"(?:^|do\(\))",
            no_dont = inverted_literal_regex_match("don't()"),
            mul = r"mul\((?<l>\d+),(?<r>\d+)\)"
        ))
        .expect("Regex should be valid");

    Matcher { dfa }
}

fn part2_search_and_calculate(matcher: &Matcher, memory: impl AsRef<str>) -> u64 {
    calculate_sum_from_patterns(
        memory.as_ref(),
        &[(
            matcher,
            |ending_in_valid_mul| {
                let mut rem = ending_in_valid_mul
                    .strip_suffix(')')
                    .expect("mul() expression should end with a parenthesis");
                let (lhs, rhs);
                (rem, rhs) = rem
                    .rsplit_once(',')
                    .expect("mul() expression should have a comma between the operands");
                (_, lhs) = rem
                    .rsplit_once('(')
                    .expect("mul() expression should have a parenthesis before the operands");
                let lhs = lhs
                    .parse()
                    .expect("mul() expression operand should be a valid unsigned integer");
                let rhs = rhs
                    .parse()
                    .expect("mul() expression operand should be a valid unsigned integer");
                (lhs, rhs)
            },
            u64::strict_mul,
        )],
    )
}

fn part2_find_enabled_muls(memory: impl AsRef<str>) -> u64 {
    // Normal closure that captures the DFA doesn't work, because there is no syntax to explain to
    // the compiler that the returned strings don't reference the DFA.
    let matcher = part2_prepare();
    part2_search_and_calculate(&matcher, memory)
}

#[test]
fn part1_example() {
    let memory = example_input();
    let result = part1_find_muls(&memory);
    assert_eq!(result, 161);
}

#[test]
fn part1_real() {
    let memory = input();
    let result = part1_find_muls(&memory);
    assert_eq!(result, 166_630_675);
}

#[test]
fn part2_example() {
    let memory = example2_input();
    let result = part2_find_enabled_muls(&memory);
    assert_eq!(result, 48);
}

#[test]
fn part2_real() {
    let memory = input();
    let result = part2_find_enabled_muls(&memory);
    assert_eq!(result, 93_465_710);
}

#[bench]
fn bench_part2_real(b: &mut test::Bencher) {
    let memory = input();
    let matcher = part2_prepare();
    b.iter(|| part2_search_and_calculate(&matcher, &memory));
}
