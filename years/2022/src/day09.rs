#![cfg(test)]

use std::cell::RefCell;
use std::{
    collections::HashSet,
    fmt::{Debug, Display},
    ops::RangeInclusive,
};

fn input() -> MoveSet {
    input_from_rows(common::input_rows("input/day09"))
}

fn example_input() -> MoveSet {
    input_from_rows(common::input_rows("input/day09-example"))
}

fn example_input2() -> MoveSet {
    input_from_rows(common::input_rows("input/day09-example2"))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Display for Dir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl From<&str> for Dir {
    fn from(value: &str) -> Self {
        assert_eq!(value.len(), 1);
        match value.chars().next().unwrap() {
            'U' => Dir::Up,
            'D' => Dir::Down,
            'L' => Dir::Left,
            'R' => Dir::Right,
            _ => panic!(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Move {
    dir: Dir,
    steps: u64,
}

impl From<&str> for Move {
    fn from(value: &str) -> Self {
        let (dir, steps) = value.split_once(' ').unwrap();
        let dir = dir.into();
        let steps = steps.parse::<u64>().unwrap();
        Move { dir, steps }
    }
}

type MoveSet = Vec<Move>;

type Position = (i64, i64);

#[derive(Debug, Clone, PartialEq, Eq)]
struct Rope {
    segm: Vec<Position>,
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> MoveSet {
    rows.map(|string| Move::from(&string[..])).collect()
}

#[cfg(release)]
fn display_grid(
    (axis_x, axis_y): (RangeInclusive<i64>, RangeInclusive<i64>),
    rope: Rope,
    visited_tiles: &HashSet<Position>,
) -> std::fmt::Result {
    Ok(())
}

#[cfg(not(release))]
fn display_grid(
    (axis_x, axis_y): (RangeInclusive<i64>, RangeInclusive<i64>),
    rope: &Rope,
    visited_tiles: &HashSet<Position>,
) -> std::fmt::Result {
    thread_local! {
        static BUFFER: RefCell<String> = RefCell::new(String::new());
    }
    let segm = &rope.segm;

    BUFFER.with(|local_key| {
        let mut out = local_key.borrow_mut();
        use std::fmt::Write;

        for y in axis_y.rev() {
            write!(out, "{y:<4}")?;

            'cell: for x in axis_x.clone() {
                let pos = (x, y);
                if segm[0] == pos {
                    write!(out, "H")?;
                    continue 'cell;
                }
                for (segment_index, segment) in segm[1..].iter().enumerate() {
                    let segment_index = segment_index + 1;
                    if segment == &pos {
                        write!(out, "{segment_index}")?;
                        continue 'cell;
                    }
                }
                if pos == (0, 0) {
                    write!(out, "s")?;
                    continue 'cell;
                }
                if visited_tiles.contains(&pos) {
                    write!(out, "#")?;
                    continue 'cell;
                }
                write!(out, ".")?;
            }
            // if y == segm[0].1 {
            //     write!(out, "   (Head covers Tail)")?;
            // }
            writeln!(out)?;
        }
        print!("{}", out);
        (*out).clear();
        Ok(())
    })
}

fn tail_follow(rope: &mut Rope) {
    let segm = &mut rope.segm;

    for i in 1..segm.len() {
        let diff_x = segm[i - 1].0 as i64 - segm[i].0 as i64;
        let diff_y = segm[i - 1].1 as i64 - segm[i].1 as i64;

        let abs_x = diff_x.abs();
        let abs_y = diff_y.abs();

        if abs_x <= 1 && abs_y <= 1 {
            return;
        }

        segm[i].0 += diff_x.signum();
        segm[i].1 += diff_y.signum();
    }
}

fn count_visisted_tiles<'a>(segment_count: usize, moves: impl Iterator<Item = &'a Move>) -> u64 {
    let mut grid_axes: (RangeInclusive<i64>, RangeInclusive<i64>) = (0..=4, 0..=4);

    let mut rope = Rope {
        segm: [(0, 0)].repeat(segment_count),
    };

    let mut visited_cells: HashSet<Position> = HashSet::new();

    println!("== Initial State ==");
    display_grid(grid_axes.clone(), &rope, &visited_cells).unwrap();
    println!();

    for (_move_nr, mv) in moves.enumerate() {
        let axis_move: (i64, i64) = match mv.dir {
            Dir::Up => (0, 1),
            Dir::Down => (0, -1),
            Dir::Left => (-1, 0),
            Dir::Right => (1, 0),
        };

        println!("== {} {} ==", mv.dir, mv.steps);
        for _iteration in 0..mv.steps {
            let head = &mut rope.segm[0];
            head.0 = head.0 + axis_move.0;
            head.1 = head.1 + axis_move.1;

            if head.0 < *grid_axes.0.start() {
                grid_axes.0 = (head.0)..=*grid_axes.0.end();
            } else if head.1 < *grid_axes.1.start() {
                grid_axes.1 = (head.1)..=*grid_axes.1.end();
            } else if head.0 > *grid_axes.0.end() {
                grid_axes.0 = *grid_axes.0.start()..=(head.0);
            } else if head.1 > *grid_axes.1.end() {
                grid_axes.1 = *grid_axes.1.start()..=(head.1);
            }

            tail_follow(&mut rope);

            visited_cells.insert(*rope.segm.last().unwrap());
        }
        display_grid(grid_axes.clone(), &rope, &visited_cells).unwrap();
        println!();
    }

    visited_cells.len() as u64
}

#[test]
fn parsing_works() {
    let parsed = example_input();
    let expected: MoveSet = [
        Move {
            dir: Dir::Right,
            steps: 4,
        },
        Move {
            dir: Dir::Up,
            steps: 4,
        },
        Move {
            dir: Dir::Left,
            steps: 3,
        },
        Move {
            dir: Dir::Down,
            steps: 1,
        },
        Move {
            dir: Dir::Right,
            steps: 4,
        },
        Move {
            dir: Dir::Down,
            steps: 1,
        },
        Move {
            dir: Dir::Left,
            steps: 5,
        },
        Move {
            dir: Dir::Right,
            steps: 2,
        },
    ]
    .into();

    assert_eq!(parsed, expected);
}

#[test]
fn part1_example() {
    let num_visited_tiles = count_visisted_tiles(2, example_input().iter());

    assert_eq!(num_visited_tiles, 13);
}

#[test]
fn part1() {
    let num_visited_tiles = count_visisted_tiles(2, input().iter());

    assert_eq!(num_visited_tiles, 6181);
}

#[test]
fn part2_example() {
    let num_visited_tiles = count_visisted_tiles(10, example_input().iter());

    assert_eq!(num_visited_tiles, 1);
}

#[test]
fn part2_example2() {
    let num_visited_tiles = count_visisted_tiles(10, example_input2().iter());

    assert_eq!(num_visited_tiles, 36);
}

#[test]
fn part2() {
    let num_visited_tiles = count_visisted_tiles(10, input().iter());

    assert_eq!(num_visited_tiles, 2386);
}
