use once_cell::sync::Lazy;
use std::{collections::HashMap, iter::FromIterator};

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
enum Move {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl Move {
    fn from_abc(abc: char) -> Self {
        match abc {
            'A' => Move::Rock,
            'B' => Move::Paper,
            'C' => Move::Scissors,
            _ => panic!(),
        }
    }

    fn from_xyz(xyz: char) -> Self {
        match xyz {
            'X' => Move::Rock,
            'Y' => Move::Paper,
            'Z' => Move::Scissors,
            _ => panic!(),
        }
    }

    const OPTIONS: [Move; 3] = [Move::Rock, Move::Paper, Move::Scissors];

    fn iter() -> impl Iterator<Item = Move> {
        Self::OPTIONS.iter().copied()
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
enum MatchResult {
    Loss = 0,
    Draw = 3,
    Win = 6,
}

impl MatchResult {
    fn from_xyz(xyz: char) -> MatchResult {
        match xyz {
            'X' => MatchResult::Loss,
            'Y' => MatchResult::Draw,
            'Z' => MatchResult::Win,
            _ => panic!(),
        }
    }

    fn from_play(play: Play) -> MatchResult {
        match play {
            // Opponent, You
            (Move::Rock, Move::Rock) => MatchResult::Draw,
            (Move::Paper, Move::Paper) => MatchResult::Draw,
            (Move::Scissors, Move::Scissors) => MatchResult::Draw,

            (Move::Rock, Move::Paper) => MatchResult::Win,
            (Move::Paper, Move::Rock) => MatchResult::Loss,

            (Move::Paper, Move::Scissors) => MatchResult::Win,
            (Move::Scissors, Move::Paper) => MatchResult::Loss,

            (Move::Scissors, Move::Rock) => MatchResult::Win,
            (Move::Rock, Move::Scissors) => MatchResult::Loss,
        }
    }

    const OPTIONS: [MatchResult; 3] = [MatchResult::Loss, MatchResult::Draw, MatchResult::Win];

    fn iter() -> impl Iterator<Item = MatchResult> {
        Self::OPTIONS.iter().copied()
    }
}

type Play = (Move, Move);

fn score(play: Play) -> i32 {
    play.1 as i32 + MatchResult::from_play(play) as i32
}

fn total_score(plays: impl Iterator<Item = Play>) -> i32 {
    plays.map(score).sum()
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Vec<Play> {
    rows.map(|row| {
        let prediction = Move::from_abc(row.chars().next().unwrap());
        let response = Move::from_xyz(row.chars().last().unwrap());
        (prediction, response)
    })
    .collect()
}

fn input() -> Vec<Play> {
    input_from_rows(common::input_rows("input/day02"))
}

fn follow_strategy(prediction: Move, response: MatchResult) -> Move {
    // IDK if iterators really was the play here, but at least i can argue that it is fast...
    static PREDICTION_RESPONSE_MAPPING: Lazy<HashMap<(Move, MatchResult), Move>> =
        Lazy::new(|| {
            let possible_matches: Vec<Play> = Move::iter()
                .flat_map(|opp: Move| Move::iter().map(move |you: Move| (opp, you)))
                .collect();
            let reverse_result_mapping: Vec<(MatchResult, [Play; 3])> =
                Vec::from_iter(MatchResult::iter().map(|wanted_result: MatchResult| {
                    let plays_with_wanted_result: Vec<(Move, Move)> = possible_matches
                        .iter()
                        .filter(|play: &&Play| MatchResult::from_play(**play) == wanted_result)
                        .copied()
                        .collect();
                    // NOTE: unwrap is safe, because there are exactly 3 ways to get each result type (3*3 unique plays / 3 result types )
                    let as_array = *plays_with_wanted_result.array_chunks::<3>().next().unwrap();
                    (wanted_result, as_array)
                }));
            let final_mapping: HashMap<(Move, MatchResult), Move> =
                HashMap::from_iter(reverse_result_mapping.iter().flat_map(|(res, plays)| {
                    plays
                        .iter()
                        .map(move |play: &Play| ((play.0, *res), play.1))
                }));
            final_mapping
        });
    *PREDICTION_RESPONSE_MAPPING
        .get(&(prediction, response))
        .unwrap()
}

fn correct_input_from_rows(rows: impl Iterator<Item = String>) -> Vec<Play> {
    rows.map(|row| {
        let prediction = Move::from_abc(row.chars().next().unwrap());
        let response = MatchResult::from_xyz(row.chars().last().unwrap());
        let response_move = follow_strategy(prediction, response);
        (prediction, response_move)
    })
    .collect()
}

fn correct_input() -> Vec<Play> {
    correct_input_from_rows(common::input_rows("input/day02"))
}

#[test]
fn non_empty() {
    let strategy = input();
    assert_ne!(strategy.len(), 0);
}

#[test]
fn example_score_matches() {
    let strat: Vec<Play> = [
        (Move::Rock, Move::Paper),
        (Move::Paper, Move::Rock),
        (Move::Scissors, Move::Scissors),
    ]
    .into();
    assert_eq!(total_score(strat.into_iter()), 15);
}

#[test]
fn follow_strategy_guide() {
    let strat: Vec<(Move, Move)> = input();
    assert_eq!(total_score(strat.into_iter()), 11603);
}

#[test]
fn correctly_follow_strategy_guide() {
    let strat: Vec<(Move, Move)> = correct_input();
    assert_eq!(total_score(strat.into_iter()), 12725);
}
