use std::fmt::Debug;

pub fn permutations<T>(values: impl AsRef<[T]> + 'static) -> Box<dyn Iterator<Item = Vec<T>>>
where
    T: Clone + Debug + 'static,
    Vec<T>: Clone,
{
    if values.as_ref().len() == 1 {
        return Box::new([Vec::from(values.as_ref())].into_iter());
    }

    Box::new((0..values.as_ref().len()).flat_map(move |last_index| {
        let mut values = Vec::from(values.as_ref());
        let last_value = values.swap_remove(last_index);
        permutations(values).map(move |mut values| {
            values.push(last_value.clone());
            values
        })
    }))
}

#[cfg(test)]
mod tests {
    use super::permutations;

    #[test]
    fn correct_permutations() {
        let mut permuted: Vec<_> = permutations([1, 2, 3]).collect();
        permuted.sort();

        let expected = vec![
            vec![1, 2, 3],
            vec![1, 3, 2],
            vec![2, 1, 3],
            vec![2, 3, 1],
            vec![3, 1, 2],
            vec![3, 2, 1],
        ];

        assert_eq!(permuted, expected);
    }

    #[test]
    fn correct_permutations_with_4_elements() {
        let mut permuted: Vec<_> = permutations([1, 2, 3, 4]).collect();
        permuted.sort();

        let expected = vec![
            vec![1, 2, 3, 4],
            vec![1, 2, 4, 3],
            vec![1, 3, 2, 4],
            vec![1, 3, 4, 2],
            vec![1, 4, 2, 3],
            vec![1, 4, 3, 2],
            vec![2, 1, 3, 4],
            vec![2, 1, 4, 3],
            vec![2, 3, 1, 4],
            vec![2, 3, 4, 1],
            vec![2, 4, 1, 3],
            vec![2, 4, 3, 1],
            vec![3, 1, 2, 4],
            vec![3, 1, 4, 2],
            vec![3, 2, 1, 4],
            vec![3, 2, 4, 1],
            vec![3, 4, 1, 2],
            vec![3, 4, 2, 1],
            vec![4, 1, 2, 3],
            vec![4, 1, 3, 2],
            vec![4, 2, 1, 3],
            vec![4, 2, 3, 1],
            vec![4, 3, 1, 2],
            vec![4, 3, 2, 1],
        ];

        assert_eq!(permuted, expected);
    }
}
