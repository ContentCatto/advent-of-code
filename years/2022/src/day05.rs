#![cfg(test)]



type Pile = Vec<char>;

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
struct Stack {
    piles: Vec<Pile>,
}

impl Stack {
    fn apply_move(&mut self, crane_move: CraneMove) {
        let from = crane_move.from_stack as usize;
        let to = crane_move.to_stack as usize;
        for _ in 0..crane_move.container_count {
            let container = self.piles[from].pop().unwrap();
            self.piles[to].push(container);
        }
    }

    fn apply_multi_move(&mut self, crane_move: CraneMove) {
        let from = crane_move.from_stack as usize;
        let to = crane_move.to_stack as usize;

        let cutoff = self.piles[from].len() - crane_move.container_count as usize;
        let containers: Vec<_> = self.piles[from].drain(cutoff..).collect();
        for container in containers {
            self.piles[to].push(container);
        }
    }

    fn top_containers(&self) -> String {
        let mut top = String::new();
        for pile in &self.piles[1..] {
            top.push(*pile.last().unwrap());
        }
        top
    }
}

fn parse_stack(rows: Vec<String>) -> Stack {
    let stack_count = rows[0].split_ascii_whitespace().count();
    let mut piles: Vec<Pile> = Vec::with_capacity(stack_count);
    for _ in 0..=stack_count {
        piles.push(Vec::new());
    }

    for row in rows {
        let mut stack_index = 1;
        let mut char_pos: usize = 1;
        while let Some(ch) = row.chars().nth(char_pos) {
            if ch.is_alphabetic() {
                piles[stack_index].push(ch);
                assert!(ch.is_uppercase());
            }
            stack_index += 1;
            const spacing: usize = 4; // "A] [".chars().count();
            char_pos += spacing;
        }
    }
    Stack { piles }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
struct CraneMove {
    container_count: i32,
    from_stack: i32,
    to_stack: i32,
}

fn parse_crane_move(row: String) -> CraneMove {
    let mut words = row.split_ascii_whitespace();
    words.next().unwrap();
    let container_count: i32 = words.next().unwrap().parse().unwrap();
    words.next().unwrap();
    let from_stack: i32 = words.next().unwrap().parse().unwrap();
    words.next().unwrap();
    let to_stack: i32 = words.next().unwrap().parse().unwrap();
    CraneMove {
        container_count,
        from_stack,
        to_stack,
    }
}

type Procedure = (Stack, Vec<CraneMove>);

fn input_from_rows(rows: impl Iterator<Item = String>) -> Procedure {
    let mut stack_rows: Vec<String> = Vec::new();
    let mut moves: Vec<CraneMove> = Vec::new();
    for row in rows {
        if row.starts_with("move") {
            moves.push(parse_crane_move(row));
        } else if !row.is_empty() {
            stack_rows.push(row);
        }
    }
    stack_rows.reverse();
    let stack = parse_stack(stack_rows);
    (stack, moves)
}

fn input() -> Procedure {
    input_from_rows(common::input_rows("input/day05"))
}

fn example_input() -> Procedure {
    let example_rows = "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2"
        .lines()
        .map(String::from);
    input_from_rows(example_rows)
}

#[test]
fn input_non_empty() {
    let inputs = [input(), example_input()];
    for (stack, moves) in inputs {
        assert_ne!(stack.piles.len(), 0);
        assert_ne!(moves.len(), 0);
    }
}

#[test]
fn example_matches_stack() {
    let (stack, _) = example_input();
    assert_eq!(stack.piles[2], Vec::from(['M', 'C', 'D']));
}

#[test]
fn example_matches_moves() {
    let (_, moves) = example_input();
    assert_eq!(
        moves[2],
        CraneMove {
            container_count: 2,
            from_stack: 2,
            to_stack: 1
        }
    );
}

#[test]
fn example_final_surface() {
    let (mut stack, moves) = example_input();
    for crane_move in moves {
        stack.apply_move(crane_move);
    }
    assert_eq!(stack.top_containers(), String::from("CMZ"));
}

#[test]
fn final_surface() {
    let (mut stack, moves) = input();
    for crane_move in moves {
        stack.apply_move(crane_move);
    }
    assert_eq!(stack.top_containers(), String::from("PSNRGBTFT"));
}

#[test]
fn example_multi_move_surface() {
    let (mut stack, moves) = example_input();
    for crane_move in moves {
        stack.apply_multi_move(crane_move);
    }
    assert_eq!(stack.top_containers(), String::from("MCD"));
}

#[test]
fn multi_move_surface() {
    let (mut stack, moves) = input();
    for crane_move in moves {
        stack.apply_multi_move(crane_move);
    }
    assert_eq!(stack.top_containers(), String::from("BNTZFPMMW"));
}
