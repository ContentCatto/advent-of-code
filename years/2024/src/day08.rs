use std::collections::{HashMap, HashSet};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day08"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day08-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let mut map_size = (0, 0);
    let mut antennas = HashMap::new();

    for (y, row) in rows.enumerate() {
        map_size.1 += 1;
        map_size.0 = row.len();
        for (x, ch) in row.chars().enumerate() {
            match ch {
                'a'..='z' | 'A'..='Z' | '0'..='9' => {
                    antennas.entry(ch).or_insert_with(Vec::new).push((x, y))
                }
                _ => {}
            }
        }
    }

    (map_size, antennas)
}

type Pos = (usize, usize);
type MapSize = (usize, usize);
type Frequency = char;
type AntennasByFrequency = HashMap<Frequency, Vec<Pos>>;

type Input = (MapSize, AntennasByFrequency);

fn antinodes_for_frequency<'a, G: Iterator<Item = Pos> + 'a>(
    map_size: MapSize,
    antenna_positions: &'a [Pos],
    antinode_generator: impl Fn(MapSize, Pos, Pos) -> G + 'a,
) -> impl Iterator<Item = Pos> + 'a {
    antenna_positions
        .iter()
        .flat_map(|&first_pos| {
            antenna_positions
                .iter()
                .copied()
                .filter(move |&second_pos| first_pos != second_pos)
                .map(move |second_pos| (first_pos, second_pos))
        })
        .flat_map(move |(a, b)| antinode_generator(map_size, a, b))
}

fn debug_print_map(
    (map_size_x, map_size_y): MapSize,
    antennas_by_frequency: &AntennasByFrequency,
    unique_antinodes: &HashSet<Pos>,
) {
    let mut s = String::with_capacity((map_size_x + 1) * map_size_y);
    for y in 0..map_size_y {
        'outer: for x in 0..map_size_x {
            for (frequency, antenna_positions) in antennas_by_frequency.iter() {
                if antenna_positions.contains(&(x, y)) {
                    s.push(*frequency);
                    continue 'outer;
                }
            }
            if unique_antinodes.contains(&(x, y)) {
                s.push('#');
                continue;
            }
            s.push('.');
        }
        s.push('\n');
    }
    eprintln!("{s}");
}

fn count_unique_antinodes_from_antinode_generator<'a, G: Iterator<Item = Pos> + 'a>(
    (map_size, antennas_by_frequency): Input,
    antinode_generator: impl Fn(MapSize, Pos, Pos) -> G + 'a + Clone,
) -> u64 {
    let mut unique_antinodes: HashSet<Pos> = HashSet::new();
    for (&frequency, antenna_positions) in &antennas_by_frequency {
        let unique_antinodes_for_frequency: HashSet<Pos> =
            antinodes_for_frequency(map_size, antenna_positions, antinode_generator.clone())
                .collect();
        let fake_map = HashMap::from([(frequency, antenna_positions.to_owned())]);
        debug_print_map(map_size, &fake_map, &unique_antinodes_for_frequency);
        println!();
        unique_antinodes.reserve(unique_antinodes_for_frequency.len());
        for antinode in unique_antinodes_for_frequency {
            unique_antinodes.insert(antinode);
        }
    }

    debug_print_map(map_size, &antennas_by_frequency, &unique_antinodes);

    unique_antinodes.len() as u64
}

fn part1_antinode_generator(
    (map_size_x, map_size_y): MapSize,
    (x1, y1): Pos,
    (x2, y2): Pos,
) -> impl Iterator<Item = Pos> {
    let (x1, y1, x2, y2) = (x1 as isize, y1 as isize, x2 as isize, y2 as isize);

    let (diff_x, diff_y) = (x1 - x2, y1 - y2);

    #[rustfmt::skip]
    let possible_antinodes = [
        (x1 + diff_x, y1 + diff_y),
        (x2 - diff_x, y2 - diff_y),
    ];

    possible_antinodes
        .into_iter()
        .filter(move |&(x, y)| {
            x >= 0 && y >= 0 && x < map_size_x as isize && y < map_size_y as isize
        })
        .map(|(x, y)| (x as usize, y as usize))
}

fn part1_count_unique_antinodes(input: Input) -> u64 {
    count_unique_antinodes_from_antinode_generator(input, part1_antinode_generator)
}

fn while_within_map(
    (map_size_x, map_size_y): MapSize,
    it: impl Iterator<Item = (isize, isize)>,
) -> impl Iterator<Item = (isize, isize)> {
    it.take_while(move |&(x, y)| {
        x >= 0 && y >= 0 && x < map_size_x as isize && y < map_size_y as isize
    })
}

fn part2_antinode_generator(
    map_size: MapSize,
    (x1, y1): Pos,
    (x2, y2): Pos,
) -> impl Iterator<Item = Pos> {
    let (x1, y1, x2, y2) = (x1 as isize, y1 as isize, x2 as isize, y2 as isize);

    let (diff_x, diff_y) = (x1 - x2, y1 - y2);

    let possible_antinodes = while_within_map(
        map_size,
        (0..).map(move |factor| (x1 + diff_x * factor, y1 + diff_y * factor)),
    )
    .chain(while_within_map(
        map_size,
        (0..).map(move |factor| (x2 - diff_x * factor, y2 - diff_y * factor)),
    ));

    possible_antinodes.map(|(x, y)| (x as usize, y as usize))
}

fn part2_count_unique_resonant_antinodes(input: Input) -> u64 {
    count_unique_antinodes_from_antinode_generator(input, part2_antinode_generator)
}

#[test]
fn part1_example() {
    let input = example_input();
    assert_eq!(input.0 .0, 12);
    assert_eq!(input.0 .1, 12);
    let result = part1_count_unique_antinodes(input);
    assert_eq!(result, 14);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_count_unique_antinodes(input);
    assert_eq!(result, 278);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_count_unique_resonant_antinodes(input);
    assert_eq!(result, 34);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_count_unique_resonant_antinodes(input);
    assert_eq!(result, 1067);
}
