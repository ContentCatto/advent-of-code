use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day05"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day05-example"))
}

fn input_from_rows(mut rows: impl Iterator<Item = String>) -> Input {
    let mut rules = Vec::new();
    while let Some(row) = rows.next() {
        if row.is_empty() {
            break;
        }
        let (before, after) = row
            .split_once('|')
            .expect("Rule row must contain pipe symbol");
        let before = before
            .parse()
            .expect("In rule, page before must be a valid number");
        let after = after
            .parse()
            .expect("In rule, page after must be a valid number");
        rules.push((before, after));
    }

    let updates = rows
        .map(|row| {
            row.split(',')
                .map(|page| page.parse().expect("Page should be a valid number"))
                .collect()
        })
        .collect();

    (rules, updates)
}

type Page = usize;
type BasicPageRule = (Page, Page);
type Update = Vec<Page>;

type Input = (Vec<BasicPageRule>, Vec<Update>);

#[derive(Debug, Clone, PartialEq, Eq)]
struct PageRules {
    /// For each key `k`, which pages need to be after it?
    pages_after: HashMap<Page, HashSet<Page>>,
}

impl PageRules {
    fn new(basic_page_rules: Vec<BasicPageRule>) -> PageRules {
        let mut pages_after = HashMap::new();
        for (before, after) in basic_page_rules {
            pages_after
                .entry(before)
                .or_insert_with(HashSet::new)
                .insert(after);
        }
        Self { pages_after }
    }

    fn is_sorted(&self, before: Page, after: Page) -> bool {
        self.pages_after
            .get(&after)
            .map(|expected_after| !expected_after.contains(&before))
            .unwrap_or(true)
    }

    /// Compare two [`Page`]s based on the page rules.
    ///
    /// - A|B <=> A<B and B>A
    /// - No rule <=> A=B
    fn compare(&self, lhs: Page, rhs: Page) -> Ordering {
        let Some(after_lhs) = self.pages_after.get(&lhs) else {
            return Ordering::Equal;
        };
        if after_lhs.contains(&rhs) {
            Ordering::Less
        } else {
            Ordering::Greater
        }
    }

    fn is_sorted_update(&self, update: &[Page]) -> bool {
        update.is_sorted_by(|&before, &after| self.is_sorted(before, after))
    }

    fn sort_update(&self, mut update: Update) -> Update {
        update.sort_by(|&before, &after| self.compare(before, after));
        update
    }
}

fn middle_page(update: impl AsRef<[Page]>) -> Page {
    let update = update.as_ref();
    let len = update.len();
    assert_eq!(len % 2, 1);
    update[(len - 1) / 2]
}

fn usize_as_u64(num: usize) -> u64 {
    num as u64
}

fn part1_checksum_correctly_ordered_updates((basic_page_rules, updates): Input) -> u64 {
    let page_rules = PageRules::new(basic_page_rules);

    updates
        .iter()
        .map(Vec::as_slice)
        .filter(|&update| page_rules.is_sorted_update(update))
        .map(middle_page)
        .map(|middle_page| middle_page as u64)
        .sum()
}

fn part2_checksum_fixed_updates((basic_page_rules, updates): Input) -> u64 {
    let page_rules = PageRules::new(basic_page_rules);

    updates
        .into_iter()
        .filter(|update| !page_rules.is_sorted_update(update))
        .map(|update| page_rules.sort_update(update))
        .inspect(|update| assert!(page_rules.is_sorted_update(update)))
        .map(middle_page)
        .map(usize_as_u64)
        .sum()
}

#[test]
fn parse_example() {
    let input = example_input();
    let expected = (
        vec![
            (47, 53),
            (97, 13),
            (97, 61),
            (97, 47),
            (75, 29),
            (61, 13),
            (75, 53),
            (29, 13),
            (97, 29),
            (53, 29),
            (61, 53),
            (97, 53),
            (61, 29),
            (47, 13),
            (75, 47),
            (97, 75),
            (47, 61),
            (75, 61),
            (47, 29),
            (75, 13),
            (53, 13),
        ],
        vec![
            vec![75, 47, 61, 53, 29],
            vec![97, 61, 53, 29, 13],
            vec![75, 29, 13],
            vec![75, 97, 47, 61, 53],
            vec![61, 13, 29],
            vec![97, 13, 75, 29, 47],
        ],
    );
    assert_eq!(input, expected);
}

#[test]
fn part1_example() {
    let input = example_input();
    let result = part1_checksum_correctly_ordered_updates(input);
    assert_eq!(result, 143);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_checksum_correctly_ordered_updates(input);
    assert_eq!(result, 3608);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_checksum_fixed_updates(input);
    assert_eq!(result, 123);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_checksum_fixed_updates(input);
    assert_eq!(result, 4922);
}
