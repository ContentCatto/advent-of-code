use std::{collections::BTreeMap, ops::Range};

use test::Bencher;

fn input() -> Input {
    input_from_rows(common::input_rows("input/day09"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day09-example"))
}

fn input_from_rows(mut rows: impl Iterator<Item = String>) -> Input {
    let input = rows.next().expect("Input should not be empty");
    assert_eq!(rows.next(), None, "Input should be exactly 1 row");
    input
        .chars()
        .array_chunks::<2>()
        .chain(
            std::iter::once(
                input
                    .chars()
                    .rev()
                    .next()
                    .expect("Input should be non-empty"),
            )
            .filter_map(|last_ch| {
                if input.chars().count() % 2 != 0 {
                    Some([last_ch, '0'])
                } else {
                    None
                }
            }),
        )
        .map(|[file_len, free_space_len]| {
            let file_len = (file_len as u32 - '0' as u32)
                .try_into()
                .expect("Number of used blocks in a row should be less than 10");
            let free_space_len = (free_space_len as u32 - '0' as u32)
                .try_into()
                .expect("Number of free blocks in a row should be less than 10");
            (file_len, free_space_len)
        })
        .collect()
}

type NumBlocks = u8;
type UncompactDiskEntry = (NumBlocks, NumBlocks);

type Input = Vec<UncompactDiskEntry>;

type FileId = usize;
type BlockId = usize;
type Filesystem = Vec<Option<FileId>>;

fn filesystem_checksum(filesystem: &[Option<usize>]) -> u64 {
    filesystem
        .iter()
        .copied()
        .enumerate()
        .filter_map(|(index, maybe_file_id)| maybe_file_id.map(|file_id| (index, file_id)))
        .map(|(index, file_id)| (index * file_id) as u64)
        .sum()
}

fn build_base_filesystem(uncompact_entries: &Input) -> Filesystem {
    let mut filesystem = Filesystem::new();
    for (file_id, &(num_used_blocks, num_free_blocks)) in uncompact_entries.iter().enumerate() {
        for _ in 0..num_used_blocks {
            filesystem.push(Some(file_id));
        }
        for _ in 0..num_free_blocks {
            filesystem.push(None);
        }
    }
    filesystem
}

fn debug_print_filesystem(filesystem: &[Option<usize>]) {
    if !common::DEBUG {
        return;
    }
    let file_id_chars: Vec<char> = ('0'..='9').chain('a'..='z').chain('A'..='Z').collect();
    for block in filesystem {
        if let Some(file_id) = block {
            eprint!("{}", file_id_chars.get(*file_id).unwrap_or(&'#'));
        } else {
            eprint!(".");
        }
    }
    eprintln!();
}

fn find_next_free(filesystem: &[Option<FileId>], start: BlockId) -> BlockId {
    for index in start..filesystem.len() {
        if filesystem[index].is_none() {
            return index;
        }
    }
    panic!("No block is free after the given start point");
}

fn part1_blockwise_compact_and_checksum(uncompact_entries: Input) -> u64 {
    let mut filesystem = build_base_filesystem(&uncompact_entries);

    let mut first_free = find_next_free(&filesystem, 0);
    let mut prev_file_id = 99;
    for block_id in (0..filesystem.len()).rev() {
        if block_id <= first_free {
            break;
        }
        if filesystem[block_id].is_some_and(|file_id| prev_file_id != file_id) {
            debug_print_filesystem(&filesystem);
        }
        if let Some(file_id) = filesystem[block_id].take() {
            filesystem[first_free] = Some(file_id);
            first_free = find_next_free(&filesystem, first_free + 1);
            prev_file_id = file_id;
        }
    }
    debug_print_filesystem(&filesystem);

    filesystem_checksum(&filesystem)
}

fn part2_filewise_compact_and_checksum(uncompact_entries: Input) -> u64 {
    let mut filesystem = build_base_filesystem(&uncompact_entries);
    debug_print_filesystem(&filesystem);

    let mut size_by_vacancy: BTreeMap<BlockId, usize> = BTreeMap::new();
    let mut files_left_to_check: Vec<(FileId, Range<BlockId>)> =
        Vec::with_capacity(uncompact_entries.len());
    let mut current_block_id = 0;

    for (file_id, (used_size, unused_size)) in uncompact_entries.into_iter().enumerate() {
        let end_of_file = current_block_id + used_size as BlockId;

        files_left_to_check.push((file_id, current_block_id..end_of_file));
        current_block_id = end_of_file;

        if unused_size != 0 {
            size_by_vacancy.insert(current_block_id, unused_size as usize);
            current_block_id += unused_size as usize;
        }
    }

    while let Some((file_id, file_range)) = files_left_to_check.pop() {
        let file_size = file_range.len();
        let Some((&first_vacancy, &vacancy_size)) = size_by_vacancy
            .iter()
            .take_while(|&(&pos, &_size)| pos < file_range.start)
            .find(|&(&_pos, &size)| size >= file_size)
        else {
            continue;
        };

        let new_pos = first_vacancy;
        let new_range = new_pos..new_pos + file_size;

        filesystem[file_range.clone()].fill(None);
        filesystem[new_range.clone()].fill(Some(file_id));

        size_by_vacancy.remove(&first_vacancy);

        let remaining_vacancy_size = vacancy_size - file_size;
        if remaining_vacancy_size > 0 {
            let new_vacancy_pos = new_range.end;
            size_by_vacancy.insert(new_vacancy_pos, remaining_vacancy_size);
        }

        debug_print_filesystem(&filesystem);
    }

    filesystem_checksum(&filesystem)
}

#[test]
fn part1_example() {
    let input = example_input();
    let result = part1_blockwise_compact_and_checksum(input);
    assert_eq!(result, 1928);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_blockwise_compact_and_checksum(input);
    assert_eq!(result, 6_384_282_079_460);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_filewise_compact_and_checksum(input);
    assert_eq!(result, 2858);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_filewise_compact_and_checksum(input);
    assert_eq!(result, 6408966547049);
}

#[bench]
fn bench_part2_real(b: &mut Bencher) {
    let input = input();
    b.iter(|| part2_filewise_compact_and_checksum(input.clone()));
}
