use std::{
    collections::HashSet,
    fmt::{Debug, Display},
};

fn input() -> Instructions {
    input_from_rows(common::input_rows("input/day10"))
}

fn example_input() -> Instructions {
    input_from_rows(common::input_rows("input/day10-example"))
}

fn example_input2() -> Instructions {
    input_from_rows(common::input_rows("input/day10-example2"))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Instruction {
    NoOp,
    AddX(i64),
}

impl Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self, f)
    }
}

impl Instruction {
    fn cycles_needed(&self) -> usize {
        match self {
            Instruction::NoOp => 1,
            Instruction::AddX(_) => 2,
        }
    }
}

impl From<&str> for Instruction {
    fn from(value: &str) -> Self {
        let chunks: Vec<&str> = value.split_whitespace().collect();
        match &chunks[..] {
            &["noop"] => Instruction::NoOp,
            &["addx", num] => Instruction::AddX(num.parse().unwrap()),
            _ => panic!("Unknown instruction {:?}", value),
        }
    }
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Instructions {
    rows.map(|string| Instruction::from(&string[..])).collect()
}

type Instructions = Vec<Instruction>;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct CpuState {
    x: i64,
}

impl CpuState {
    fn finalize_instruction(&mut self, instr: Instruction) {
        match instr {
            Instruction::NoOp => {}
            Instruction::AddX(num) => self.x += num,
        }
    }
}

type CpuTimeline = Vec<CpuState>;

fn calculate_timeline(instructions: &Instructions) -> CpuTimeline {
    let mut timeline = CpuTimeline::with_capacity(instructions.len() + 1);
    let mut state = CpuState { x: 1 };

    for instr in instructions {
        let cycles_taken = instr.cycles_needed();
        for _ in 0..cycles_taken {
            timeline.push(state.clone());
        }
        state.finalize_instruction(*instr);
    }

    timeline
}

fn signal_strength(timeline: &CpuTimeline) -> i64 {
    const START: usize = 20;
    const STRIDE: usize = 40;

    let mut signal_strength = 0;
    let mut i = START;
    loop {
        let Some(state) = timeline.get(i - 1) else {
            break;
        };
        let x = state.x;

        let added = x * i as i64;
        println!("During cycle {i}, x is {x}, giving a signal strength of {added}");
        signal_strength += added;

        i += STRIDE;
    }

    signal_strength
}

fn print_row(title: &str, bright_pixels: HashSet<usize>) {
    let mut pixels = String::with_capacity(40);

    for x in 0..40 {
        if bright_pixels.contains(&x) {
            pixels.push('#');
        } else {
            pixels.push('.');
        }
    }

    println!("{title}: {pixels}");
}

fn draw_pixel(current_row: &mut String, x: i64) {
    let crt_head_pos = current_row.len();

    if (crt_head_pos as i64 - x).abs() <= 1 {
        current_row.push('#');
    } else {
        current_row.push('.');
    }
}

fn get_sprite_pixels(x: i64) -> HashSet<usize> {
    let x = x as usize;
    HashSet::from([x.saturating_sub(1), x, x.saturating_add(1)])
}

fn render(instructions: &Instructions) -> Vec<String> {
    let mut rows = Vec::new();
    let mut current_row = String::with_capacity(40);

    let mut state = CpuState { x: 1 };
    let mut cycle = 0;

    for instr in instructions {
        let cycles_taken = instr.cycles_needed();
        for subcycle in 0..cycles_taken {
            println!();

            cycle += 1;
            if subcycle == 0 {
                println!("Start cycle  {cycle:>2}: begin executing {instr}");
            }

            let sprite_pixels = get_sprite_pixels(state.x);
            print_row("Sprite position", sprite_pixels);

            let crt_head_pos = cycle % 40 - 1;
            if crt_head_pos == 0 && cycle > 1 {
                rows.push(current_row.clone());
                current_row.clear();
            }
            println!("During cycle {cycle:>2}: CRT draws pixel in position {crt_head_pos}");
            draw_pixel(&mut current_row, state.x);
            println!("Current CRT row: {current_row}");
        }
        state.finalize_instruction(*instr);
        let x = state.x;
        println!("End of cycle {cycle:>2}: finish executing {instr} (Register is now {x})");
    }
    println!();

    rows.push(current_row);

    for row in rows.iter() {
        println!("{row}");
    }
    println!();

    rows
}

#[test]
fn parsing_works() {
    let parsed = example_input();
    let expected: Instructions = [
        Instruction::NoOp,
        Instruction::AddX(3),
        Instruction::AddX(-5),
    ]
    .into();

    assert_eq!(parsed, expected);
}

#[test]
fn execution_works() {
    let timeline = calculate_timeline(&example_input());
    let expected_timeline = vec![
        CpuState { x: 1 },
        CpuState { x: 1 },
        CpuState { x: 1 },
        CpuState { x: 4 },
        CpuState { x: 4 },
    ];

    assert_eq!(timeline, expected_timeline);
}

#[test]
fn part1_example2() {
    let timeline = calculate_timeline(&example_input2());
    let signal_strength = signal_strength(&timeline);

    assert_eq!(signal_strength, 13140);
}

#[test]
fn part1() {
    let timeline = calculate_timeline(&input());
    let signal_strength = signal_strength(&timeline);

    assert_eq!(signal_strength, 15220);
}

#[test]
fn part2_example2() {
    let display = render(&example_input2());
    let expected_display = vec![
        "##..##..##..##..##..##..##..##..##..##..",
        "###...###...###...###...###...###...###.",
        "####....####....####....####....####....",
        "#####.....#####.....#####.....#####.....",
        "######......######......######......####",
        "#######.......#######.......#######.....",
    ];

    assert_eq!(display[0], expected_display[0]);
    assert_eq!(display[1], expected_display[1]);
    assert_eq!(display[2], expected_display[2]);
    assert_eq!(display[3], expected_display[3]);
    assert_eq!(display[4], expected_display[4]);
    assert_eq!(display[5], expected_display[5]);

    assert_eq!(display, expected_display);
}

#[test]
fn part2() {
    let display = render(&input());
    let expected_display = vec![
        "###..####.####.####.#..#.###..####..##..",
        "#..#.#.......#.#....#.#..#..#.#....#..#.",
        "#..#.###....#..###..##...###..###..#..#.",
        "###..#.....#...#....#.#..#..#.#....####.",
        "#.#..#....#....#....#.#..#..#.#....#..#.",
        "#..#.#....####.####.#..#.###..#....#..#.",
    ];

    assert_eq!(display[0], expected_display[0]);
    assert_eq!(display[1], expected_display[1]);
    assert_eq!(display[2], expected_display[2]);
    assert_eq!(display[3], expected_display[3]);
    assert_eq!(display[4], expected_display[4]);
    assert_eq!(display[5], expected_display[5]);

    assert_eq!(display, expected_display);
}
