#![cfg(test)]

use log;

fn input() -> TreeGrid {
    input_from_rows(common::input_rows("input/day08"))
}

fn example_input() -> TreeGrid {
    input_from_rows(common::input_rows("input/day08-example"))
}

type TreeGrid = Vec<Vec<i32>>;

fn input_from_rows(rows: impl Iterator<Item = String>) -> TreeGrid {
    let mut grid = TreeGrid::new();

    for line in rows {
        let trees = line
            .char_indices()
            .map(|(start, _)| &line[start..line.ceil_char_boundary(start + 1)]);
        let mut trees = trees.map(str::parse);
        grid.push(trees.try_collect().unwrap());
    }

    grid
}

fn count_externally_visible_trees(grid: &TreeGrid) -> usize {
    let size_y = grid.len();
    let size_x = grid[0].len();

    let mut count = 0;

    for y in 0..size_y {
        'cell: for x in 0..size_x {
            let tree = grid[y][x];

            let pos_to_check: [Vec<(usize, usize)>; 4] = [
                (0..y).map(|y2| (x, y2)).collect(),
                ((y + 1)..size_y).map(|y2| (x, y2)).collect(),
                (0..x).map(|x2| (x2, y)).collect(),
                ((x + 1)..size_x).map(|x2| (x2, y)).collect(),
            ];

            'dirs: for dir in pos_to_check {
                for (x2, y2) in dir {
                    let tree2 = grid[y2][x2];
                    if tree2 >= tree {
                        // Occluded.
                        continue 'dirs;
                    }
                }
                // Not occluded.
                count += 1;
                continue 'cell;
            }
        }
    }

    count
}

fn calculate_scenic_score(grid: &TreeGrid, (x, y): (usize, usize)) -> u64 {
    let size_y = grid.len();
    let size_x = grid[0].len();

    let tree = grid[y][x];
    println!("Calculating for tree at position [{x}, {y}], which has a length of {tree}.");

    let pos_to_check: [Vec<(usize, usize)>; 4] = [
        (0..y).rev().map(|y2| (x, y2)).collect(),
        ((y + 1)..size_y).map(|y2| (x, y2)).collect(),
        (0..x).rev().map(|x2| (x2, y)).collect(),
        ((x + 1)..size_x).map(|x2| (x2, y)).collect(),
    ];

    let mut scenic_score = 1;

    for dir in pos_to_check {
        println!();
        let mut dist = 0;
        for (x2, y2) in dir {
            dist += 1;
            let tree2 = grid[y2][x2];
            println!("Position [{x2}, {y2}] has a tree of length {tree2}.");
            if tree2 >= tree {
                // Occluded.
                println!("Tree height {tree2} is taller than {tree}, stopping here.");
                break;
            } else {
                println!("Tree does not block view.");
            }
        }

        println!("Multiplying scenic score by {dist}.");
        scenic_score *= dist;
    }
    println!();

    scenic_score
}

fn find_max_scenic_score(grid: &TreeGrid) -> u64 {
    let size_y = grid.len();
    let size_x = grid[0].len();

    // NOTE: The real max scenic score must be greater than 0.
    let mut max_scenic_score = 0;

    for y in 0..size_y {
        for x in 0..size_x {
            let scenic_score = calculate_scenic_score(grid, (x, y));
            max_scenic_score = std::cmp::max(max_scenic_score, scenic_score);
        }
    }

    max_scenic_score
}

#[test]
fn parsing_works() {
    let parsed = example_input();
    let expected: TreeGrid = [
        [3, 0, 3, 7, 3].into(),
        [2, 5, 5, 1, 2].into(),
        [6, 5, 3, 3, 2].into(),
        [3, 3, 5, 4, 9].into(),
        [3, 5, 3, 9, 0].into(),
    ]
    .into();

    assert_eq!(parsed, expected);
}

#[test]
fn part1_example() {
    let count_visible = count_externally_visible_trees(&example_input());

    assert_eq!(count_visible, 21);
}

#[test]
fn part1() {
    let count_visible = count_externally_visible_trees(&input());

    assert_eq!(count_visible, 1796);
}

#[test]
fn part2_example() {
    let grid = example_input();

    assert_eq!(calculate_scenic_score(&grid, (2, 1)), 4);

    let max_scenic_score = find_max_scenic_score(&grid);
    assert_eq!(max_scenic_score, 8);
}

#[test]
fn part2() {
    let grid = input();

    let max_scenic_score = find_max_scenic_score(&grid);
    assert_eq!(max_scenic_score, 288120);
}
