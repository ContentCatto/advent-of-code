use std::collections::HashMap;

use crate::pos::{IntoBounds, Pos};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GrowGrid<T> {
    cells: HashMap<Pos, T>,
}

impl<T> GrowGrid<T> {
    pub fn new() -> Self {
        Self {
            cells: HashMap::new(),
        }
    }
}

impl<T> IntoBounds<Pos> for GrowGrid<T> {
    fn into_bounds(&self) -> Option<(Pos, Pos)> {
        self.cells.keys().copied().into_bounds()
    }
}

impl<T> From<T> for GrowGrid<T>
where
    HashMap<Pos, T>: From<T>,
{
    fn from(value: T) -> Self {
        Self {
            cells: HashMap::from(value),
        }
    }
}

impl<A> FromIterator<A> for GrowGrid<A>
where
    HashMap<Pos, A>: FromIterator<A>,
{
    fn from_iter<T: IntoIterator<Item = A>>(value: T) -> Self {
        Self {
            cells: HashMap::from_iter(value),
        }
    }
}
