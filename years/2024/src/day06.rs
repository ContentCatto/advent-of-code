use std::{
    collections::HashSet,
    fmt::{Display, Write},
    str::FromStr,
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day06"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day06-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let mut grid_rows = Vec::new();
    let mut guard = None;

    for (y, row) in rows.into_iter().enumerate() {
        let mut current_row = Vec::new();
        for (x, mut tile) in row.chars().enumerate() {
            if let Ok(dir) = Dir::from_char(tile) {
                assert_eq!(guard, None);
                guard = Some(((x, y), dir));
                tile = '.';
            };

            let is_obstacle = match tile {
                '.' => false,
                '#' => true,
                _ => panic!("Unexpected tile {tile}"),
            };
            current_row.push(is_obstacle);
        }
        grid_rows.push(current_row);
    }

    (
        grid_rows,
        guard.expect("Guard should be somewhere on the map"),
    )
}

type Pos = (usize, usize);
type GuardState = (Pos, Dir);
type Row = Vec<bool>;
type Map = Vec<Row>;

type Input = (Map, GuardState);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Dir {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct InvalidDir;

impl Dir {
    fn from_char(ch: char) -> Result<Self, InvalidDir> {
        match ch {
            '^' => Ok(Dir::Up),
            'v' => Ok(Dir::Down),
            '<' => Ok(Dir::Left),
            '>' => Ok(Dir::Right),
            _ => Err(InvalidDir),
        }
    }

    fn offset(self, (x, y): Pos) -> Option<Pos> {
        match self {
            Dir::Up if y > 0 => Some((x, y - 1)),
            Dir::Down => Some((x, y + 1)),
            Dir::Left if x > 0 => Some((x - 1, y)),
            Dir::Right => Some((x + 1, y)),
            _ => None,
        }
    }

    fn turn_right(self) -> Self {
        match self {
            Dir::Up => Dir::Right,
            Dir::Right => Dir::Down,
            Dir::Down => Dir::Left,
            Dir::Left => Dir::Up,
        }
    }
}

impl FromStr for Dir {
    type Err = InvalidDir;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.chars().count() > 1 || s.chars().next().is_none() {
            return Err(InvalidDir);
        }

        Self::from_char(
            s.chars()
                .next()
                .expect("Already checked to have at least one char"),
        )
    }
}

impl Display for Dir {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_char(match self {
            Dir::Up => '^',
            Dir::Down => 'v',
            Dir::Left => '<',
            Dir::Right => '>',
        })
    }
}

fn is_position_blocked(grid: &Vec<Vec<bool>>, (x, y): Pos) -> Option<bool> {
    grid.get(y).and_then(|row| row.get(x).copied())
}

struct GuardStateIter<'g> {
    obstacle_detection_function: Box<dyn FnMut(Pos) -> Option<bool> + 'g>,
    guard_pos: Pos,
    guard_dir: Dir,
}

impl<'g> GuardStateIter<'g> {
    pub fn new(grid: &'g Vec<Vec<bool>>, guard_state: GuardState) -> Self {
        Self::from_obstacle_detection_function(
            Box::new(|pos: Pos| is_position_blocked(grid, pos)),
            guard_state,
        )
    }

    pub fn from_obstacle_detection_function(
        obstacle_detection_function: impl FnMut(Pos) -> Option<bool> + 'g,
        (guard_pos, guard_dir): (Pos, Dir),
    ) -> Self {
        let obstacle_detection_function = Box::new(obstacle_detection_function);
        Self {
            obstacle_detection_function,
            guard_pos,
            guard_dir,
        }
    }
}

impl<'g> Iterator for GuardStateIter<'g> {
    type Item = (Pos, Dir);

    fn next(&mut self) -> Option<Self::Item> {
        let old_pos = self.guard_pos;
        for _ in 0..4 {
            self.guard_pos = self.guard_dir.offset(old_pos)?;
            let is_obstacle_tile = (self.obstacle_detection_function)(self.guard_pos)?;

            if !is_obstacle_tile {
                common::verbose_println!("Guard moved to {:?}", self.guard_pos);
                return Some((self.guard_pos, self.guard_dir));
            } else {
                common::verbose_println!("Obstacle at {:?}, turning right...", self.guard_pos);
                self.guard_dir = self.guard_dir.turn_right();
            }
        }
        panic!("Invalid map, guard is stuck between 4 obstacles")
    }
}

struct GuardPosIter<'g> {
    inner: GuardStateIter<'g>,
}

impl<'g> GuardPosIter<'g> {
    pub fn new(grid: &'g Vec<Vec<bool>>, (guard_pos, guard_dir): (Pos, Dir)) -> Self {
        Self {
            inner: GuardStateIter::new(grid, (guard_pos, guard_dir)),
        }
    }
}

impl<'g> Iterator for GuardPosIter<'g> {
    type Item = Pos;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(pos, _dir)| pos)
    }
}

fn part1_count_visited_tiles((grid, (guard_start_pos, guard_start_dir)): Input) -> u64 {
    let visited_positions = HashSet::<Pos>::from_iter(
        std::iter::once(guard_start_pos)
            .chain(GuardPosIter::new(&grid, (guard_start_pos, guard_start_dir))),
    );
    visited_positions.len() as u64
}

fn would_cause_loop_if_blocked(
    grid: &Vec<Vec<bool>>,
    blocked_position: Pos,
    (guard_start_pos, guard_start_dir): GuardState,
) -> bool {
    let mut visited_states = HashSet::new();
    visited_states.insert((guard_start_pos, guard_start_dir));

    let obstacle_detection_function = |pos| {
        is_position_blocked(grid, pos)
            .map(|is_normally_blocked| is_normally_blocked || pos == blocked_position)
    };

    for (pos, dir) in GuardStateIter::from_obstacle_detection_function(
        obstacle_detection_function,
        (guard_start_pos, guard_start_dir),
    ) {
        if visited_states.contains(&(pos, dir)) {
            return true;
        }
        visited_states.insert((pos, dir));
    }

    return false;
}

fn part2_count_looping_obstruction_positions(
    (grid, (guard_start_pos, guard_start_dir)): Input,
) -> u64 {
    // TODO: Could be optimized by keeping track of all the normally visited states, and trying to
    //       block the tile in front of each of them. This would mean we only need to check states
    //       after the blockage was introduced.

    let normally_visited_positions = HashSet::<Pos>::from_iter(
        std::iter::once(guard_start_pos)
            .chain(GuardPosIter::new(&grid, (guard_start_pos, guard_start_dir))),
    );

    normally_visited_positions
        .into_iter()
        .filter(|&pos_to_block| {
            would_cause_loop_if_blocked(&grid, pos_to_block, (guard_start_pos, guard_start_dir))
        })
        .count() as u64
}

#[test]
fn part1_example() {
    let input = example_input();
    let result = part1_count_visited_tiles(input);
    assert_eq!(result, 41);
}

#[test]
fn part1_real() {
    let input = input();
    let result = part1_count_visited_tiles(input);
    assert_eq!(result, 4454);
}

#[test]
fn part2_example() {
    let input = example_input();
    let result = part2_count_looping_obstruction_positions(input);
    assert_eq!(result, 6);
}

#[test]
fn part2_real() {
    let input = input();
    let result = part2_count_looping_obstruction_positions(input);
    assert_eq!(result, 1503);
}
