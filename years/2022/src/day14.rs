use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
    fmt::{Debug, Display, Write},
    iter::FromIterator,
    num::NonZeroUsize,
    ops::{Index, IndexMut, RangeInclusive},
};

use common::annotations::{select_coords_to_print, Annotatable};
use common::pos::*;
use common::{
    assert_line_by_line_equal, impl_display_for_annotatable, verbose_println, LinesToString, DEBUG,
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day14"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day14-example"))
}

fn part2_input() -> Input {
    part2_input_from_rows(common::input_rows("input/day14"))
}

fn part2_example_input() -> Input {
    part2_input_from_rows(common::input_rows("input/day14-example"))
}

fn parse_rocks_from_input(
    rows: impl Iterator<Item = String>,
) -> Result<Vec<RockDefinition>, Box<str>> {
    rows.map(|row| RockDefinition::try_from(&row[..]))
        .try_collect()
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let sand_leak = Pos(500, 0);
    let rocks = parse_rocks_from_input(rows).unwrap();

    let definition = TerrainDefinition { rocks, sand_leak };
    Terrain::from(definition)
}

fn part2_input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let sand_leak = Pos(500, 0);
    let rocks = parse_rocks_from_input(rows).unwrap();

    let mut definition = TerrainDefinition { rocks, sand_leak };
    definition.add_floor();

    Terrain::from(definition)
}

type Input = Terrain;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct RockDefinition(Vec<Pos>);

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct TerrainDefinition {
    rocks: Vec<RockDefinition>,
    sand_leak: Pos,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Terrain {
    grid: Vec<TileType>,
    grid_size: Size2D,
    grid_base_position: Pos,
    sand_leak: Pos,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TileType {
    Rock,
    Air,
    Sand,
}

impl TileType {
    pub fn char_representation(&self) -> char {
        match self {
            TileType::Rock => '#',
            TileType::Air => '.',
            TileType::Sand => 'o',
        }
    }
}

impl TryFrom<&str> for RockDefinition {
    type Error = Box<str>;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let position_strings = value.split(" -> ");
        let positions = position_strings
            .map(Pos::try_from_string_no_parens)
            .try_collect()?;

        Ok(RockDefinition(positions))
    }
}

impl RockDefinition {
    pub fn iter_path<'a>(&'a self) -> impl Iterator<Item = Pos> + 'a {
        let mut path: Vec<Pos> = self
            .0
            .windows(2)
            .map(|position_pairs| {
                let [start, end]: [Pos; 2] = position_pairs.try_into().unwrap();
                walk_cardinal_direction_path(start, end)
                    .unwrap()
                    .collect::<Vec<Pos>>()
                    .into_iter()
                    .inspect(|value| println!("{value:?}"))
            })
            .inspect(|value| println!("{value:?}"))
            .flatten()
            .inspect(|value| println!("{value:?}"))
            .collect();

        path.dedup();
        println!("Calculated rockdef path: {path:?}");

        path.into_iter()
    }
}

fn walk_cardinal_direction_path(
    start: Pos,
    end: Pos,
) -> Option<Box<dyn DoubleEndedIterator<Item = Pos>>> {
    match [start.into(), end.into()] {
        [(x1, mut y1), (x2, mut y2)] if x1 == x2 => {
            let x = x1;
            assert_eq!(x, x1);
            assert_eq!(x, x2);

            verbose_println!("Walking {x}, ({y1}..={y2})");

            let needs_revers = y1 > y2;
            if needs_revers {
                (y1, y2) = (y2, y1);
            }

            let mut iter: Box<dyn DoubleEndedIterator<Item = Pos>> =
                Box::new((y1..=y2).map(move |y| Pos(x, y)));

            if needs_revers {
                iter = Box::new(iter.rev());
            }

            Some(iter)
        }
        [(mut x1, y1), (mut x2, y2)] if y1 == y2 => {
            let y = y1;
            assert_eq!(y, y1);
            assert_eq!(y, y2);

            verbose_println!("Walking ({x1}..={x2}), {y}");

            let needs_revers = x1 > x2;
            if needs_revers {
                (x1, x2) = (x2, x1);
            }

            let mut iter: Box<dyn DoubleEndedIterator<Item = Pos>> =
                Box::new((x1..=x2).map(move |x| Pos(x, y)));

            if needs_revers {
                iter = Box::new(iter.rev());
            }
            Some(iter)
        }
        [(x1, y1), (x2, y2)] if x1 != x2 && y1 != y2 => None,
        [..] => {
            unreachable!("Unhandled case when matching start and end")
        }
    }
}

impl TerrainDefinition {
    pub fn size_bounds(&self) -> (Pos, Pos) {
        let mentioned_positions = self
            .rocks
            .iter()
            .flat_map(|rock_def| rock_def.0.iter())
            .copied()
            .chain(std::iter::once(self.sand_leak));

        let (x_positions, y_positions): (Vec<_>, Vec<_>) =
            mentioned_positions.map(Pos::into_tuple).unzip();

        let min_x = *x_positions.iter().min().unwrap();
        let max_x = *x_positions.iter().max().unwrap();
        let min_y = *y_positions.iter().min().unwrap();
        let max_y = *y_positions.iter().max().unwrap();

        let min = Pos(min_x, min_y);
        let max = Pos(max_x, max_y);
        (min, max)
    }

    pub fn add_floor(&mut self) {
        let (Pos(min_x, min_y), Pos(max_x, max_y)) = self.size_bounds();
        let max_y = max_y + 2;
        let map_height = max_y - min_y;
        let min_x = min_x.min(self.sand_leak.0 - map_height);
        let max_x = max_x.max(self.sand_leak.0 + map_height);

        self.rocks
            .push(RockDefinition(vec![Pos(min_x, max_y), Pos(max_x, max_y)]));
    }
}

impl From<TerrainDefinition> for Terrain {
    fn from(definition: TerrainDefinition) -> Self {
        let (Pos(min_x, min_y), Pos(max_x, max_y)) = definition.size_bounds();

        let TerrainDefinition { rocks, sand_leak } = definition;

        let grid_size = Size2D((max_x - min_x + 1) as usize, (max_y - min_y + 1) as usize);
        let grid_base_position = Pos(min_x, min_y);

        let grid = [TileType::Air].repeat(grid_size.0 * grid_size.1);
        let mut terrain = Terrain {
            grid,
            grid_size,
            grid_base_position,
            sand_leak,
        };

        for rock_def in rocks {
            verbose_println!("Following rockdef:  {rock_def:?}");
            for pos in rock_def.iter_path() {
                verbose_println!("{}", terrain);
                terrain[pos] = TileType::Rock;
            }
        }
        verbose_println!("Final rocks:\n{}", terrain);

        terrain
    }
}

impl Index<Pos> for Terrain {
    type Output = TileType;

    fn index(&self, index: Pos) -> &Self::Output {
        self.get(index)
            .unwrap_or_else(|| panic!("Attempting to access nothing at {:?}.", index))
    }
}

impl IndexMut<Pos> for Terrain {
    fn index_mut(&mut self, index: Pos) -> &mut Self::Output {
        self.get_mut(index)
            .unwrap_or_else(|| panic!("Attempting to mutate nothing at {:?}.", index))
    }
}

impl Terrain {
    pub fn has_tile(&self, Pos(x, y): Pos) -> bool {
        self.range_x().contains(&x) && self.range_y().contains(&y)
    }

    pub fn get(&self, index: Pos) -> Option<&TileType> {
        let Pos(x, y) = index;
        let x = x as isize - self.grid_base_position.0 as isize;
        let y = y as isize - self.grid_base_position.1 as isize;

        if x < 0 || y < 0 {
            return None;
        }

        let (x, y) = (x as usize, y as usize);

        let Size2D(x_size, y_size) = self.grid_size;

        if !(0..x_size).contains(&x) || !(0..y_size).contains(&y) {
            return None;
        }

        let flat_index = x + x_size * y;

        Some(self.grid.get(flat_index).unwrap())
    }

    pub fn get_mut(&mut self, index: Pos) -> Option<&mut TileType> {
        let Pos(x, y) = index - self.grid_base_position;

        let Size2D(x_size, y_size) = self.grid_size;

        let x: usize = x.try_into().ok()?;
        let y: usize = y.try_into().ok()?;

        if !(0..x_size).contains(&x) || !(0..y_size).contains(&y) {
            return None;
        }

        let flat_index = x + x_size * y;

        Some(self.grid.get_mut(flat_index).unwrap())
    }

    pub fn range_x(&self) -> RangeInclusive<isize> {
        self.grid_base_position.0..=(self.grid_base_position.0 + self.grid_size.0 as isize - 1)
    }

    pub fn range_y(&self) -> RangeInclusive<isize> {
        self.grid_base_position.1..=(self.grid_base_position.1 + self.grid_size.1 as isize - 1)
    }

    /// Returns true if the sand came to rest.
    /// Returns false if the sand fell off the world.
    /// Returns false if the sand leak is blocked by sand.
    pub fn add_sand(&mut self) -> bool {
        const OFFSET_DOWN: Pos = Pos(0, 1);
        const OFFSET_DOWN_LEFT: Pos = Pos(-1, 1);
        const OFFSET_DOWN_RIGHT: Pos = Pos(1, 1);

        let mut current_pos = self.sand_leak;

        if let Some(TileType::Sand) = self.get(current_pos) {
            println!("Sand leak is blocked by sand.");
            return false;
        }

        verbose_println!("Starting terrain:\n{self}");

        'outer: loop {
            if false {
                verbose_println!("{}", self.annotate([(current_pos, "O".to_owned())]));
            }

            for offset in [OFFSET_DOWN, OFFSET_DOWN_LEFT, OFFSET_DOWN_RIGHT] {
                let new_pos = current_pos + offset;
                let Some(&tile_at_pos) = self.get(new_pos) else {
                    current_pos = new_pos;
                    break 'outer;
                };

                match tile_at_pos {
                    TileType::Rock | TileType::Sand => {}
                    _ => {
                        current_pos = new_pos;
                        continue 'outer;
                    }
                }
            }

            break;
        }

        if let Some(TileType::Air) = self.get(current_pos) {
            *self.get_mut(current_pos).unwrap() = TileType::Sand;
            return true;
        }

        return false;
    }

    pub fn fill_with_sand(&mut self) -> usize {
        let mut num_sand_at_rest = 0;

        while self.add_sand() {
            num_sand_at_rest += 1;
        }
        println!("Final terrain:\n{}", self);

        return num_sand_at_rest;
    }
}

impl Annotatable for Terrain {
    fn write_fmt_annotated(
        &self,
        mut w: impl Write,
        annotations: &HashMap<Pos, String>,
    ) -> std::fmt::Result {
        let (x_coords_to_print, max_x_coord_num_chars) =
            select_coords_to_print(self.range_x(), NonZeroUsize::new(5).unwrap(), 1);
        let (y_coords_to_print, max_y_coord_num_chars) =
            select_coords_to_print(self.range_y(), NonZeroUsize::new(1).unwrap(), 0);

        let y_coord_pad = " ".repeat(max_y_coord_num_chars);

        for coord_char_index in 0..max_x_coord_num_chars {
            write!(w, "{y_coord_pad} ")?;
            for x in self.range_x() {
                let Some(string) = x_coords_to_print.get(&x) else {
                    write!(w, " ")?;
                    continue;
                };
                let blank_pad_count = max_x_coord_num_chars - string.len();
                let Some(digit_index) = coord_char_index.checked_sub(blank_pad_count) else {
                    write!(w, " ")?;
                    continue;
                };
                let digit_char = string.chars().nth(digit_index).unwrap();
                write!(w, "{}", digit_char)?;
            }
            write!(w, "\n")?;
        }

        for y in self.range_y() {
            if let Some(string) = y_coords_to_print.get(&y) {
                write!(w, "{string:>max_y_coord_num_chars$} ")?;
            } else {
                write!(w, "{y_coord_pad} ")?;
            }

            for x in self.range_x() {
                let pos = Pos(x, y);

                if let Some(annotation) = annotations.get(&pos) {
                    write!(w, "{}", annotation)?;
                    continue;
                }

                let tile = self[pos];

                if pos == self.sand_leak && tile != TileType::Sand {
                    write!(w, "+")?;
                    continue;
                }

                write!(w, "{}", tile.char_representation())?;
            }
            write!(w, "\n")?;
        }
        Ok(())
    }
}

impl_display_for_annotatable!(Terrain);

#[test]
fn test_parse_rock_definition() {
    let rock_def = RockDefinition::try_from("498,4 -> 498,6 -> 496,6").unwrap();
    assert_eq!(rock_def.0, [Pos(498, 4), Pos(498, 6), Pos(496, 6)]);
}

#[test]
fn test_iter_path() {
    let rock_def = RockDefinition::try_from("10,4 -> 15,4 -> 15,6").unwrap();

    let expected_path = [
        Pos(10, 4),
        Pos(11, 4),
        Pos(12, 4),
        Pos(13, 4),
        Pos(14, 4),
        Pos(15, 4),
        Pos(15, 5),
        Pos(15, 6),
    ];

    let path: Vec<_> = rock_def.iter_path().collect();

    assert_eq!(path, expected_path);
}

#[test]
fn test_iter_path_2() {
    let rock_def = RockDefinition(vec![Pos(503, 4), Pos(502, 4), Pos(502, 9), Pos(494, 9)]);

    let expected_path = [
        Pos(503, 4),
        Pos(502, 4),
        Pos(502, 5),
        Pos(502, 6),
        Pos(502, 7),
        Pos(502, 8),
        Pos(502, 9),
        Pos(501, 9),
        Pos(500, 9),
        Pos(499, 9),
        Pos(498, 9),
        Pos(497, 9),
        Pos(496, 9),
        Pos(495, 9),
        Pos(494, 9),
    ];

    let path: Vec<_> = rock_def.iter_path().collect();

    assert_eq!(path, expected_path);
}

#[test]
fn test_example_rockdef_parsing_works() {
    let rockdef = parse_rocks_from_input(common::input_rows("input/day14-example")).unwrap();
    println!("{:?}", rockdef);
    let expected = vec![
        RockDefinition(vec![Pos(498, 4), Pos(498, 6), Pos(496, 6)]),
        RockDefinition(vec![Pos(503, 4), Pos(502, 4), Pos(502, 9), Pos(494, 9)]),
    ];
    assert_eq!(rockdef, expected);
}

#[test]
fn test_example_parsing_works() {
    let terrain = example_input();

    println!("{:?}", terrain.grid_size);
    println!("{}", terrain);

    assert_eq!(terrain.sand_leak, Pos(500, 0));

    assert_eq!(terrain[Pos(503, 4)], TileType::Rock);
    assert_eq!(terrain[Pos(503, 3)], TileType::Air);
}

#[test]
fn test_terrain_displays_correctly() {
    let terrain = example_input();

    let display_full = format!("{}", terrain);
    let display_lines: Vec<&str> = display_full.lines().collect();
    assert!(display_full.ends_with("\n"));

    let expected_lines = vec![
        "  4     5  5",
        "  9     0  0",
        "  4     0  3",
        "0 ......+...",
        "1 ..........",
        "2 ..........",
        "3 ..........",
        "4 ....#...##",
        "5 ....#...#.",
        "6 ..###...#.",
        "7 ........#.",
        "8 ........#.",
        "9 #########.",
    ];

    assert_line_by_line_equal(&display_lines, &expected_lines);
}

#[test]
fn test_part2_terrain_displays_correctly() {
    let terrain = part2_example_input();

    let display_full = format!("{}", terrain);
    let display_lines: Vec<&str> = display_full.lines().collect();
    assert!(display_full.ends_with("\n"));

    let expected_lines = vec![
        "   4     4    5    5     5",
        "   8     9    0    0     1",
        "   9     5    0    5     1",
        " 0 ...........+...........",
        " 1 .......................",
        " 2 .......................",
        " 3 .......................",
        " 4 .........#...##........",
        " 5 .........#...#.........",
        " 6 .......###...#.........",
        " 7 .............#.........",
        " 8 .............#.........",
        " 9 .....#########.........",
        "10 .......................",
        "11 #######################",
    ];

    assert_line_by_line_equal(&display_lines, &expected_lines);
}

#[test]
fn part1_example() {
    let mut terrain = example_input();

    let sand_at_rest = terrain.fill_with_sand();

    assert_eq!(sand_at_rest, 24);
}

#[test]
fn part1_real() {
    let mut terrain = input();

    let sand_at_rest = terrain.fill_with_sand();

    assert_eq!(sand_at_rest, 892);
}

#[test]
fn part2_example() {
    let mut terrain = part2_example_input();

    let sand_at_rest = terrain.fill_with_sand();

    assert_eq!(sand_at_rest, 93);
}

#[test]
fn part2_real() {
    let mut terrain = part2_input();

    let sand_at_rest = terrain.fill_with_sand();

    assert_eq!(sand_at_rest, 27155);
}
