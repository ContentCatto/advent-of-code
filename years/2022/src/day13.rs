use std::{cmp::Ordering, fmt::Display, rc::Rc};

use common::verbose_println;

fn input() -> Input {
    input_from_rows(common::input_rows("input/day13"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day13-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    let mut row_buf = Vec::with_capacity(2);
    let mut pairs = Vec::new();

    for row in rows {
        if row.is_empty() {
            assert_eq!(row_buf.len(), 0);
            continue;
        }
        row_buf.push(row);
        if row_buf.len() == 2 {
            let second_row = row_buf.pop().unwrap();
            let first_row = row_buf.pop().unwrap();

            let second_packet = Packet::from(&second_row[..]);
            let first_packet = Packet::from(&first_row[..]);

            pairs.push([first_packet, second_packet]);
        }
    }

    pairs
}

type Input = PacketPairList;

#[derive(Debug, Clone, PartialEq, Eq)]
enum PacketPart {
    Integer(Num),
    List(PacketList),
}

impl Display for PacketPart {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PacketPart::Integer(n) => write!(f, "{}", n),
            PacketPart::List(elements) => {
                write!(f, "[")?;
                for (index, element) in elements.iter().enumerate() {
                    if index > 0 {
                        write!(f, ",")?;
                    }
                    write!(f, "{}", element)?;
                }
                write!(f, "]")?;
                Ok(())
            }
        }
    }
}

fn find_next_list_element(list_elements_string: &str) -> Option<usize> {
    if list_elements_string.is_empty() {
        return None;
    }

    let mut surrounding_brackets: usize = 0;

    for (pos, ch) in list_elements_string.char_indices() {
        match ch {
            ',' if surrounding_brackets == 0 => {
                return Some(pos);
            }
            '[' => surrounding_brackets += 1,
            ']' => surrounding_brackets -= 1,
            _ => {}
        }
    }

    assert_eq!(surrounding_brackets, 0);
    Some(list_elements_string.len())
}

impl From<&str> for PacketPart {
    fn from(value: &str) -> Self {
        if let Some(list_content_string) = value.strip_prefix('[').and_then(|s| s.strip_suffix(']'))
        {
            verbose_println!("Parsing list: {value:?}");
            let mut left_to_parse = list_content_string;

            let mut elements: Vec<PacketPart> = Vec::new();
            while left_to_parse.len() > 0 {
                let next_element = find_next_list_element(left_to_parse).unwrap();
                elements.push(PacketPart::from(&left_to_parse[..next_element]));
                left_to_parse = &left_to_parse[next_element..];
                if left_to_parse.len() > 0 {
                    assert_eq!(&left_to_parse[..1], ",");
                    left_to_parse = &left_to_parse[','.len_utf8()..];
                }
            }
            PacketPart::List(elements.into())
        } else {
            verbose_println!("Parsing integer: {value:?}");
            let number = value.parse().expect("Failed to parse integer");
            PacketPart::Integer(number)
        }
    }
}

type Num = i64;
type PacketList = Rc<[PacketPart]>;
type Packet = PacketPart;
type PacketPair = [Packet; 2];
type PacketPairList = Vec<PacketPair>;

impl PartialOrd for PacketPart {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(compare_packets([self, other], 0))
    }
}

impl Ord for PacketPart {
    fn cmp(&self, other: &Self) -> Ordering {
        compare_packets([self, other], 0)
    }
}

fn compare_packets([left, right]: [&Packet; 2], indent: usize) -> Ordering {
    fn convert_to_list(num: Num) -> PacketPart {
        PacketPart::List([PacketPart::Integer(num)].into())
    }

    let indent_whitespace = "  ".repeat(indent);
    verbose_println!("{indent_whitespace}- Compare {left} vs {right}");

    let next_indent = indent + 1;

    match [left, right] {
        [PacketPart::Integer(left), PacketPart::Integer(right)] => {
            if left < right {
                verbose_println!("{indent_whitespace}  - Left side is smaller, so the inputs are in the right order");
                Ordering::Less
            } else if left > right {
                verbose_println!("{indent_whitespace}  - Right side is smaller, so the inputs are not in the right order");
                Ordering::Greater
            } else {
                // NOTE: The actual values do not need to be equal,
                //       they simply order equally.
                Ordering::Equal
            }
        }
        [PacketPart::List(left_elems), PacketPart::List(right_elems)] => {
            for (left_elem, right_elem) in left_elems.iter().zip(right_elems.iter()) {
                let order = compare_packets([left_elem, right_elem], next_indent);
                if order == Ordering::Less || order == Ordering::Greater {
                    return order;
                }
            }

            let order = left_elems.len().cmp(&right_elems.len());
            match order {
                Ordering::Less => {
                    verbose_println!("{indent_whitespace}  - Left side ran out of items, so inputs are in the right order");
                }
                Ordering::Greater => {
                    verbose_println!("{indent_whitespace}  - Right side ran out of items, so inputs are not in the right order");
                }
                Ordering::Equal => {}
            }
            return order;
        }
        [PacketPart::Integer(left), right] => {
            let left = convert_to_list(*left);
            verbose_println!(
                "{indent_whitespace}  - Mixed types; convert left to {left} and retry comparison"
            );
            compare_packets([&left, right], next_indent)
        }
        [left, PacketPart::Integer(right)] => {
            let right = convert_to_list(*right);
            verbose_println!(
                "{indent_whitespace}  - Mixed types; convert right to {right} and retry comparison"
            );
            compare_packets([left, &right], next_indent)
        }
    }
}

fn is_right_order([left, right]: [&Packet; 2]) -> bool {
    match left.cmp(right) {
        Ordering::Less => true,
        Ordering::Equal => true,
        Ordering::Greater => false,
    }
}

fn compare_packet_stream(packets: &PacketPairList) -> Vec<bool> {
    let mut result = Vec::with_capacity(packets.len());
    for (index, [left, right]) in packets.iter().enumerate() {
        verbose_println!("== Pair {} ==", index + 1);
        result.push(is_right_order([left, right]));
        verbose_println!();
    }
    result
}

fn flatten_packets(packet_pairs: &PacketPairList) -> Vec<Packet> {
    packet_pairs.iter().flatten().cloned().collect()
}

fn find_decoder_key(packet_pairs: &PacketPairList) -> u64 {
    let mut packets = flatten_packets(packet_pairs);
    let divider_1 = Packet::from("[[2]]");
    let divider_2 = Packet::from("[[6]]");
    packets.push(divider_1.clone());
    packets.push(divider_2.clone());

    packets.sort();

    let divider_1_index = packets.iter().position(|elem| elem == &divider_1).unwrap();
    let divider_2_index = packets.iter().position(|elem| elem == &divider_2).unwrap();

    // The packets are 1-indexed, for some reason.
    let divider_1_index = divider_1_index + 1;
    let divider_2_index = divider_2_index + 1;

    (divider_1_index * divider_2_index) as u64
}

fn already_in_right_order_checksum(packets: &PacketPairList) -> u64 {
    let mut is_in_right_order = compare_packet_stream(packets);
    is_in_right_order.insert(0, false);

    is_in_right_order
        .into_iter()
        .enumerate()
        .fold(0, |acc, (index, is_right_order)| {
            if is_right_order {
                acc + index as u64
            } else {
                acc
            }
        })
}

#[test]
fn parsing_packet_works() {
    let packet_string = "[[1],[2,3,4]]";
    let packet = Packet::from(packet_string);
    let expected = PacketPart::List(
        [
            PacketPart::List([PacketPart::Integer(1)].into()),
            PacketPart::List(
                [
                    PacketPart::Integer(2),
                    PacketPart::Integer(3),
                    PacketPart::Integer(4),
                ]
                .into(),
            ),
        ]
        .into(),
    );

    assert_eq!(packet, expected);
}

#[test]
fn parsing_works() {
    let parsed = example_input();
    assert_eq!(&format!("{}", parsed[7][0]), "[1,[2,[3,[4,[5,6,7]]]],8,9]")
}

#[test]
fn part1_example() {
    let input = example_input();
    let checksum = already_in_right_order_checksum(&input);

    assert_eq!(checksum, 13);
}

#[test]
fn part1_actual() {
    let input = input();
    let checksum = already_in_right_order_checksum(&input);

    assert_eq!(checksum, 5208);
}

#[test]
fn part2_example() {
    let input = example_input();
    let decoder_key = find_decoder_key(&input);

    assert_eq!(decoder_key, 140);
}

#[test]
fn part2_actual() {
    let input = input();
    let decoder_key = find_decoder_key(&input);

    assert_eq!(decoder_key, 25792);
}
