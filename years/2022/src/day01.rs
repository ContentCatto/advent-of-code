#![cfg(test)]

type Elf = Vec<i32>;

fn input_from_rows(rows: impl Iterator<Item = String>) -> Vec<Elf> {
    let mut elves = Vec::<Elf>::new();
    let mut current = Elf::new();

    for row in rows {
        if row.is_empty() {
            elves.push(current);
            current = Elf::new();
        } else {
            let calories = row.parse::<i32>().unwrap();
            current.push(calories);
        }
    }
    if !current.is_empty() {
        elves.push(current);
    }

    elves
}

fn input() -> Vec<Elf> {
    input_from_rows(common::input_rows("input/day01"))
}

#[test]
fn non_empty() {
    assert_ne!(input().len(), 0);
}

#[test]
fn example_matches() {
    let example_elves: Vec<Elf> = [
        vec![1000, 2000, 3000],
        vec![4000],
        vec![5000, 6000],
        vec![7000, 8000, 9000],
        vec![10000],
    ]
    .into();
    let example_rows = example_elves
        .iter()
        .map(|elf: &Elf| {
            elf.iter()
                .map(|cal: &i32| format!("{}", cal))
                .collect::<Vec<String>>()
        })
        .collect::<Vec<Vec<String>>>()
        .join(&String::from(""));
    assert_eq!(input_from_rows(example_rows.into_iter()), example_elves);
}

#[test]
fn most_calories() {
    let elves = input();
    let calories_per_elf = elves
        .into_iter()
        .map(|elf: Elf| elf.into_iter().sum::<i32>());
    let max_cal_elf = calories_per_elf.max().unwrap();
    assert_eq!(max_cal_elf, 69177);
}

#[test]
fn top_three_calories() {
    let elves = input();
    let mut calories_per_elf: Vec<_> = elves
        .into_iter()
        .map(|elf: Elf| elf.into_iter().sum::<i32>())
        .collect();
    calories_per_elf.sort();
    calories_per_elf.reverse();
    let max_cal_elves = &calories_per_elf[..3];
    let top_tree_cal: i32 = max_cal_elves.iter().sum();
    assert_eq!(top_tree_cal, 207456);
}
