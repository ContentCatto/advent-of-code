#![cfg(test)]




// #[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
type Section = (i32, i32);
type SectionPair = (Section, Section);

fn parse_section(s: String) -> Section {
    let mut begin_and_end = s.split('-');
    let begin = begin_and_end.next().unwrap();
    let end = begin_and_end.next().unwrap();
    assert_eq!(begin_and_end.count(), 0);

    let begin: i32 = begin.parse().unwrap();
    let end: i32 = end.parse().unwrap();
    (begin, end)
}

fn full_containment(pair: SectionPair) -> bool {
    for (a, b) in [pair, (pair.1, pair.0)] {
        if a.0 >= b.0 && a.1 <= b.1 {
            return true;
        }
    }
    false
}

fn any_overlap(pair: SectionPair) -> bool {
    for (a, b) in [pair, (pair.1, pair.0)] {
        if b.0 >= a.0 && b.0 <= a.1 {
            return true;
        }
    }
    false
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Vec<SectionPair> {
    rows.map(|row: String| {
        let left: String = row.chars().take_while(|ch: &char| *ch != ',').collect();
        let right = String::from(&row[left.len() + ",".len()..]);

        let left = parse_section(left);
        let right = parse_section(right);

        (left, right)
    })
    .collect()
}

fn input() -> Vec<SectionPair> {
    input_from_rows(common::input_rows("input/day04"))
}

fn example_input() -> Vec<SectionPair> {
    let example_rows = "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8"
        .lines()
        .map(String::from);
    input_from_rows(example_rows)
}

#[test]
fn input_non_empty() {
    let inputs = [input(), example_input()];
    for i in inputs {
        assert_ne!(i.len(), 0);
    }
}

#[test]
fn example_containment_count() {
    let section_pairs = example_input();
    let full_containments = section_pairs.into_iter().map(full_containment);
    let total_containments = full_containments.filter(|contains| *contains).count();
    assert_eq!(total_containments, 2);
}

#[test]
fn containment_count() {
    let section_pairs = input();
    let full_containments = section_pairs.into_iter().map(full_containment);
    let total_containments = full_containments.filter(|contains| *contains).count();
    assert_eq!(total_containments, 413);
}

#[test]
fn example_overlap_count() {
    let section_pairs = example_input();
    let overlaps = section_pairs.into_iter().map(any_overlap);
    let overlap_count = overlaps.filter(|contains| *contains).count();
    assert_eq!(overlap_count, 4);
}

#[test]
fn overlap_count() {
    let section_pairs = input();
    let overlaps = section_pairs.into_iter().map(any_overlap);
    let overlap_count = overlaps.filter(|contains| *contains).count();
    assert_eq!(overlap_count, 806);
}
