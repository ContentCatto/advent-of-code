use std::{collections::HashSet, iter::FromIterator};

type Compartment = String;

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
struct Rucksack {
    left: Compartment,
    right: Compartment,
}

impl Rucksack {
    fn duplicate_item(&self) -> char {
        let left_set: HashSet<char> = HashSet::from_iter(self.left.chars());
        let right_set: HashSet<char> = HashSet::from_iter(self.right.chars());
        let mut common = left_set.intersection(&right_set);
        let duplicate = common.next().unwrap();
        assert_eq!(common.count(), 0);
        *duplicate
    }

    fn duplicate_priority(&self) -> i32 {
        item_priority(self.duplicate_item())
    }

    fn whole<'a>(&'a self) -> impl Iterator<Item = char> + 'a {
        self.left.chars().chain(self.right.chars())
    }
}

fn item_priority(item: char) -> i32 {
    match item {
        'a'..='z' => (item as u32 - 'a' as u32 + 1) as i32,
        'A'..='Z' => (item as u32 - 'A' as u32 + 27) as i32,
        _ => panic!(),
    }
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Vec<Rucksack> {
    rows.map(|row: String| {
        let compartment_size = row.chars().count() / 2;
        let left = row.chars().take(compartment_size).collect::<String>();
        let right = row.chars().skip(compartment_size).collect::<String>();
        Rucksack { left, right }
    })
    .collect()
}

fn input() -> Vec<Rucksack> {
    input_from_rows(common::input_rows("input/day03"))
}

fn example_input() -> Vec<Rucksack> {
    let example_rows = "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw"
        .lines()
        .map(String::from);
    input_from_rows(example_rows)
}

#[test]
fn non_empty() {
    let sacks = input();
    assert_ne!(sacks.len(), 0);
    let example_sacks = example_input();
    assert_ne!(example_sacks.len(), 0);
}

#[test]
fn even_compartments() {
    let real_sacks = input();
    let example_sacks = example_input();
    for sacks in [real_sacks, example_sacks] {
        for sack in sacks {
            assert_eq!(sack.left.len(), sack.right.len());
        }
    }
}

#[test]
fn priority_sum() {
    let sacks = input();
    let priorities = sacks.iter().map(Rucksack::duplicate_priority);
    let prio_sum: i32 = priorities.sum();
    assert_eq!(prio_sum, 7817);
}

#[test]
fn group_priority_sum() {
    let sacks = input();
    let groups: Vec<_> = sacks.chunks_exact(3).collect();

    let priorities = groups
        .iter()
        .map(|sacks| {
            let strings = sacks.iter().map(|sack| sack.whole().collect::<String>());
            let mut sets = strings.map(|s| HashSet::<char>::from_iter(s.chars()));
            let mut common = sets.next().unwrap();
            for set in sets {
                common = common.intersection(&set).copied().collect();
            }
            assert_eq!(common.len(), 1);
            common.into_iter().next().unwrap()
        })
        .map(item_priority);
    let prio_sum: i32 = priorities.sum();
    assert_eq!(prio_sum, 2444);
}
