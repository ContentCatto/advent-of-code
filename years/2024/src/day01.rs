use std::{
    collections::{HashMap, HashSet},
    hash::Hash,
    ops::AddAssign,
};

fn input() -> Input {
    input_from_rows(common::input_rows("input/day01"))
}

fn example_input() -> Input {
    input_from_rows(common::input_rows("input/day01-example"))
}

fn input_from_rows(rows: impl Iterator<Item = String>) -> Input {
    rows.map(|row| {
        let &[left, right] = &row.split_whitespace().collect::<Vec<&str>>()[..] else {
            panic!("Expected two numbers separated by whitespace.");
        };

        let left: u64 = left
            .parse()
            .expect("Expected left substring to be a number");
        let right: u64 = right
            .parse()
            .expect("Expected right substring to be a number");
        (left, right)
    })
    .unzip()
}

type Input = (Vec<u64>, Vec<u64>);

fn part1_checksum((mut left, mut right): Input) -> u64 {
    left.sort_unstable();
    right.sort_unstable();

    left.into_iter()
        .zip(right)
        .map(|(left, right)| left.abs_diff(right))
        .sum()
}

fn occurance_counts<C: From<u8> + AddAssign, T: Eq + Hash>(
    it: impl IntoIterator<Item = T>,
) -> HashMap<T, C> {
    let mut counts = HashMap::new();
    for val in it {
        *counts.entry(val).or_insert_with(|| 0.into()) += 1.into();
    }
    counts
}

fn part2_similarity_score((left, right): Input) -> u64 {
    let left_counts: HashMap<u64, u64> = occurance_counts(left);
    let right_counts: HashMap<u64, u64> = occurance_counts(right);

    left_counts
        .into_iter()
        .map(|(num, left_count)| {
            let right_count = right_counts.get(&num).copied().unwrap_or(0);
            (num, left_count * right_count)
        })
        .map(|(num, total_count)| num * total_count)
        .sum()
}

#[test]
fn parse_example() {
    let lists = example_input();
    let side_by_side: Vec<(u64, u64)> = lists.0.into_iter().zip(lists.1).collect();
    #[rustfmt::skip]
    let expected = vec![
        (3, 4),
        (4, 3),
        (2, 5),
        (1, 3),
        (3, 9),
        (3, 3),
    ];
    assert_eq!(side_by_side, expected);
}

#[test]
fn part1_example() {
    let lists = example_input();
    let checksum = part1_checksum(lists);
    assert_eq!(checksum, 11);
}

#[test]
fn part1_real() {
    let lists = input();
    let checksum = part1_checksum(lists);
    assert_eq!(checksum, 1_873_376);
}

#[test]
fn part2_example() {
    let lists = example_input();
    let similarity_score = part2_similarity_score(lists);
    assert_eq!(similarity_score, 31);
}

#[test]
fn part2_real() {
    let lists = input();
    let similarity_score = part2_similarity_score(lists);
    assert_eq!(similarity_score, 18_997_088);
}
