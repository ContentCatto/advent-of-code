use std::slice::Join;

pub trait LinesToString
where
    Self: Join<&'static str>,
    <Self as Join<&'static str>>::Output: Into<String>,
{
    fn lines_to_string(&self) -> String {
        lines_to_string(self)
    }
}

impl<T> LinesToString for Vec<T>
where
    Self: Join<&'static str>,
    String: From<<Self as Join<&'static str>>::Output>,
{
}

impl<T> LinesToString for [T]
where
    Self: Join<&'static str>,
    String: From<<Self as Join<&'static str>>::Output>,
{
}

fn lines_to_string<C>(container: &C) -> String
where
    C: Join<&'static str> + ?Sized,
    <C as Join<&'static str>>::Output: Into<String>,
{
    let mut s = C::join(container, "\n").into();
    s.push('\n');
    s
}
