use std::fmt::{Debug, Display};
use std::rc::Rc;

use derive_display_from_debug::Display;
use once_cell::sync::Lazy;

use common::{verbose_println, DEBUG};

fn input() -> MonkeyById {
    input_from_rows(common::input_rows("input/day11"))
}

fn example_input() -> MonkeyById {
    input_from_rows(common::input_rows("input/day11-example"))
}

#[derive(Debug, Display, Clone, PartialEq, Eq)]
enum TestOperation {
    Multiply(WorryLevel),
    Add(WorryLevel),
    Square(),
}

impl TestOperation {
    fn perform(&self, mut item: WorryLevel) -> WorryLevel {
        self.perform_inplace(&mut item);
        item
    }

    fn perform_inplace(&self, item: &mut WorryLevel) {
        match self {
            TestOperation::Multiply(n) => *item *= n,
            TestOperation::Add(n) => *item += n,
            TestOperation::Square() => *item = item.pow(2),
        }
    }

    fn describe(&self) -> String {
        match self {
            TestOperation::Multiply(n) => format!("is multiplied by {n}"),
            TestOperation::Add(n) => format!("increases by {n}"),
            TestOperation::Square() => format!("is multiplied by itself"),
        }
    }
}

impl From<&str> for TestOperation {
    fn from(value: &str) -> Self {
        match as_vec(value.split_whitespace()).as_slice() {
            &["Operation:", "new", "=", "old", "*", "old"] => TestOperation::Square(),
            &["Operation:", "new", "=", "old", "*", num] => {
                TestOperation::Multiply(num.parse().unwrap())
            }
            &["Operation:", "new", "=", "old", "+", num] => {
                TestOperation::Add(num.parse().unwrap())
            }
            _ => panic!("Unparsed TestOperation: {:?}", value),
        }
    }
}

#[derive(Debug, Display, Clone, PartialEq, Eq)]
enum TestCheck {
    Divisible(WorryLevel),
}

static ZERO_WORRY: Lazy<WorryLevel> = Lazy::new(|| WorryLevel::from(0u32));

impl TestCheck {
    fn perform(&self, worry: &WorryLevel) -> bool {
        match self {
            TestCheck::Divisible(n) => worry % n == *ZERO_WORRY,
        }
    }

    fn describe(&self) -> String {
        match self {
            TestCheck::Divisible(n) => format!("divisible by {n}"),
        }
    }

    fn divisor(&self) -> &WorryLevel {
        match self {
            TestCheck::Divisible(n) => n,
        }
    }
}

impl From<&str> for TestCheck {
    fn from(value: &str) -> Self {
        match as_vec(value.split_whitespace()).as_slice() {
            &["Test:", "divisible", "by", num] => TestCheck::Divisible(num.parse().unwrap()),
            _ => panic!("Unparsed TestCheck: {:?}", value),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ThrowTo {
    if_true: MonkeyId,
    if_false: MonkeyId,
}

impl ThrowTo {
    fn get(&self, check: bool) -> MonkeyId {
        match check {
            true => self.if_true,
            false => self.if_false,
        }
    }
}

impl From<Vec<&str>> for ThrowTo {
    fn from(value: Vec<&str>) -> Self {
        let lines = value;

        let true_line = lines[0];
        let false_line = lines[1];

        let if_true = true_line
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();
        let if_false = false_line
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        ThrowTo { if_true, if_false }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct MonkeyLogic {
    operation: TestOperation,
    test: TestCheck,
    throw_to: ThrowTo,
}

impl From<Vec<&str>> for MonkeyLogic {
    fn from(value: Vec<&str>) -> Self {
        let lines = value;
        let operation_line = lines[0];
        let operation = TestOperation::from(operation_line);

        let test_line = lines[1];
        let test = TestCheck::from(test_line);

        let throw_to_lines: Vec<&str> = Vec::from(&lines[2..4]);
        let throw_to = ThrowTo::from(throw_to_lines);

        MonkeyLogic {
            operation,
            test,
            throw_to,
        }
    }
}

impl MonkeyLogic {
    fn process_item(
        &self,
        mut worry_level: WorryLevel,
        manageable_worry: bool,
    ) -> (WorryLevel, MonkeyId) {
        //  Monkey inspects an item with a worry level of 79.
        //    Worry level is multiplied by 19 to 1501.
        //    Monkey gets bored with item. Worry level is divided by 3 to 500.
        //    Current worry level is not divisible by 23.
        //    Item with worry level 500 is thrown to monkey 3.

        verbose_println!("  Monkey inspects an item with a worry level of {worry_level}.");

        worry_level = self.operation.perform(worry_level);
        if DEBUG {
            let operation_description = self.operation.describe();
            verbose_println!("    Worry level {operation_description} to {worry_level}.");
        }

        if manageable_worry {
            worry_level /= 3u32;
            verbose_println!(
                "    Monkey gets bored with item. Worry level is divided by 3 to {worry_level}."
            );
        }

        let check = self.test.perform(&worry_level);
        if DEBUG {
            let check_description = self.test.describe();
            let not = match check {
                true => "",
                false => "not ",
            };
            verbose_println!("    Current worry level is {not}{check_description}.");
        }

        let thrown_to = self.throw_to.get(check);
        verbose_println!(
            "    Item with worry level {worry_level} is thrown to monkey {thrown_to}."
        );

        (worry_level, thrown_to)
    }

    fn bulk_perform_operation(&self, items: &mut [WorryLevel]) {
        for item in items {
            self.operation.perform_inplace(item);
        }
    }

    fn bulk_reduce_worry(&self, items: &mut [WorryLevel]) {
        for item in items {
            *item /= 3u32;
        }
    }

    fn bulk_optimize_worry(&self, items: &mut [WorryLevel], common_divisor: &WorryLevel) {
        for item in items {
            *item %= common_divisor;
        }
    }

    fn bulk_test_items(&self, items: Vec<WorryLevel>) -> ([Vec<WorryLevel>; 2], Vec<bool>) {
        let mut false_items = Vec::with_capacity(items.len());
        let mut true_items = Vec::with_capacity(items.len());
        let mut bools = Vec::with_capacity(items.len());

        for item in items {
            let check_result = self.test.perform(&item);
            bools.push(check_result);
            if check_result {
                true_items.push(item);
            } else {
                false_items.push(item);
            }
        }

        ([false_items, true_items], bools)
    }

    fn bulk_process_items(
        &mut self,
        items: &mut Vec<WorryLevel>,
        manageable_worry: bool,
        common_divisor: &WorryLevel,
    ) -> [(MonkeyId, Vec<WorryLevel>); 2] {
        let mut items = items.split_off(0);

        let debug_strings_capacity = if DEBUG { items.len() } else { 0 };
        let mut debug_strings: [Vec<Rc<str>>; 5] = [
            Vec::with_capacity(debug_strings_capacity),
            Vec::with_capacity(debug_strings_capacity),
            Vec::with_capacity(debug_strings_capacity),
            Vec::with_capacity(debug_strings_capacity),
            Vec::with_capacity(debug_strings_capacity),
        ];

        // Generate messages for the inspection.
        if DEBUG {
            for item in &items {
                let message =
                    format!("  Monkey inspects an item with a worry level of {item}.").into();
                debug_strings[0].push(message);
            }
        }

        self.bulk_perform_operation(&mut items[..]);

        // Generate messages for the operation.
        if DEBUG {
            let operation_description = self.operation.describe();
            for item in &items {
                let message = format!("    Worry level {operation_description} to {item}.").into();
                debug_strings[1].push(message);
            }
        }

        if manageable_worry {
            self.bulk_reduce_worry(&mut items[..]);
        }

        self.bulk_optimize_worry(&mut items[..], common_divisor);

        // Generate messages for the worry reduction.
        if manageable_worry && DEBUG {
            for item in &items {
                let message = format!(
                    "    Monkey gets bored with item. Worry level is divided by 3 to {item}."
                )
                .into();
                debug_strings[2].push(message);
            }
        }

        let mut item_strings = Vec::with_capacity(debug_strings_capacity);
        if DEBUG {
            for item in &items {
                item_strings.push(format!("{}", item));
            }
        }

        let ([false_items, true_items], bools) = self.bulk_test_items(items);

        // Generate messages for the worry level check.
        if DEBUG {
            let check_description = self.test.describe();
            let false_message: Rc<str> =
                format!("    Current worry level is not {check_description}.").into();
            let true_message: Rc<str> =
                format!("    Current worry level is {check_description}.").into();

            for (item_index, check_result) in bools.into_iter().enumerate() {
                let item = &item_strings[item_index];
                let thrown_to = self.throw_to.get(check_result);
                let thrown_to_message =
                    format!("    Item with worry level {item} is thrown to monkey {thrown_to}.")
                        .into();

                if check_result {
                    debug_strings[3].push(true_message.clone());
                } else {
                    debug_strings[3].push(false_message.clone());
                }
                debug_strings[4].push(thrown_to_message);
            }
        }

        let thrown_to_false = self.throw_to.if_false;
        let thrown_to_true = self.throw_to.if_true;

        // Print the accumilated messages.
        // We do this per-item first, so it looks like we did each item one-by-one.
        if DEBUG {
            for item_index in 0..debug_strings[0].len() {
                for step_index in 0..debug_strings.len() {
                    if step_index == 2 {
                        continue;
                    }
                    let message = &debug_strings[step_index][item_index];
                    verbose_println!("{}", message);
                }
            }
        }

        [(thrown_to_false, false_items), (thrown_to_true, true_items)]
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Monkey {
    id: MonkeyId,
    logic: MonkeyLogic,
    items: Vec<WorryLevel>,
}

impl Monkey {
    fn process_items(
        &mut self,
        manageable_worry: bool,
        common_divisor: &WorryLevel,
    ) -> [(MonkeyId, Vec<WorryLevel>); 2] {
        // let mut processed_items = Vec::new();
        // for item in self.items.drain(..) {
        //     processed_items.push(self.logic.process_item(item, manageable_worry));
        // }
        // processed_items
        self.logic
            .bulk_process_items(&mut self.items, manageable_worry, common_divisor)
    }
}

fn as_vec<'a>(strings: impl IntoIterator<Item = &'a str>) -> Vec<&'a str> {
    strings.into_iter().collect::<Vec<_>>()
}

impl From<Vec<&str>> for Monkey {
    fn from(value: Vec<&str>) -> Self {
        let lines = value;
        let id_line = lines[0];
        let (_, id) = id_line.split_once(' ').unwrap();
        let id = id.strip_suffix(':').unwrap();
        let id = id.parse().unwrap();

        let starting_items_lines = lines[1];
        let (_, starting_items) = starting_items_lines.split_once(':').unwrap();
        let starting_items: Vec<WorryLevel> = starting_items
            .trim_start()
            .split(", ")
            .map(str::parse)
            .try_collect()
            .unwrap();

        let logic_lines: Vec<&str> = Vec::from(&lines[2..6]);
        let logic = MonkeyLogic::from(logic_lines);

        Monkey {
            id,
            logic,
            items: starting_items,
        }
    }
}

type MonkeyId = usize;
type WorryLevel = num::BigUint;
type MonkeyById = Vec<Monkey>;

fn input_from_rows(rows: impl Iterator<Item = String>) -> MonkeyById {
    let rows: Vec<String> = rows.map(|line| line.trim_start().to_owned()).collect();
    let mut monkeys = MonkeyById::new();
    let mut current_monkey_rows = Vec::with_capacity(6);
    for row in rows.iter() {
        if row.is_empty() {
            let monkey = Monkey::from(current_monkey_rows.split_off(0));
            monkeys.insert(monkey.id, monkey);
            continue;
        }

        current_monkey_rows.push(&row);
    }
    let monkey = Monkey::from(current_monkey_rows.split_off(0));
    monkeys.insert(monkey.id, monkey);

    monkeys
}

fn inspection_counts(
    mut clan: MonkeyById,
    num_rounds: usize,
    manageable_worry: bool,
) -> Vec<usize> {
    let mut inspection_counts_by_monkey = [0usize].repeat(clan.len());
    let mut inspection_counts_this_round = inspection_counts_by_monkey.clone();

    let mut divisors: Vec<WorryLevel> = clan
        .iter()
        .map(|monkey| monkey.logic.test.divisor().clone())
        .collect();
    divisors.sort();
    divisors.dedup();

    let common_divisor = divisors
        .into_iter()
        .fold(WorryLevel::from(1u32), |lhs, rhs| lhs * rhs);

    for round in 1..=num_rounds {
        println!("Begin round {round}.");

        if DEBUG {
            verbose_println!("Monkey possessions:");
            for monkey in &clan {
                verbose_println!("  Monkey {}: {:?}", monkey.id, monkey.items);
            }
        }

        for throwing_monkey_id in 0..clan.len() {
            verbose_println!("Monkey {throwing_monkey_id}:");

            let inspection_count = clan[throwing_monkey_id].items.len();
            inspection_counts_this_round[throwing_monkey_id] += inspection_count;

            let thrown_items: [(MonkeyId, Vec<WorryLevel>); 2] =
                clan[throwing_monkey_id].process_items(manageable_worry, &common_divisor);

            for (catching_monkey_id, mut caught_items) in thrown_items {
                clan[catching_monkey_id].items.append(&mut caught_items);
            }
        }
        verbose_println!("Inspection counts this round:");
        for monkey_id in 0..clan.len() {
            let inspection_count = inspection_counts_this_round[monkey_id];
            verbose_println!("  Monkey {monkey_id}: {inspection_count}");

            inspection_counts_this_round[monkey_id] = 0;
            inspection_counts_by_monkey[monkey_id] += inspection_count;
        }
        if DEBUG {
            verbose_println!("Inspection counts total:");
            for monkey_id in 0..clan.len() {
                let inspection_count = inspection_counts_by_monkey[monkey_id];
                verbose_println!("  Monkey {monkey_id}: {inspection_count}");
            }
            verbose_println!();
        }
    }

    inspection_counts_by_monkey
}

fn calculate_monkey_business(mut inspection_counts: Vec<usize>) -> usize {
    inspection_counts.sort_unstable();
    inspection_counts.reverse();

    assert!(inspection_counts.len() >= 2);
    inspection_counts[0] * inspection_counts[1]
}

#[test]
fn parsing_works() {
    let parsed = example_input();
    let specific_monkey = &parsed[1];

    // Monkey 1:
    //   Starting items: 54, 65, 75, 74
    //   Operation: new = old + 6
    //   Test: divisible by 19
    //     If true: throw to monkey 2
    //     If false: throw to monkey 0
    let expected = Monkey {
        id: 1,
        logic: MonkeyLogic {
            operation: TestOperation::Add(6u32.into()),
            test: TestCheck::Divisible(19u32.into()),
            throw_to: ThrowTo {
                if_true: 2,
                if_false: 0,
            },
        },
        items: vec![54u32.into(), 65u32.into(), 75u32.into(), 74u32.into()],
    };
    assert_eq!(specific_monkey, &expected);

    assert_eq!(parsed.len(), 4);
}

#[test]
fn part1_example() {
    let clan = example_input();
    let monkey_business = calculate_monkey_business(inspection_counts(clan, 20, true));

    assert_eq!(monkey_business, 10605);
}

#[test]
fn part1() {
    let clan = input();
    let monkey_business = calculate_monkey_business(inspection_counts(clan, 20, true));

    assert_eq!(monkey_business, 51075);
}

#[test]
fn part2_example() {
    let clan = example_input();
    let monkey_business = calculate_monkey_business(inspection_counts(clan, 10000, false));

    assert_eq!(monkey_business, 2713310158);
}

#[test]
fn part2() {
    let clan = input();
    let monkey_business = calculate_monkey_business(inspection_counts(clan, 10000, false));

    assert_eq!(monkey_business, 11741456163);
}
