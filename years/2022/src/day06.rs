#![cfg(test)]

use std::{
    collections::{VecDeque},
};

// #[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]

fn input() -> String {
    // :facepalm:
    let mut it = common::input_rows("input/day06");
    let s = it.next().unwrap();
    assert_eq!(it.count(), 0); // ???
    s
}

fn example_input() -> String {
    String::from("mjqjpqmgbljsphdztnvjfqwrcgsmlb")
}

fn example_inputs() -> Vec<String> {
    [
        "mjqjpqmgbljsphdztnvjfqwrcgsmlb",
        "bvwbjplbgvbhsrlpgdmjqwftvncz",
        "nppdvjthqldpwncqszvftbrmjlhg",
        "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
        "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw",
    ]
    .iter()
    .copied() // Copy &str from the &&str
    .map(String::from)
    .collect()
}

fn start_of_packet(stream: &str, num_distinct: usize) -> usize {
    const fn index(ch: char) -> usize {
        ch as usize - 'a' as usize
    }

    const NUM_CHARS: usize = index('z') + 1;
    type CharCount = [usize; NUM_CHARS];
    let mut char_counts: CharCount = [0; NUM_CHARS];
    let mut unique_count: usize = 0;

    let mut debug_queue: VecDeque<char> = VecDeque::with_capacity(num_distinct);
    fn debug(debug_queue: &VecDeque<char>) {
        eprintln!("[{}]", debug_queue.iter().collect::<String>());
    }

    fn enter(
        char_counts: &mut CharCount,
        unique_count: &mut usize,
        debug_queue: &mut VecDeque<char>,
        ch: char,
    ) {
        let i = index(ch);
        char_counts[i] += 1;
        debug_queue.push_back(ch);
        if char_counts[i] == 1 {
            *unique_count += 1;
        }
    }

    fn exit(
        char_counts: &mut CharCount,
        unique_count: &mut usize,
        debug_queue: &mut VecDeque<char>,
        ch: char,
    ) {
        let i = index(ch);
        char_counts[i] -= 1;
        debug_queue.pop_front();
        if char_counts[i] == 0 {
            *unique_count -= 1;
        }
    }

    let mut enter_count: usize = 0;
    let mut entering = stream.chars();
    let mut exiting = stream.chars();
    for _ in 0..num_distinct {
        enter(
            &mut char_counts,
            &mut unique_count,
            &mut debug_queue,
            entering.next().unwrap(),
        );
        enter_count += 1;
        debug(&debug_queue);
    }
    loop {
        let enter_ch = entering.next().unwrap();
        let exit_ch = exiting.next().unwrap();

        exit(
            &mut char_counts,
            &mut unique_count,
            &mut debug_queue,
            exit_ch,
        );
        enter(
            &mut char_counts,
            &mut unique_count,
            &mut debug_queue,
            enter_ch,
        );
        enter_count += 1;
        debug(&debug_queue);

        if unique_count == num_distinct {
            eprint!("[{}]", debug_queue.iter().collect::<String>());
            eprintln!("{}", stream.chars().skip(enter_count).collect::<String>());

            eprint!(
                "{}",
                stream
                    .chars()
                    .take(enter_count - num_distinct)
                    .collect::<String>()
            );
            eprint!("[{}]", debug_queue.iter().collect::<String>());
            eprintln!("{}", stream.chars().skip(enter_count).collect::<String>());
            return enter_count;
        }
    }
}

#[test]
fn example_matches() {
    let s = example_input();
    let sop = start_of_packet(&s, 4);
    assert_eq!(sop, 7);
}

#[test]
fn examples_all_match() {
    let strings = example_inputs();
    let sops: Vec<_> = strings
        .iter()
        .map(|s: &String| start_of_packet(s, 4))
        .collect();
    assert_eq!(&sops[..], &[7, 5, 6, 10, 11]);
}

#[test]
fn start_of_packet_matches() {
    let s = input();
    let sop = start_of_packet(&s, 4);
    assert_eq!(sop, 1542);
}

#[test]
fn start_of_message_matches() {
    let s = input();
    let sop = start_of_packet(&s, 14);
    assert_eq!(sop, 3153);
}
