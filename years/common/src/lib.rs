#![feature(slice_concat_trait)]

pub mod annotations;
pub mod combinatorics;
pub mod grid;
pub mod lines;
pub mod pos;
pub mod sparse_range;
pub mod verbose;

pub use crate::lines::*;
pub use crate::verbose::*;

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

pub fn input_rows(path: &'static str) -> impl Iterator<Item = String> {
    let f = File::open(path).unwrap();
    let buf = BufReader::new(f);
    buf.lines().map(|r| r.unwrap())
}

pub fn assert_line_by_line_equal(actual_lines: &Vec<&str>, expected_lines: &Vec<&str>) {
    let actual_full = actual_lines.lines_to_string();
    let expected_full = expected_lines.lines_to_string();

    println!("Expected:\n{}", expected_full);
    println!("Actual:\n{}", actual_full);

    for (&expected_line, &actual_line) in expected_lines.iter().zip(actual_lines) {
        assert_eq!(actual_line, expected_line);
    }

    assert_eq!(actual_full, expected_full);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn has_rows() {
        assert!(input_rows("../2022/input/day01").count() > 5);
    }
}
